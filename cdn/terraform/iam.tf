# IAM

# A friendly name for our account.  This is displayed in various
# places, such as the AWS Management Console.

resource "aws_iam_account_alias" "alias" {
  account_alias = "guix"
}

# Encourage good password hygiene.

resource "aws_iam_account_password_policy" "strict" {
  minimum_password_length = 20
  require_lowercase_characters = true
  require_numbers = true
  require_uppercase_characters = true
  require_symbols = true
  allow_users_to_change_password = true
  password_reuse_prevention = 1
}

# The administrators group.

resource "aws_iam_group" "administrators" {
  name = "administrators"
}
resource "aws_iam_group_policy_attachment" "administrators-policy-attachment" {
  group      = "${aws_iam_group.administrators.name}"
  # This is a "managed policy".  See:
  # https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_job-functions.html#jf_administrator
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}
resource "aws_iam_group_membership" "administrators-membership" {
  name = "administrators-membership"
  users = [
    "${aws_iam_user.marusich.name}",
    "${aws_iam_user.civodul.name}",
    "${aws_iam_user.rekado.name}",
  ]
  group = "${aws_iam_group.administrators.name}"
}

# The administrators themselves.

# Note that if we don't set force_destroy to true, then Terraform
# might fail to delete the user when we remove it: See:
# https://github.com/hashicorp/terraform/issues/8621

# Chris Marusich <cmmarusich@gmail.com>

resource "aws_iam_user" "marusich" {
  name = "marusich"
  force_destroy = true
}
resource "aws_iam_access_key" "marusich-access-key-1" {
  user = "${aws_iam_user.marusich.name}"
  pgp_key = "${var.pgp_key_marusich}"
}
resource "aws_iam_user_login_profile" "marusich-login-profile" {
  user    = "${aws_iam_user.marusich.name}"
  pgp_key = "${var.pgp_key_marusich}"
}

output "marusich-name" {
  value = "${aws_iam_user.marusich.name}"
}
output "marusich-password" {
  value = "${aws_iam_user_login_profile.marusich-login-profile.encrypted_password}"
}
output "marusich-access-key-1-id" {
  value = "${aws_iam_access_key.marusich-access-key-1.id}"
}
output "marusich-access-key-1-secret" {
  value = "${aws_iam_access_key.marusich-access-key-1.encrypted_secret}"
}

# Ludovic Courtès <ludo@gnu.org>

resource "aws_iam_user" "civodul" {
  name = "civodul"
  force_destroy = true
}
resource "aws_iam_access_key" "civodul-access-key-1" {
  user = "${aws_iam_user.civodul.name}"
  pgp_key = "${var.pgp_key_civodul}"
}
resource "aws_iam_user_login_profile" "civodul-login-profile" {
  user    = "${aws_iam_user.civodul.name}"
  pgp_key = "${var.pgp_key_civodul}"
}

output "civodul-name" {
  value = "${aws_iam_user.civodul.name}"
}
output "civodul-password" {
  value = "${aws_iam_user_login_profile.civodul-login-profile.encrypted_password}"
}
output "civodul-access-key-1-id" {
  value = "${aws_iam_access_key.civodul-access-key-1.id}"
}
output "civodul-access-key-1-secret" {
  value = "${aws_iam_access_key.civodul-access-key-1.encrypted_secret}"
}

# Ricardo Wurmus <rekado@elephly.net>

resource "aws_iam_user" "rekado" {
  name = "rekado"
  force_destroy = true
}
resource "aws_iam_access_key" "rekado-access-key-1" {
  user = "${aws_iam_user.rekado.name}"
  pgp_key = "${var.pgp_key_rekado}"
}
resource "aws_iam_user_login_profile" "rekado-login-profile" {
  user    = "${aws_iam_user.rekado.name}"
  pgp_key = "${var.pgp_key_rekado}"
}

output "rekado-name" {
  value = "${aws_iam_user.rekado.name}"
}
output "rekado-password" {
  value = "${aws_iam_user_login_profile.rekado-login-profile.encrypted_password}"
}
output "rekado-access-key-1-id" {
  value = "${aws_iam_access_key.rekado-access-key-1.id}"
}
output "rekado-access-key-1-secret" {
  value = "${aws_iam_access_key.rekado-access-key-1.encrypted_secret}"
}
