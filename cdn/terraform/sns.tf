# SNS

# Email subscriptions cannot be managed via Terraform.  Therefore, any
# email subscriptions must be configured manually.  See:
# https://www.terraform.io/docs/providers/aws/r/sns_topic_subscription.html
# https://docs.aws.amazon.com/sns/latest/dg/sns-getting-started.html#SubscribeTopic
resource "aws_sns_topic" "guix-billing-alarms" {
  name = "guix-billing-alarms"
}
