# ACM

# Terraform will create this certificate automatically.  However, to
# validate it requires one-time manual action outside of Terraform.
# This is because there don't seem to be any Terraform providers that
# are compatible with our DNS registrar.  To create and validate the
# certificate, you must do the following in order:
#
#   - Run "terraform apply" to create the certificate.
#   - Run "terraform show" to view the CNAME record ACM needs you to
#     create in order to prove domain ownership.
#   - Manually create the CNAME record.
#   - Run "terraform apply" or "terraform plan" again to ensure the
#     resources are in their final, correct state.
#   - Run "terraform show" to verify that the certificate is now
#     listed as "validated".
#   - Repeat the last two steps until the validation succeeds.
#
# This only has to be done once.  For more information, see:
# https://docs.aws.amazon.com/acm/latest/userguide/gs-acm-validate-dns.html
#
# Finally, since we intend to use this certificate with our CloudFront
# distribution, we must create it in the us-east-1 region.  See:
# https://docs.aws.amazon.com/acm/latest/userguide/acm-services.html
# https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/cnames-and-https-requirements.html
resource "aws_acm_certificate" "charlie-certificate" {
  domain_name       = "ci.guix.gnu.org"
  validation_method = "DNS"
  lifecycle {
    create_before_destroy = true
  }
}
