# S3

# DO NOT DELETE THIS BUCKET!  This bucket contains the Terraform
# state, shared by all Terraform users in the Guix project.  In
# addition, the s3 backend will not function without it.
resource "aws_s3_bucket" "guix-terraform-state" {
  bucket = "guix-terraform-state"
  # Access should be granted via IAM policies.
  acl = "private"
  # This allows us to recover state if something ever goes wrong - as
  # long as we do so within the time period specified by our lifecycle
  # policy (see below).  For details, see:
  # https://docs.aws.amazon.com/AmazonS3/latest/dev/DeletingObjectVersions.html
  versioning {
    enabled = true
  }
  # When we destroy the bucket, destroy all objects first so that the
  # bucket deletion succeeds.  Of course, you should think twice
  # before deleting this bucket!
  force_destroy = true
  # Encrypt data at rest using S3's server side encryption.  See:
  # https://docs.aws.amazon.com/AmazonS3/latest/dev/serv-side-encryption.html
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  # The intent of this rule is to retain the current version and zero
  # or more recent non-current versions, while preventing the size of
  # the bucket from growing out of hand.
  lifecycle_rule {
    id = "clean-up"
    enabled = true
    # It seems unlikely that Terraform would use multi-part uploads to
    # upload the state, since the state is small, but just in case,
    # let's automatically abort any stuck multi-part uploads.
    abort_incomplete_multipart_upload_days = 7
    # Clean up old non-current versions.
    noncurrent_version_expiration {
      days = 14
    }
  }
  lifecycle {
    prevent_destroy = true
  }
}
