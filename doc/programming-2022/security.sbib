(article lamb2021:reproducible
  (author "Chris Lamb, Stefano Zacchiroli")
  (title "Reproducible Builds: Increasing the Integrity of Software Supply Chains")
  (publisher "IEEE Computer Society")
  (year "2022")
  (month "March")
  (volume "39")
  (number "2")
  (pages "62–70")
  (issn "0740-7459")
  (doi "10.1109/MS.2021.3073045")
  (journal "IEEE Software"))

(inproceedings samuel2010:survivable
  (author "Justin Samuel, Nick Mathewson, Justin Cappos, Roger Dingledine")
  (title "Survivable Key Compromise in Software Update Systems")  ;TUF
  (year "2010")
  (isbn "9781450302456")
  (publisher "Association for Computing Machinery")
  (address "New York, NY, USA")
  (url "https://doi.org/10.1145/1866307.1866315")
  (doi "10.1145/1866307.1866315")
  (booktitle "Proceedings of the 17th ACM Conference on Computer and Communications Security")
  (pages "61–72")
  (numpages "12")
  (keywords "authentication, revocation, software updates, key management, delegation, threshold signatures, key compromise")
  (location "Chicago, Illinois, USA")
  (series "CCS '10"))

(misc cappos2020:tuf-spec
  (author "Justin Cappos, Trishank Karthik Kuppusamy, Joshua Lock, Marina Moore, Lukas Pühringer")
  (title "The Update Framework Specification")
  (year "2020")
  (month "December")
  (note "Last accessed June 2022")
  (url "https://github.com/theupdateframework/specification/"))

(inproceedings mehnert2016:conex
  (author "Hannes Mehnert, Louis Gesbert")
  (title "Conex — establishing trust into data repositories")
  (booktitle "Proceedings of the ACM OCaml 2016 Workshop")
  (conf "OCaml 2016")
  (year "2016")
  (month "September")
  (url "https://github.com/hannesm/conex-paper/raw/master/paper.pdf"))

(misc nixos2021:signed-commits
  (author "Nix contributors")
  (url "https://github.com/NixOS/rfcs/pull/100")
  (title "[RFC 0100] Sign commits")
  (year "2021")
  (month "August")
  (note "Last accessed June 2022"))
  
(misc dolstra2019:flake-auth
  (author "Eelco Dolstra")
  (url "https://github.com/NixOS/nix/issues/2849")
  (title "Flake authentication")
  (year "2019")
  (month "March")
  (note "Last accessed June 2022"))
  
(misc brew2022:github
  (url "https://github.com/Homebrew/brew")
  (title "Brew source code")
  (year "2022")
  (month "January")
  (author "Brew contributors")
  (note "Last accessed June 2022"))

(misc condaforge2022:web
  (url "https://conda-forge.org/")
  (title "CONDA-Forge Web site")
  (year "2022")
  (month "January")
  (note "Last accessed June 2022")
  (author "CONDA-Forge contributors"))
  
(misc freebsd2022:handbook
  (url "https://docs.freebsd.org/en/books/handbook/")
  (title "FreeBSD Handbook")
  (year "2022")
  (month "January")
  (note "Last accessed June 2022")
  (author "The FreeBSD Documentation Project"))

(misc pkgsrc2022:guide
  (url "https://www.netbsd.org/docs/pkgsrc/")
  (title "The pkgsrc Guide")
  (year "2022")
  (month "January")
  (note "Last accessed June 2022")
  (author "The pkgsrc Developers"))
  
(misc gentoo2022:portage-security
  (url "https://wiki.gentoo.org/wiki/Portage_Security")
  (title "Portage Security")
  (year "2022")
  (month "January")
  (note "Last accessed June 2022")
  (author "Gentoo developers"))

(inproceedings torresarias2016:omitting
(author "Santiago Torres-Arias, Anil Kumar Ammula, Reza Curtmola, Justin Cappos")
(title "On Omitting Commits and Committing Omissions: Preventing Git Metadata Tampering That (Re)introduces Software Vulnerabilities")
(booktitle "25th USENIX Security Symposium")
(year "2016")
(isbn "978-1-931971-32-4")
(address "Austin, TX")
(pages "379--395")
(url "https://www.usenix.org/conference/usenixsecurity16/technical-sessions/presentation/torres-arias")
(publisher "USENIX Association")
(month "August"))

(inproceedings kuppusamy2017:mercury
  (author "Trishank Karthik Kuppusamy, Vladimir Diaz, Justin Cappos")
  (title "Mercury: Bandwidth-Effective Prevention of Rollback Attacks Against Community Repositories")
  (booktitle "2017 USENIX Annual Technical Conference (USENIX ATC 17)")
  (year "2017")
  (isbn "978-1-931971-38-6")
  (address "Santa Clara, CA")
  (pages "673--688")
  (url "https://www.usenix.org/conference/atc17/technical-sessions/presentation/kuppusamy")
  (publisher "USENIX Association")
  (month "July"))

(inproceedings torresarias2019:intoto
(author "Santiago Torres-Arias, Hammad Afzali, Trishank Karthik Kuppusamy, Reza Curtmola, Justin Cappos")
(title "in-toto: Providing farm-to-table guarantees for bits and bytes")
(booktitle "28th USENIX Security Symposium")
(year "2019")
(isbn "978-1-939133-06-9")
(address "Santa Clara, CA")
(pages "1393--1410")
(url "https://www.usenix.org/conference/usenixsecurity19/presentation/torres-arias")
(publisher "USENIX Association")
(month "Aug"))

(inproceedings cappos2008:attacks
(author "Justin Cappos, Justin Samuel, Scott Baker, John H. Hartman")
(title "A Look in the Mirror: Attacks on Package Managers")
(year "2008")
(isbn "9781595938107")
(publisher "Association for Computing Machinery")
(address "New York, NY, USA")
(url "https://doi.org/10.1145/1455770.1455841")
(doi "10.1145/1455770.1455841")
(booktitle "Proceedings of the 15th ACM Conference on Computer and Communications Security")
(pages "565–574")
(numpages "10")
(keywords "package management, replay attack, mirrors")
(location "Alexandria, Virginia, USA")
(series "CCS '08"))

(article hinsen2020:staged-computation
  (author "Konrad Hinsen")
  (journal "Computing in Science Engineering")
  (title "Staged Computation: The Technique You Did Not Know You Were Using")
  (year "2020")
  (volume "22")
  (number "4")
  (pages "99--103")
  (doi "10.1109/MCSE.2020.2985508")
  (url "https://dx.doi.org/10.1109/MCSE.2020.2985508"))

(misc janneke:mes-web
  (title "GNU Mes web site")
  (author "Jan Nieuwenhuizen")
  (url "https://gnu.org/software/mes")
  (note "Last accessed June 2022")
  (year "2021"))
  
(misc janneke2020:bootstrap
  (title "Guix Further Reduces Bootstrap Seed to 25%")
  (author "Jan Nieuwenhuizen")
  (year "2020")
  (month "June")
  (note "Last accessed June 2022")
  (url "https://guix.gnu.org/en/blog/2020/guix-further-reduces-bootstrap-seed-to-25/"))

(misc janneke2021:full-source-bootstrap
  (url "https://fosdem.org/2021/schedule/event/gnumes/")
  (title "GNU Mes — the Full Source Bootstrap")
  (author "Jan Nieuwenhuizen")
  (note "Last accessed June 2022")
  (year "2021")
  (month "February"))

(misc janneke2022:full-source-bootstrap
  ;; (url "https://archive.softwareheritage.org/browse/revision/eeba993f570483345acd59c2d8709fd5d7ee7ae7/log/")
  (url "https://issues.guix.gnu.org/55227")
  (title "Patch series implementing the “full-source bootstrap”")
  (author "Jan Nieuwenhuizen")
  (note "Last accessed June 2022")
  (year "2022")
  (month "May"))

(article thompson1984:trusting-trust
  (author "Ken Thompson")
  (title "Reflections on Trusting Trust")
  (year "1984")
  (issue_date "Aug 1984")
  (publisher "Association for Computing Machinery")
  (address "New York, NY, USA")
  (volume "27")
  (number "8")
  (issn "0001-0782")
  (url "https://doi.org/10.1145/358198.358210")
  (doi "10.1145/358198.358210")
  (journal "Communications of the ACM")
  (month "August")
  (pages "761--763"))

(inproceedings merkle1980:protocols
  (author "Ralph C. Merkle")
  (institution "ELXSi International")
  (title "Protocols for Public Key Cryptosystems")
  (booktitle "Proceedings of the IEEE Symposium on Security and
Privacy")
  (address "Oakland, CA")
  (pages "122--134")
  (month "April")
  (year "1980")
  (doi "10.1109/SP.1980.10006")
  (url "http://www.merkle.com"))

(inproceedings stevens2017:detection
  (author "Marc Stevens, Daniel Shumow")
  (title "Speeding up Detection of SHA-1 Collision Attacks Using Unavoidable Attack Conditions")
  (year "2017")
  (isbn "978-1-931-97140-9")
  (publisher "USENIX Association")
  (address "USA")
  (booktitle "Proceedings of the 26th USENIX Conference on Security Symposium")
  (pages "881–897")
  (numpages "17")
  (location "Vancouver, BC, Canada")
  (series "SEC'17"))

(inproceedings stevens2017:collision
  (author "Marc Stevens, Elie Bursztein, Pierre Karpman, Ange Albertini, Yarik Markov")
  (editor "Katz, Jonathan
  and Shacham, Hovav")
  (title "The First Collision for Full SHA-1")
  (booktitle "Advances in Cryptology -- CRYPTO 2017")
  (year "2017")
  (publisher "Springer International Publishing")
  (isbn "978-3-319-63687-0")
  (doi "10.1007/978-3-319-63688-7_19")
  ;;(address "Cham")
  (pages "570--596")
  (isbn "978-3-319-63688-7"))

(inproceedings leurent2020:shambles
  (author "Gaëtan Leurent, Thomas Peyrin")
  (title "SHA-1 is a Shambles: First Chosen-Prefix Collision on SHA-1 and Application to the PGP Web of Trust")
  (booktitle "29th USENIX Security Symposium (USENIX Security 20)")
  (year "2020")
  (isbn "978-1-939133-17-5")
  (pages "1839--1856")
  (url "https://www.usenix.org/conference/usenixsecurity20/presentation/leurent")
  (publisher "USENIX Association")
  (month "August"))

(misc callas98:rfc2440-openpgp
  (author "Jon Callas, Lutz Donnerhacke, Hal Finney, Rodney
Thayer")
  (title "OpenPGP Message Format (RFC 2440)")
  (institution "Internet Engineering Task Force (IETF)")
  (year "1998")
  (month "November")
  (note "Last accessed June 2022")
  (url "https://tools.ietf.org/html/rfc2440"))

(misc callas2007:rfc4880-openpgp
  (author "Jon Callas, Lutz Donnerhacke, Hal Finney, Rodney Thayer")
  (title "OpenPGP Message Format (RFC 4880)")
  (institution "Internet Engineering Task Force (IETF)")
  (year "2007")
  (month "November")
  (url "https://tools.ietf.org/html/rfc4880"))

(misc denis2021:minisign-web
  (author "Frank Denis")
  (title "Minisign — Simple tool to sign files and verify signatures")
  (url "https://jedisct1.github.io/minisign")
  (note "Last accessed June 2022")
  (year "2021"))

(misc git2021:hash-transition
  (author "Git project")
  (title "Hash Function Transition")
  (year "2021")
  (note "Last accessed June 2022")
  (url "https://git-scm.com/docs/hash-function-transition/"))
  
(misc bitcoin2021:verify-commits
  (author "BitCoin Core project")
  (title "Tooling for verification of PGP signed commits")
  (year "2021")
  (note "Last accessed June 2022")
  (url "https://github.com/bitcoin/bitcoin/tree
/d4b3dc5b0a726cc4cc7a8467be43126e78f841cf/contrib/verify-commits"))

(misc fsf2010:compromise
  (author "Free Software Foundation")
  (title "Savannah and www.gnu.org Downtime")
  (year "2010")
  (note "Last accessed June 2022")
  (url "https://www.fsf.org/blogs/sysadmin/savannah-and-www.gnu.org-downtime"))

(misc sigstore2021:web
  (author "The Linux Foundation")
  (title "sigstore, a new standard for signing, verifying and protecting software")
  (year "2021")
  (note "Last accessed June 2022")
  (url "https://www.sigstore.dev/"))

(misc huseby2021:git-crypto
  (author "Dave Huseby, Git contributors")
  (title "Git Cryptography Protocol")
  (year "2021")
  (note "Last accessed June 2022")
  (url "https://github.com/cryptidtech/git-cryptography-protocol"))

(misc git2021:relnotes
  (author "Git contributors")
  (title "Git 2.34 Release Notes")
  (year "2021")
  (month "November")
  (note "Last accessed June 2022")
  (url "https://raw.githubusercontent.com/git/git/master/Documentation
/RelNotes/2.34.0.txt"))

(misc courtes2016:authentication
  (author "Ludovic Courtès, GNU Guix contributors")
  (year "2016")
  (month "May")
  (url "https://issues.guix.gnu.org/22883")
  (note "Last accessed June 2022")
  (title "Trustable “guix pull”"))

(misc devos2021:diverted
  (author "Maxime Devos")
  (year "2021")
  (month "May")
  (url "https://issues.guix.gnu.org/48146")
  (note "Last accessed June 2022")
  (title "Getting diverted to non-updated branches: a limitation of the authentication mechanism?"))

(article peisert2021:solarwinds
  (author "Sean Peisert, Bruce Schneier, Hamed Okhravi, Fabio Massacci, Terry Benzel, Carl Landwehr, Mohammad Mannan, Jelena Mirkovic, Atul Prakash, James Bret Michael")
  (journal "IEEE Security & Privacy")
  (title "Perspectives on the SolarWinds Incident")
  (year "2021")
  (volume "19")
  (number "02")
  (issn "1558-4046")
  (pages "7-13")
  (doi "10.1109/MSEC.2021.3051235")
  (publisher "IEEE Computer Society")
  (address "Los Alamitos, CA, USA")
  (month "March"))
  
(misc biden2021:executive-order
  (author "Joseph Biden")
  (year "2021")
  (month "May")
  (title "Executive Order on Improving the Nation’s Cybersecurity")
  (note "Last accessed June 2022")
  (url "https://www.whitehouse.gov/briefing-room/presidential-actions/2021/05/12/executive-order-on-improving-the-nations-cybersecurity/"))

(misc google2021:slsa
  (author "Google, Inc.")
  (year "2021")
  (month "June") ;see https://security.googleblog.com/2021/06/introducing-slsa-end-to-end-framework.html
  (title "Supply-chain Levels for Software Artifacts (SLSA)")
  (note "Last accessed June 2022")
  (url "https://slsa.dev/"))

(misc github2021:verify-commits
  (author "GitHub, Inc.")
  (year "2021")
  (title "Managing commit signature verification")
  (note "Last accessed June 2022")
  (url "https://docs.github.com/en/github/authenticating-to-github/managing-commit-signature-verification"))

(misc gitlab2021:verify-commits
  (author "GitLab, Inc.")
  (year "2021")
  (title "Signing commits with GPG")
  (note "Last accessed June 2022")
  (url "https://docs.gitlab.com/ce/user/project/repository/gpg_signed_commits/"))

(article courant2022:ocamlboot
  (author "Nathanaëlle Courant, Julien Lepiller, Gabriel Scherer")
  (year "2022")
  (month "February")
  (title "Debootstrapping Without Archeology: Stacked Implementations in Camlboot")
  (journal "Programming Journal")
  (issue "3")
  (volume "6")
  (article "13")
  (doi "10.22152/programming-journal.org/2022/6/13"))

(misc wurmus2017:jdk-bootstrap
  (author "Ricardo Wurmus")
  (year "2017")
  (month "June")
  (title "Building the JDK without Java")
  (note "Last accessed June 2022")
  (url "https://www.freelists.org/post/bootstrappable/Building-the-JDK-without-Java"))

(misc milosavljevic2018:rust-bootstrap
  (title "Bootstrapping Rust")
  (author "Danny Milosavljevic")
  (year "2018")
  (month "December")
  (note "Last accessed June 2022")
  (url "https://guix.gnu.org/en/blog/2018/bootstrapping-rust/"))

(misc wurmus2022:bootstrappable-web
  (title "Bootstrappable Builds")
  (author "Ricardo Wurmus, Ludovic Courtès, Paul Wise, Gábor Boskovits,
rain1, Matthew Kraai, Julien Lepiller, Jeremiah Orians, Jelle Licht,
Jan Nieuwenhuizen")
  (year "2022")
  (note "Last accessed June 2022")
  (url "https://bootstrappable.org/"))

(software courtes2022:programming-artifact
  (author "Ludovic Courtès, GNU Guix contributors")
  (title "Accepted Artifact for “Building a Secure Software Chain with GNU Guix”")
  (month "June")
  (year "2022")
  (publisher "Zenodo")
  (version "1.0")
  (doi "10.5281/zenodo.6581453")
  (url "https://doi.org/10.5281/zenodo.6581453"))

#|
(defun skr-from-bibtex ()
  "Vaguely convert the BibTeX snippets after POINT to SBibTeX."
  (interactive)
  (while (re-search-forward "\\([a-z_-]+\\) *= *[{\"]\\([^}\"]+\\)[}\"] *, *$" nil nil)
    (replace-match "(\\1 \"\\2\")")))
|#
