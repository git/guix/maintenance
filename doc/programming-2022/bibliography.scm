;; Bibliography via BibTeX.  Taken from (skribilo packages lncs).

(define (bib-entry-template kind)
  ;; Return a template for KIND that includes a DOI.
  ;; Note: This uses the API of Skribilo 0.9.5 and earlier.
  (let ((template (make-bib-entry-template/default kind)))
    (cond ((eq? 'misc kind)
           `(,@template (" " note ".")))
          ((eq? 'software kind)
           `(author ".  " title ".  " publisher ", " month " " year
                    ".  " (if url url ("  DOI:" doi))))
          (else
           `(,@template (if doi (" DOI: " doi) (" ISBN: " isbn)))))))

(when-engine-is-loaded 'latex
  (lambda ()
    (let ((latex (find-engine 'latex)))

      ;; Use the native bibliography system (BibTeX).

      (markup-writer 'bib-ref latex
         :options '(:text :bib)
         :action (lambda (n e)
                   (let ((entry (handle-ast (markup-body n))))
                     (format #t "\\cite{~a}" (markup-ident entry)))))

      (markup-writer 'bib-ref+ latex
         :options '(:text :bib :sort-bib-refs)
         :action (lambda (n e)
                   (let ((entries   (map (lambda (bib-ref)
                                           (handle-ast (markup-body bib-ref)))
                                         (markup-body n)))
                         (sort-proc (markup-option n :sort-bib-refs)))
                     (format #t "\\cite{~a}"
                             (string-join (map markup-ident
                                               (if (procedure? sort-proc)
                                                   (sort entries sort-proc)
                                                   entries))
                                          ",")))))

      (markup-writer '&the-bibliography latex
         :before (lambda (n e)
                   (let ((count (length (markup-body n))))
                     (format #t "\\begin{thebibliography}{~a}\n"
                             count)))
         :after  "\\end{thebibliography}\n")

      (markup-writer '&bib-entry-body
         :action (lambda (n e)
                   (let* ((kind (markup-option n 'kind))
                          (template (bib-entry-template kind)))
                     (output-bib-entry-template n e template))))

      (markup-writer '&bib-entry latex
         :action (lambda (n e)
                   (display "%\n\\bibitem[")
                   (output (markup-option n :title) e)
                   (format #t "]{~a}\n" (markup-ident n))
                   (output n e (markup-writer-get '&bib-entry-body e)))
         :after "\n%\n")

      ;; Journal and book titles must not be italicized.
      (markup-writer '&bib-entry-booktitle latex
         :action (lambda (n e)
                   (let ((title (markup-body n)))
                     (evaluate-document title e))))

      (markup-writer '&bib-entry-journal latex
         :action (lambda (n e)
                   (evaluate-document (markup-body n) e))))))

;;; Local Variables:
;;; eval: (put 'markup-writer 'scheme-indent-function 2)
;;; End:
