;;!begin-initrd
(expression->initrd
 (with-imported-modules (source-module-closure
                         '((gnu build linux-boot)))
   #~(begin
       (use-modules (gnu build linux-boot))

       (boot-system #:mounts '#$file-systems
                    #:linux-modules '#$linux-modules
                    #:linux-module-directory '#$kodir)))
;;!end-initrd
