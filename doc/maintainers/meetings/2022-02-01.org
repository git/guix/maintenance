* Maintainers meeting
** Schedule
The 1st of February from 14:00 CET to 15:00 CET on Jami.

** Agenda
The proposed points to discuss are:

- Installation of new SSDs (done) and migration of file system (in
  process).

- Outreachy sponsorship via Guix Europe

- Tasks for the release

** Notes
Meeting was held on Jami. Efraim, Mathieu and Maxim were present.

- Berlin has been copying /var/cache for days to the new Btrfs array
  (a subvolume at /mnt/btrfs-pool/@cache); at this rate it will take
  many more days before the migration can be completed.

*Action* Suggested by Efraim: use what we have as the new /var/cache
 already (shadowing current one); perhaps rebuilding NARs is actually
 faster than copying them from the old array.  And continue copying
 stuff with rsync to it without the --delete option.

*Action* Investigate the impact of not baking gzip or lzip NARs on the
build farm to reduce the amount of held NARs, which is more than 10
TiB currently.

*Action* Review if the nar-herder could be put to use for the build
farm.

- We have the option to move partial sums from the Guix FSF balance to
  the banking account held by Guix Finance, which should solve the
  Outreachy impasse.

- Tasks for the release were discussed; the following action items
  were noted:

*Action* Investigate the Cuirass workers crashing on ARM
(https://issues.guix.gnu.org/52182).

*Action* Resolve the guix evaluation problem
(https://issues.guix.gnu.org/53463).

*Action* Find and fix high impact non-x86_64 packages build failures.

Carried on from last meeting:

*Action* Check the release machinery status.

*Action* Draft the list of changes for the NEWS file and the blog
article.

*Action* Check that every test and system-test is passing. Merge the
wip-harden-installer branch and call for an installer test.

Other points discussed:

- Consider the use of Gitea (Efraim is almost done packaging it) to
  ease code review/collaboration.
