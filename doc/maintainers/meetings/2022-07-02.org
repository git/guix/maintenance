* Maintainers meeting
** Schedule
The 2nd of August from 14:00 CEST to 15:15 CET on Jami & Jitsi.

** Agenda
No fixed agenda -- discuss an action to take.

** Notes
Meeting was held on Jami & Jitsi. Efraim, Maxim and Tobias were present.

- The infrastructure status was discussed.  Berlin is now accessible
  via iDRAC, and its root file system was migrated to the 100 TiB SAN
  storage, to resolve inodes exhaustion problem that was occurring on
  the older ext4 partition.  =/var/cache= and =/home= which are still
  on the RAID 10 SSDs.  To boot from the SAN, the store items
  referenced by grub.cfg needed to be copied to /boot.

*Action* Migrate the remaining =/var/cache= and =/home= to @cache and
@home Btrfs submodules on the SAN. [Tobias]

*Action* Implement a 'stand-alone?' bootloader-configuration option
that would take care of copying grub.cfg things to =/boot=
automatically. [Maxim?]

- Discussed re-entering release mode.

*Action* Expound NEWS file. [All]

*Action* Exercise release machinery, e.g. ~make release~ and verify
documented process in =doc/release.org= [All]

- Also discussed: RISC-V support being worked on by Efraim, and the
  process of granting commit access.
