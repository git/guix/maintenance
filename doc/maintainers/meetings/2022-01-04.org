* Maintainers meeting
** Schedule
The 4th of January from 14:00 CET to 15:00 CET on Berlin's Jami instance.

** Agenda
The proposed points to discuss are:

- Maintainer rotation: welcome Efraim! Are we fine with the drafted article,
  when should we publish it? Are we fine with 4 co-maintainers or should we
  try to expand the collective?

- Fiscal sponsorship: shall we stick to the FSF as our fiscal sponsor? What
  are the possible alternatives? How can we keep financing Outreachy
  internships?

- Infrastructure point: how can we deal with Berlin IO issues, what are the
  actions left to ensure redundancy for the website and the substitutes?

- Bordeaux build farm: are we fine with its extension? Should we support Chris
  expenses, in particular with external VPS (Hetzner)?

- Formalizing teams: how can we build on Ludo proposal to formalize teams?

- Release 1.4.0: what could be the target and how do we split the work?

- Making the maintainers meetings periodical: do we agree on that, would a 1h
  meeting on a monthly basis be alright? Where should we host the meeting
  agenda and minutes, should they be public?

** Notes

Meeting was held on Jami. Efraim, Ludo, Marius, Mathieu, Maxim and Tobias were
present.

- Maintainer rotation: the maintainer committee will now be composed of Efraim,
Mathieu, Maxim and Tobias. We are fine with four co-maintainers, but not
opposed to a fifth candidature.

*Action*: Maxim will edit the draft blog post and submit it.

- Fiscal sponsorship: we are currently sponsored by the FSF, with no direct
access to our account. The FSF takes 10% of the incoming donations.
We need to send them an invoice to be reimbursed. This situation is complex to
deal with because of the FSF opacity.

We also cannot participate to Outreachy anymore as Outreachy is boycotting the
FSF. Each internship costs 6000 USD. It was mentioned that none of the interns
were active in the Guix community anymore. The Outreach results were also
often disappointing in term of technical result. However, participating is
more about promoting diversity.

*Action*: Schedule a meeting with Guix Europe to discuss it.

- Infrastructure point: we should try to upgrade Berlin HDD array to an SSD
array of ~30TiB. Using RAID10 is not really necessary. We should also increase
Bordeaux storage by at least 1.3TiB to ensure substitutes redundancy.

*Action*: Maxim will look for SSD hardware for Berlin while Ludo will do the
 same for Bordeaux.
*Action*: Mathieu will schedule a new infrastructure hackathon.

- Bordeaux build farm: reimbursing Chris Hetzner expenses is problematic as we
  would like to keep a tight control on our build farms.

- Formalizing teams: this item was not discussed. We will introduce the
  subject again, probably during the Guix Days.

- Release 1.4.0: a specific branch has been started and contains some
  world-rebuilding changes. It would be preferable to merge it on master
  first, quite soon, so that we can get some feedback. We can then tag a
  1.4.0-rc1 release.

For the release, we can then distinguish three separate tasks:

    1. Check the release machinery status.
    2. Draft the list of changes for the NEWS file and the blog article.
    3. Check that every test and system-test is passing. Merge the
       wip-harden-installer branch and call for an installer test.

*Action*: Mathieu will send an email on guix-devel to recruit some volunteers
 for those tasks.

It was also proposed for each frozen branch (core-updates, version-xxx), to
send some weekly recap containing:

    1. The branch target: expected merge date, package updates and fixes.
    2: The branch status: opened Debbugs issues for that branch, Cuirass and
    weather reports.

Those reports could maybe be automatically generated. It would be nice to
start producing some for the upcoming 1.4.0 branch release.

- Making the maintainers meeting periodical: it was agreed to have monthly
  meetings, every first Thuesday of the month. The next meeting is scheduled
  the 1 of February at 14:00 CET. The meeting agenda and notes should be made
  public.
