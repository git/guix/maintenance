* Maintainers meeting
** Schedule
The 1st of March from 14:00 CET to 15:00 CET on Jami.

** Agenda
No fixed agenda -- discuss an action to take.

** Notes
Meeting was held on Jami. Efraim, Maxim and Tobias were present.

- Discussed the recent events and what action was appropriate from the
  Guix maintainers.

- Berlin file system migration status: awaiting on-site person
  availability to try a reboot again.  There was a problem where the
  devices would not be available at the time 'btrfs device scan' was
  run by the init ram disk, which caused the root file system to be
  mounted in a degraded state.  It is assumed this is due to our init
  ram disk not supporting hardware events/synchronization.  Using
  rootdelay=20 or alternatively passing the
  'device=/dev/sda3,/dev/sdb3,/dev/sdc3,/dev/sdd3,/dev/sde3,/dev/sdf3'
  mount option should be a workaround (which can now be changed easily
  via the rootflags kernel option from the GRUB interface).

*Action* Draft and send an official statement regarding recent events.

*Action* Update our Code of Conduct copy the latest version.

*Action* Add a notice at the top of the Code of Conduct mentioning
issues pertaining to it should be brought up with the maintainers, not
in the other public Guix communication channels.

*Action* Turn any release-related past (uncompleted yet) actions into
 Debbugs issues tagged as blocking the 1.4.0 release issue (#53214).
