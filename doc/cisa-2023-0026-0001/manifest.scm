;; Manifest for Org-generated LaTeX.

(specifications->manifest
 '("rubber"

   "texlive-scheme-basic"
   "texlive-collection-latexrecommended"
   "texlive-collection-fontsrecommended"

   "texlive-libertine"
   "texlive-inconsolata"

   "texlive-wrapfig"
   "texlive-ulem"
   "texlive-capt-of"
   "texlive-hyperref"
   "texlive-upquote"))
