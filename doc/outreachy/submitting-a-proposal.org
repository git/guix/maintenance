* Project details
** Approved license: yes
** No proprietary software: yes
** How long has your team been accepting publicly submitted contributions: More than 2 years
** How many regular contributors does your team have: More than 100 people
** Project title: Desciptive, short, no need to explain it, the long description is for that. 
** Project long description
*** Should provide context, namely at least a link about guix and other relevant software.
*** Should provide a glossay of terms that are stricly related to guix (For example package is a well known term, while substitute might need explanation)
** System requirements
*** General
- A working development setup for the kind of project
*** For projects on guix
- A working installation for guix
- A working development setup for guix
- Having the testsuites run on the checkout
- A working installation of guix

- A working development setup for guix (at least git and a capable text editor)

- Having the guix testsuite run on a current checkout

For general instructions see:

http://guix.gnu.org/en/videos/

For installation instruction see:

The installation from script video here:

http://guix.gnu.org/en/videos/

or the relevant section of the manual here:

https://guix.gnu.org/manual/en/html_node/Installation.html

For instruction on how to build from a checkout see:

https://guix.gnu.org/manual/en/html_node/Building-from-Git.html

For instructions on how to run the tests see:

https://guix.gnu.org/manual/en/html_node/Running-the-Test-Suite.html

For a recommended development setup see:

https://guix.gnu.org/manual/en/html_node/The-Perfect-Setup.html

*** For projects on gnu system
- A working installation of GNU System, either on bare metal, or in a VM
- A working development setup for guix
- Having the testsuites run on the checkout
- (Optional) Having the system tests run on the checkout
- A working installation of guix

- A working development setup for guix (at least git and a capable text editor)

- Having the guix testsuite run on a current checkout

For general instructions see:

http://guix.gnu.org/en/videos/

For installation instruction see:

The installation from script video here:

http://guix.gnu.org/en/videos/

or the relevant section of the manual here:

https://guix.gnu.org/manual/en/html_node/Installation.html

For instruction on how to build from a checkout see:

https://guix.gnu.org/manual/en/html_node/Building-from-Git.html

For instructions on how to run the tests see:

https://guix.gnu.org/manual/en/html_node/Running-the-Test-Suite.html

For a recommended development setup see:

https://guix.gnu.org/manual/en/html_node/The-Perfect-Setup.html

*** For projects independent of guix, but under the community umbrella 
- whatever is required for this project
- whatever is required to perform the integration of the project into guix (most probably this includes everything under *For projects on guix*)

** contributions

Initial contribution possibilities(H4)
Packaging software(H5)
For packaging instructions see:

https://guix.gnu.org/cookbook/en/html_node/Packaging-Tutorial.html#Packaging-Tutorial

and

https://guix.gnu.org/manual/en/html_node/Defining-Packages.html#Defining-Packages

Packaging software is a good first contribution candidate, it is not so hard to do, but ensures that everything is working on the installation, and is a lasting contribution to Guix. Good initial candidate packages are R packages, as Guix includes a well working recursive importer for R packages.

Refining current documentation(H5)
Contributions to the manual, and also to the Guix Cookbook are welcome.

The source files of these are located at:

- doc/guix.texi (a rendered version is here: https://guix.gnu.org/manual/en/guix.html)

- doc/guix-cookbook.texi (a rendered version is here: https://guix.gnu.org/cookbook/en/html_node/index.html)

Working on bugs(H5)
Working on any bug and creating a contibution by that is also a possibility. However, it might be quite hard to estimate the invesment needed to fix a bug. It is recommended to record your first contribution by doing something else instead, but this can make a good later contibution, or if you already have a bug fixed, then feel free to register that as a first contribution.

General Information(H4)
For you initial contributions and system setup you can find general material about guix here:

http://guix.gnu.org/en/videos/
** Repository: https://savannah.gnu.org/git/?group=guix
** Issue tracker: http://issues.guix.gnu.org/
** Newcomer issue tag: easy
** Intern tasks: separate these according to the following structure if possible: Starting tasks, Milestones, Stretch Goals, set these to(H5)
** Intern benefits: please, try to fill this
** Community benefits: please, try to fill this
** Unapproved license description: leave empty
** Proprietary software description: leave empty
** Is your project open to new contributions: My project is open to new conrtibutions.
* Mentor profile: feel free to fill this as it seems to fit. This is essentially your personal inforamtion.
* Project skills: require here only what is needed.
There is one thing I suggest very strongly, and it's good communication skills.
As this is a full remote internship it is of crucial importance to circulate information fast about any blockers,
so that time is not wasted. It is not a problem if a goal is not reached on it's proposed deadline, but you have to
keep track of what is happening, if the problem is on track, but just delayed, and you have to discuss a modified timeline.
* Communication channels
** Channel #1:
*** tool name: IRC
*** URL: irc://irc.freenode.net:6667/#guix
*** Instructions on joining:
If you are asked for username, pick any username! You will not need a password to join this chat.
Please make sure to add a nick and a password to your account, as only registered users are allowed to talk on the channel.
For further information see: https://freenode.net/kb/answer/registration

Please make sure to introduce yourself on this channel.
*** Community norms:
Please make sure to familiarize yourself with the Code of Conduct. You can find a local version in the CODE-OF-CONDUCT file in the source tree.

Please note, that when talking on IRC in general you are addressing everyone on the channel. Use nickname: to direct your message to someone. Most clients have an autocompletion feature when you type a tabulation.

Guix's IRC channel is open to communication in any language people want to engage in, but please remember that your mentors might not understand your language. English is the only language understood by all your mentors and other community members, and is therefore mandatory when you want to communicate results or have questions about the internship.

IRC has the capability to open private communication channels, but experience has shown that communication should be public, unless there is a very compelling (most of the time legal or security) reason to make it private.

Please keep communication public, so that the community as a whole can help.
*** Communication tool documentation URL: https://freenode.net/kb/all
** Channel #2:
*** tool name: mailing list
*** URL:_https://lists.gnu.org/mailman/listinfo/guix-devel
*** Instructions on joining
Please visit the mailing list subscription page, then fill in the form.

The only required field is the email address.

Click subscribe to send the subscription request.

You will soon receive a 'welcome to the mailing list' email to the specified address.

Please make sure that after subscribing to the list you write an email introducing yourself.
*** Community norms
Please CC your mentors on the mails sent to the list.

Please also make sure that when answering to mail you answer to all.

Please make sure, that when answering emails you do not top post, and you answer questions and add remarks right after quoting the relevant part of the original message. You can remove long parts of messages you are not answering to.

Please make sure to familiarize yourself with the Code of Conduct. You can find a local version in the CODE-OF-CONDUCT file in the source tree.

Experience has shown that communication should be public, unless there is a very compelling (most of the time legal or security) reason to make it private.

Please keep communication public, so that the community as a whole can help.
*** Documentation URL: leave empty 
