# CLI of Guix

## How a perfect CLI would look like?
* consistent
* self-explaining
* intuitive
* mapping between API and CLI (e.g. easier REPL)
* output can be parsed (composability)
* flexible outputs
* override defaults (aliases or extensions)
* should be fast
* auto completion, support for tabbing

## What's the current state (good or bad)?
* consistency in conventions (mostly)
* fast tab completion
* 39 top-level commands (`guix top-level-command`)
* guix refresh / lint / style / challenge / download / hash are toplevel but really specific
* Some options are not consistent between different commands
  * `guix system list-generations` vs `guix pull --list-profile`
* the `guix import` command is hard coded so we can't plug new importers with a channel for instance
* the order of channel specification is important in regard to definitions (might not be the case anymore)
* there's a common confusion between the name of the package and the name of the variable that holds the package

## What we can improve
* make guix extensions / modules more accessible
  * there's something about that on a git repo of Andreas
* package name writing differs between CLI and code (e.g. "glibc:debug" vs glibc `("debug"))
* `guix shell --development spec` should accept a = sign (`guix shell --development=spec`)
* Having a equivalent of the CLI example for the REPL in documentation
