# Hurd + Guix

* a few people in the room have run the Hurd before, even less on baremetal machines

## Challenges
* hurd on bare metal is difficult -> missing drivers
* hurd supports only small disks (up to 100GB or so)
* mach provides less then Linux kernel (file system, network etc) -> bootstrap is hard

## Upstream status
* 64bit (x86) support WIP
* support for SMP (multi-threading)
* port to aarch64 was started
* Debian is mostly used for developing/integrated
* activity increased a lot in the last 2 years (1 person -> couple contributors)
* patches are pushed to glibc upstream

## Guix on hurd
* netdde (Linux kernels built for mach) -> Guix integration non-working
* rumpdisk, disk drivers from NetBSD -> integrated in Guix already
* rumpnet (for WiFi, network drivers etc.)
* Guix with Hurd image available https://guix.gnu.org/en/download/latest/ including:
  * netdde and rumpdisk included
  * shepherd
* glibc in core-updates contains many improvements for the Hurd
  * 2.39 brings even more fixes, but maybe to late for core-updates
* Bordeaux and Berlin build farms build packages for i586-gnu
  * only master, core-updates currently disabled
* `guix system reconfigure` broken on core-updates
* rust@1.75 is cross-compilable from Linux
* Mes builds on the hurd, stage0 misses support for mach+hurd
  * so no bootstrap of Guix yet

## Ideas
* implement /gnu/store as a hurd translator
* install Hurd via Guix installer
* writing Guile bindings for Hurd translator
* work closer together with Hurd devs
* mach kernel can be "stretched" over network -> build "clusters"

* how can users/devs start with Hurd
  * run either via childhurd or Hurd VM
  * childhurd allows offloading from Guix@Linux
  * write a simple translator
* what would help the Hurd ecosystem
  * fix failing packages
* empower users (non-admins) -> idea shared by Hurd and Guix
  * "Linux+systemd+rust" is making things complex (high entry barrier)
