# Infrastructure

## Why (ideal world)

Areas where we could improve:

* serve the whole world web
* redundancy
* low maintenance (labour), but maintained, supported and observed
* understood, documented and transparent
* sustainable (financially and environmental)

Other (okeish) things:

* secure and trusted
* privacy considerations
* scaleable
* no cost to use
* ethical
* hosted or project owned hardware?
* minimal but futureproof
* provides (substitutes etc.)
* efficient operations (bandwith)

## What is our infrastructure
* Build farms
* Security and trust
* Services
  * QA
  * issues/bug tracker
  * git
  * website
  * package search
  * mailing lists
  * DNS
  * IRC
* substitutes
* sysadmin
  * updates
  * software e.g. guix deploy
  * support
  * incident response
* money, donations
* location, hosting
* virtual/physical hardware
* state, storage

## Ideas for improvement
* mirroring
  * use a CDN
  * peer-to-peer hosting
* equivalence ignoring reference hashes
* substitutes
  * storage for the Bordeaux build farm
  * how long do we store them (scientific community "loves" substitutes for the past)
  * prepare for the loss of MDC hosting (Berlin build farm)
* pay/financially support people providing hardware

## Discussion whiteboard

[Discussion whiteboard](2024-guix-days-infrastructure-discussion-whiteboard.jpg)
