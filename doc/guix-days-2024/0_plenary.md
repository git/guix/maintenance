# Notes from the Guix Days plenary session

- Date: Thursday, Feb. 1st, 2024 and Friday, Feb. 2nd, 2024
- Attendees: 33 people at the events opening, including new users and Nix people

## Main Track

- Introduction to Goblins by Christine from Spritely Institute
- Status of Guix by Efraim
- Guix QA update by Chris

Sessions:

- [Goblins and Hoot](goblins-hoot-guix.md) (Christine)
- [Infrastructure](infrastructure.md) (Alexis)
- Documentation (Julien)
- Guix Home  (Gabor)
- [Alternative Architectures](architecture-support.org) (Efraim)
- Bootstrapping (Janneke)
- Release (Julien)


### Day 2

Sessions:

- Funding (Christine)
- [Governance/Maintainers](governance.org) (Efraim + Chris)
- [Patch flow](patch-flow.md) (Steve)
- [Guix CLI improvements](cli.md) (Jonathan)
- Profiling (Adriel)
- Hurd (Janneke)
- L10N (Julien)
- [Hoot](goblins-hoot-guix.md) (Christine)
- Newbie room (Gabor)
- Onboarding (Gabor)


### Ideas that didn’t make it to sessions

- Shepherd
- HPC


## Status update (Efraim)

  - last release 1 year old
  - installer
    - working okay
    - does not cater to the needs of power users (e.g., btrfs on encrypted)
    - problems when choosing EXWM
    - allow choosing the Hurd?
  - substitute availability is good for x86_64
    - currently 98% or so
    - qa.guix gives a summary
    - 32-bit support is in a poor state
    - idea: estimate i686 substitute usage using server logs?
    - is ARMv7 (32-bit, armhf-linux) used at all?
  - onboarding
    - "not a lot of projects are doing email patches"
    - people maintain their own stack of patches they don't submit
    - "we need to do something on our side"
  - teams
    - "nice but not perfect", "mostly working"
    - what about patches with no associated teams?
    - Rust team is just Efraim
  - sysadmin
    - small bus factor
    - Chris in charge qa.guix, data.guix, etc.
    - berlin was building slowly early January: needed to vacuum the
      posgresql database
  - we're moving faster
    - #5 on Repology
    - we should talk more about package availability
  - time traps
    - "virtual build machine" service to address that
    - could be used for 2038 bug testing
    - RISC-V builds broke with some kernel update
  - SWH

## Guix Quality assurance update (Chris Baines)

  - architecture diagram
  - entry points: https://qa.guix.gnu.org/patches,
    https://qa.guix.gnu.org/reproducible-builds
  - question about “slowness” of builds
    - could use a different prioritirization method
    - could add more hardware
    - https://bordeaux.guix.gnu.org/activity -> it’s busy :-)

## Actions

- TBD
