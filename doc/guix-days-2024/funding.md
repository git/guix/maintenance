# Finances
Currently Guix has monet on FSF, on USA law.
However, the Guix Foundation receives most of its money in Euros.

~100k has been raised
~50k is left

4-5k belongs to Guix Foundation legally.

FSF money is safeguarded by the spending committee.

Both organisations do both (double process).

# Legal
Creating more of a stronger legal approach for Guix takes time,
Guix is not professional on this side and its hard to do right.
Perhaps there needs to be a home for the Guix Foundation.

Has Guix outgrown its umbrella organisation?
Perhaps it should be treated like an independent organisation (like Gnome).

There are lawyers who are specialist for such things.
Guix could pay for specialists to help with the legal and funding aspects.
It takes lots of time and energy to manage the distinction.

Somebody at FSFE has experience for EU FOSS not-for-profits and hirees.

## Trademark Management
Seems healthy that specific problems get solved before more formal funding applications.
Recognise that a legal entity needs funding first.


# Guix Spending
The spending is directed towards:
* hosting costs
* stickers
* hardware
* Outreachy

Hardware and hosting has been donated,
and that Guix should be prepared to manage things differently.
There is naturally no guarantee that
such donations will continue in perpetuity.

What else should we spend money on?:
* Perhaps promoting Guix to other communities;
* Perhaps compensating people for onsite maintenance of infrastructure;
* Indirect costs;
* A build farm (we can meet more need with infrastructure);
* Subsidising international travel (or support for precariat community members);
* Scholarships;
* Paying people for things that wouldnt be done;

# Direct Support
It needs organising in order to shepherd more complex patch reviews.
This includes having a call for applicants,
a hiring process should involve known people.

If somebody is paid to review patches, this requires a fair an equitable approach.
This should be regarding the thing that would not otherwise be done.

Paying people should not deter others.

Paying somebody to repair equipment is helpful and important.

A committee should be collaboratively covered for consensus.
Maybe fundraising requires a committee.



# Governance

A common approach with other organisations:
* Foundation for IP; legal; and governance
* Technical board (its different for profit reasons)
* A foundation provides services to a project -- it does not have control
* The project would be handled by the TOC

Names should be known,
full delegation should be make, including a board from association.

Once goals have been defined,
then do things with the roadmap.

## Guix
Guix foundation has statutes on the internat.

You can be member of guix foundation,
then you can be part of the double foundation process.

The governance problem is ambiguous


There is a confused-deputy problem concerning governance.

The Guix Foundation and the Guix project can have friction.


Guix has more of a European constituency



## Nix
At Nix, they have to play specific roles well.
The RFC process is seen as the governance and the check and balance of the teams,
general community mechanisms help
everybody watches eachother -- checks and balances.

# Funding
## Approaches to Securing Funding
You have to put yourself in other peoples shoes,
thats the hard part.

[for funding applications] you have to put down your own vision,
as it formualtes what you want to do.
We can use appliying for a grant,
to define the mission of an organisations.

Funding descriptions can be a bit of an icebreaker.

Things one identifies with is important with the choice,
making certain choices which have impacts for later.

If you wasnt to do a fundraiser in 6 months,
you have to think about for what and how.

Fundees have predominantly experienced one major funding.

There is not a page which lists grants.

## NLNet
NLNet are direct and technically orientated,
Other funds are not like that.

Funding can be pleasurable with them.



## Joint Funding
It can be tough applying on joint projects,
such as thinking about stratagy, people working together,
as well as what should be done.

Coordination of grants adds complexity.

## Guix Funding
Guix is immature concerning funding.
Guix are fundraising to cover basic costs.

People dont donate very often,
they need to do a campaign.
It needs to happen repeatedly,
which can unfortunately be stressful for organisers.
The money tends to be free for the project,
though not a much as you need.

Fundraising for own projects is harder,
people are interested in doing something for guix.

Guix is likely to reah an audience of people willing to give.
Walking through the process,
it is more fun to talk about than do.

Rather than fundraising, its also important to
fund ways of contributing coding in work.

Its worth speaking to employers for donations,
such as for using infrastructure.


Guix has a strong reputation:
* Science would be worth emphasizing.
* Bitcoin as a point of emphasis seems less interesting.
* Problems from any large institution is problematic
* Framasoft type individual funding is possible but its the hardest path.

Getting employers onboard is a way of getting more funders onboard.

Once institutions are dependent on Guix,
they will be more supportive.

Larger institutional grants require a reputation (as a whole),
it does involve having a spokesperson in particular.
An example of this is Wikipedia's Jimmy Wales.

To receive over a million in funding,
you need a track record.
You need to develop personal relationships with funders.

Having people funding work and being aware of
technology was part of the Spriteley journey (for example).
Doing work that people find exciting, you have to use charisma,
and a sense of you knowing what you want.

# Other Operating Systems and Funding
## Nix
Nix has been successful.

Summer of Nix jumpstarted college students and worked as a testcase,
motivating extra collaboration with NLNet in subsequent years.

This involved hundreds of thousands for a stipend.
It required a demonstrated plan,
and required implementing it properly with doing something
(with figuring out value later).

Managers want to know whether they are doing things.

NLNet would be interested to do something similar with Guix.
Affiliation was useful for building trust.

## Ubuntu
For Africa, sending laptos was good for narriative selection.
It creates a hook - as you need to close the loop for funding,
and then the narriative.

# Fundraising and Spending


# Promoting Guix for funding
Its important to list prestige projects,
tied with institutions or fundees.

There has has been a prejudice that Guix coders are a minority,
and that employers prioritise the fungability of their staff
"Its bullshit but finding talented people has an advantage from Guix and Guile -- you wont have trouble"
