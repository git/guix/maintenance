;; Nginx configuration for ci.guix.gnu.org
;; Copyright © 2016-2022, 2024-2025 Ludovic Courtès <ludo@gnu.org>
;; Copyright © 2017, 2018, 2019, 2020, 2021 Ricardo Wurmus <rekado@elephly.net>
;; Copyright © 2020 Christopher Baines <mail@cbaines.net>
;; Copyright © 2020, 2021 Florian Pelz <pelzflorian@pelzflorian.de>
;; Copyright © 2020, 2021 Tobias Geerinckx-Rice <me@tobias.gr>
;; Copyright © 2021 Mathieu Othacehe <othacehe@gnu.org>
;; Copyright © 2024 Arun Isaac <arunisaac@systemreboot.net>
;; Released under the GNU GPLv3 or any later version.

(use-modules (gnu services web)
             (gnu services version-control)
             (gnu packages monitoring)
             (sysadmin nginx)
             (ice-9 regex))



(define publish-robots.txt
  ;; Try to prevent good-faith crawlers from downloading substitutes.  Allow
  ;; indexing the root—which is expected to be static or cheap—to remain visible
  ;; in search engine results for, e.g., ‘Guix CI’.
  "\
User-agent: *\r
Disallow: /\r
Allow: /$\r
\r
")

(define (publish-locations url)
  "Return the nginx location blocks for 'guix publish' running on URL."
  (list (nginx-location-configuration
         (uri "/nix-cache-info")
         (body
          (list
           (string-append
            "proxy_pass " url "/nix-cache-info;")
           ;; Cache this file since that's always the first thing we ask
           ;; for.
           "proxy_cache static;"
           "proxy_cache_valid 200 100d;"     ; cache hits for a looong time.
           "proxy_cache_valid any 5m;"       ; cache misses/others for 5 min.
           "proxy_ignore_client_abort on;"

           ;; We need to hide and ignore the Set-Cookie header to enable
           ;; caching.
           "proxy_hide_header    Set-Cookie;"
           "proxy_ignore_headers Set-Cookie;")))

        (nginx-location-configuration
         (uri "/nar/")
         (body
          (list
           (string-append "proxy_pass " url ";")
           "client_body_buffer_size 256k;"

           ;; Be more tolerant of delays when fetching a nar.
           "proxy_read_timeout 60s;"
           "proxy_send_timeout 60s;"

           ;; Enable caching for nar files, to avoid reconstructing and
           ;; recompressing archives.
           "proxy_cache nar;"
           "proxy_cache_valid 200 30d;"           ; cache hits for 1 month
           "proxy_cache_valid 504 3m;" ; timeout, when hydra.gnu.org is overloaded
           "proxy_cache_valid any 1h;" ; cache misses/others for 1h.

           "proxy_ignore_client_abort on;"

           ;; Nars are already compressed.
           "gzip off;"

           ;; We need to hide and ignore the Set-Cookie header to enable
           ;; caching.
           "proxy_hide_header    Set-Cookie;"
           "proxy_ignore_headers Set-Cookie;"

           ;; Provide a 'content-length' header so that 'guix
           ;; substitute-binary' knows upfront how much it is downloading.
           ;; "add_header Content-Length $body_bytes_sent;"
           )))

        (nginx-location-configuration
         (uri "~ \\.narinfo$")
         (body
          (list
           ;; Since 'guix publish' has its own caching, and since it relies
           ;; on the atime of cached narinfos to determine whether a
           ;; narinfo can be removed from the cache, don't do any caching
           ;; here.
           (string-append "proxy_pass " url ";")

           ;; For HTTP pipelining.  This has a dramatic impact on
           ;; performance.
           "client_body_buffer_size 128k;"

           ;; Narinfos requests are short, serve many of them on a
           ;; connection.
           "keepalive_requests 600;"

           ;; Do not tolerate slowness of hydra.gnu.org when fetching
           ;; narinfos: better return 504 quickly than wait forever.
           "proxy_connect_timeout 10s;"
           "proxy_read_timeout 10s;"
           "proxy_send_timeout 10s;"

           ;; 'guix publish --ttl' produces a 'Cache-Control' header for
           ;; use by 'guix substitute'.  Let it through rather than use
           ;; nginx's "expire" directive since the expiration time defined
           ;; by 'guix publish' is the right one.
           "proxy_pass_header Cache-Control;"

           "proxy_ignore_client_abort on;"

           ;; We need to hide and ignore the Set-Cookie header to enable
           ;; caching.
           "proxy_hide_header    Set-Cookie;"
           "proxy_ignore_headers Set-Cookie;")))

        ;; Content-addressed files served by 'guix publish'.
        (nginx-location-configuration
         (uri "/file/")
         (body
          (list
           (string-append "proxy_pass " url ";")

           "proxy_cache cas;"
           "proxy_cache_valid 200 200d;"          ; cache hits
           "proxy_cache_valid any 5m;"            ; cache misses/others

           "proxy_ignore_client_abort on;")))

	;; Try to prevent good-faith crawlers from downloading substitutes.
	(nginx-location-configuration
	 (uri "= /robots.txt")
	 (body
	  (list
	   #~(string-append "try_files "
			    #$(plain-file "robots.txt" publish-robots.txt)
			    " =404;")
           "expires 365d;"
	   "root /;")))))

(define %ci-onion
  ;; Onion service name of ci.guix.
  "4zwzi66wwdaalbhgnix55ea3ab4pvvw66ll2ow53kjub6se4q2bclcyd.onion")

(define (berlin-locations publish-url)
  "Return nginx location blocks with 'guix publish' reachable at
PUBLISH-URL."
  (append (publish-locations publish-url)
          (list
           ;; Cuirass.
           (nginx-location-configuration
            (uri "/")
            (body (list "proxy_pass http://localhost:8081;"
                        ;; See
                        ;; <https://community.torproject.org/onion-services/advanced/onion-location/>.
                        (string-append
                         "add_header Onion-Location http://" %ci-onion
                         "$request_uri;"))))

           ;; Cache the home page for a little while to reduce pressure.
           (nginx-location-configuration
            (uri "= /")
            (body (list "proxy_pass http://localhost:8081;"
                        "proxy_cache static;"
                        "proxy_cache_valid 200 120s;"
                        "proxy_ignore_client_abort on;"
                        (string-append
                         "add_header Onion-Location http://" %ci-onion
                         "$request_uri;"))))

           (nginx-location-configuration
            (uri "~ ^/admin")
            (body
             (list "if ($ssl_client_verify != SUCCESS) { return 403; } proxy_pass http://localhost:8081;")))

           (nginx-location-configuration
            (uri "/static")
            (body
             (list
              "proxy_pass http://localhost:8081;"
              ;; Cuirass adds a 'Cache-Control' header, honor it.
              "proxy_cache static;"
              "proxy_cache_valid 200 2d;"
              "proxy_cache_valid any 10m;"
              "proxy_ignore_client_abort on;")))

           (nginx-location-configuration
            (uri "/download")                     ;Cuirass "build products"
            (body
             (list
              "proxy_pass http://localhost:8081;"
              "expires 10d;"                      ;override 'Cache-Control'
              "proxy_cache static;"
              "proxy_cache_valid 200 30d;"
              "proxy_cache_valid any 10m;"
              "proxy_ignore_client_abort on;")))

           (nginx-location-configuration          ;certbot
            (uri "/.well-known")
            (body (list "root /var/www;")))

           (nginx-location-configuration
            (uri "/berlin.guixsd.org-export.pub")
            (body
             (list "root /var/www/guix;"))))))

(define %publish-url "http://localhost:3000")

(define %berlin-servers
  (list
   ;; Redirect domains that don't explicitly support HTTP (below) to HTTPS.
   (nginx-server-configuration
    (listen '("80"))
    (raw-content
     (list "return 308 https://$host$request_uri;")))

   ;; Domains that still explicitly support plain HTTP.
   (nginx-server-configuration
    (listen '("80"))
    (server-name `("ci.guix.gnu.org"
		   ;; <https://logs.guix.gnu.org/guix/2021-11-20.log#155427>
		   "~[0-9]$"
                   ,(regexp-quote %ci-onion)))
    (locations (berlin-locations %publish-url))
    (raw-content
     (list
      "access_log  /var/run/anonip/http.access.log;"
      "proxy_set_header X-Forwarded-Host $host;"
      "proxy_set_header X-Forwarded-Port $server_port;"
      "proxy_set_header X-Forwarded-For  $proxy_add_x_forwarded_for;")))

   (nginx-server-configuration
    (listen '("80"))
    (server-name '("bootstrappable.org"
                   "www.bootstrappable.org"))
    (root "/srv/bootstrappable.org")
    (locations
     (list (nginx-location-configuration ;certbot
            (uri "/.well-known")
            (body (list "root /var/www;")))))
    (raw-content
     (list
      "access_log /var/run/anonip/bootstrappable.access.log;")))

   (nginx-server-configuration
    (listen '("80"))
    (server-name '("guixwl.org"
                   "www.guixwl.org"))
    (root "/home/rekado/gwl/")
    (locations
     (list (nginx-location-configuration ;certbot
            (uri "/.well-known")
            (body (list "root /var/www;")))

           (nginx-location-configuration
            (uri "/manual")
            (body (list "alias /srv/gwl-manual;")))

           ;; Pass requests to 'guix workflow --web-interface'.
           (nginx-location-configuration
            (uri "/")
            (body '("proxy_pass http://localhost:5000;")))))
    (raw-content
     (list
      "access_log /var/run/anonip/workflows-guix-info.access.log;")))
        
   ;; HTTPS servers
   (nginx-server-configuration
    (listen '("443 ssl"))
    (server-name '("ci.guix.gnu.org"))
    (ssl-certificate (le "ci.guix.gnu.org"))
    (ssl-certificate-key (le "ci.guix.gnu.org" 'key))
    (locations (berlin-locations %publish-url))
    (raw-content
     (append
      %tls-settings
      (list
       "access_log  /var/run/anonip/https.access.log;"
       "proxy_set_header X-Forwarded-Host $host;"
       "proxy_set_header X-Forwarded-Port $server_port;"
       "proxy_set_header X-Forwarded-For  $proxy_add_x_forwarded_for;"
       ;; For Cuirass admin interface authentication
       "ssl_client_certificate /etc/ssl-ca/certs/ca.crt;"
       "ssl_verify_client optional;"

       ;; Block bots that do not honor 'robots.txt'.
       "\
if ($http_user_agent ~ (Bytespider|ClaudeBot|DotBot|PetalBot) ) {
  return 403;
  break;
}\n"))))

   (nginx-server-configuration
    (listen '("443 ssl"))
    (server-name '("qualif.ci.guix.gnu.org"))
    (locations (berlin-locations "http://localhost:3003"))
    (raw-content
     (append %tls-settings
             '("access_log  /var/run/anonip/qualif.access.log;"))))

   (nginx-server-configuration
    (listen '("443 ssl"))
    (server-name '("bootstrappable.org"
                   "www.bootstrappable.org"))
    (ssl-certificate (le "bootstrappable.org"))
    (ssl-certificate-key (le "bootstrappable.org" 'key))
    (root "/srv/bootstrappable.org")
    (locations
     (list (nginx-location-configuration ;certbot
            (uri "/.well-known")
            (body (list "root /var/www;")))))
    (raw-content
     (append
      %tls-settings
      (list
       "access_log /var/run/anonip/bootstrappable.https.access.log;"))))

   (nginx-server-configuration
    (listen '("443 ssl"))
    (server-name '("disarchive.guix.gnu.org"))
    (ssl-certificate (le "disarchive.guix.gnu.org"))
    (ssl-certificate-key (le "disarchive.guix.gnu.org" 'key))
    (root "/gnu/disarchive")
    (locations
     (list (nginx-location-configuration ;certbot
            (uri "/.well-known")
            (body (list "root /var/www;")))))
    (raw-content
     (list "gzip_static always; gunzip on;\n"
           "access_log /var/run/anonip/disarchive.access.log;")))

   (nginx-server-configuration
    (listen '("443 ssl"))
    (server-name '("issues.guix.gnu.org"))
    (ssl-certificate (le "issues.guix.gnu.org"))
    (ssl-certificate-key (le "issues.guix.gnu.org" 'key))
    (root "/home/rekado/mumi/")
    (locations
     (list
      (nginx-location-configuration     ;certbot
       (uri "/.well-known")
       (body (list "root /var/www;")))
      (nginx-location-configuration
       (uri "/")
       (body '("proxy_pass http://localhost:1234;")))
      ;; Rate limit graphql endpoint.
      (nginx-location-configuration
       (uri "/graphql")
       (body '("proxy_pass http://localhost:1234;"

               "limit_req zone=mumigraphqlzone burst=20 nodelay;"
               "limit_req_status 429;")))))
    (raw-content
     (append
      %tls-settings
      (list
       "proxy_set_header X-Forwarded-Host $host;"
       "proxy_set_header X-Forwarded-Port $server_port;"
       "proxy_set_header X-Forwarded-For  $proxy_add_x_forwarded_for;"
       "proxy_connect_timeout       600;"
       "proxy_send_timeout          600;"
       "proxy_read_timeout          600;"
       "send_timeout                600;"
       "access_log /var/run/anonip/issues-guix-gnu-org.https.access.log;"))))

   (nginx-server-configuration
    (listen '("443 ssl"))
    (server-name '("dump.guix.gnu.org"))
    (ssl-certificate (le "dump.guix.gnu.org"))
    (ssl-certificate-key (le "dump.guix.gnu.org" 'key))
    (locations
     (list
      (nginx-location-configuration ;certbot
       (uri "/.well-known")
       (body (list "root /var/www;")))
      (nginx-location-configuration
       (uri "/")
       (body '("proxy_pass http://localhost:2121;")))))
    (raw-content
     (append
         %tls-settings
         (list
          "proxy_set_header X-Forwarded-Host $host;"
          "proxy_set_header X-Forwarded-Port $server_port;"
          "proxy_set_header X-Forwarded-For  $proxy_add_x_forwarded_for;"
          "proxy_connect_timeout       600;"
          "proxy_send_timeout          600;"
          "proxy_read_timeout          600;"
          "send_timeout                600;"
          "client_max_body_size        500M;"
          "access_log /var/run/anonip/dump-guix-gnu-org.https.access.log;"))))

   (nginx-server-configuration
    (listen '("443 ssl"))
    (server-name '("guixwl.org"
                   "www.guixwl.org"))
    (ssl-certificate (le "www.guixwl.org"))
    (ssl-certificate-key (le "www.guixwl.org" 'key))
    (root "/home/rekado/gwl/")
    (locations
     (list
      (nginx-location-configuration
       (uri "/manual")
       (body (list "alias /srv/gwl-manual;")))
      (nginx-location-configuration
       (uri "/")
       (body '("proxy_pass http://localhost:5000;")))))
    (raw-content
     (append
      %tls-settings
      (list
       "proxy_set_header X-Forwarded-Host $host;"
       "proxy_set_header X-Forwarded-Port $server_port;"
       "proxy_set_header X-Forwarded-For  $proxy_add_x_forwarded_for;"
       "proxy_connect_timeout       600;"
       "proxy_send_timeout          600;"
       "proxy_read_timeout          600;"
       "send_timeout                600;"
       "access_log /var/run/anonip/workflows-guix-info.https.access.log;"))))

   ;; Backwards compatibility with legacy hostnames.
   (nginx-server-configuration
    (listen '("443 ssl"
	      "80"))
    (ssl-certificate (le "berlin.guixsd.org"))
    (ssl-certificate-key (le "berlin.guixsd.org" 'key))
    (server-name '("berlin.guixsd.org"
		   "berlin.guix.info"
                   "ci.guix.info"))
    (locations
     (list
      (nginx-location-configuration
       (uri "~ /(.*)")
       (body (list "return 301 $scheme://ci.guix.gnu.org/$1;"))))))

   (nginx-server-configuration
    (listen '("443 ssl"
	      "80"))
    (server-name '("guix.info"
                   "www.guix.info"))
    (ssl-certificate (le "guix.info"))
    (ssl-certificate-key (le "guix.info" 'key))
    (locations
     (list
      (nginx-location-configuration
       (uri "~ /(.*)")
       (body (list "return 301 $scheme://guix.gnu.org/$1;"))))))

   (nginx-server-configuration
    (listen '("443 ssl"
	      "80"))
    (server-name '("issues.guix.info"))
    (ssl-certificate (le "issues.guix.info"))
    (ssl-certificate-key (le "issues.guix.info" 'key))
    (locations
     (list
      (nginx-location-configuration
       (uri "~ /(.*)")
       (body (list "return 301 $scheme://issues.guix.gnu.org/$1;"))))))

   (nginx-server-configuration
    (listen '("443 ssl"
	      "80"))
    (server-name '("workflows.guix.info"
                   "workflow.guix.info"))
    (ssl-certificate (le "www.guixwl.org"))
    (ssl-certificate-key (le "www.guixwl.org" 'key))
    (locations
     (list
      (nginx-location-configuration
       (uri "~ /(.*)")
       (body (list "return 301 $scheme://guixwl.org/$1;"))))))))

(define %extra-content
  (list
   "default_type  application/octet-stream;"
   "sendfile        on;"

   (accept-languages)

   ;; Maximum chunk size to send.  Partly this is a workaround for
   ;; <http://bugs.gnu.org/19939>, but also the nginx docs mention that
   ;; "Without the limit, one fast connection may seize the worker
   ;; process entirely."
   ;;  <http://nginx.org/en/docs/http/ngx_http_core_module#sendfile_max_chunk>
   "sendfile_max_chunk 1m;"

   "keepalive_timeout  65;"

   ;; Use HTTP 1.1 to talk to the backend so we benefit from keep-alive
   ;; connections and chunked transfer encoding.  The latter allows us to
   ;; make sure we do not cache partial downloads.
   "proxy_http_version 1.1;"

   ;; The 'inactive' parameter for caching is not very useful in our
   ;; case: all that matters is that LRU sweeping happens when 'max_size'
   ;; is hit.

   ;; cache for nar files
   "proxy_cache_path /var/cache/nginx/nar"
   "     levels=2"
   "     inactive=8d"	    ; inactive keys removed after 8d
   "     keys_zone=nar:4m"  ; nar cache meta data: ~32K keys
   "     max_size=10g;"     ; total cache data size max

   ;; cache for content-addressed files
   "proxy_cache_path /var/cache/nginx/cas"
   "     levels=2"
   "     inactive=180d"	    ; inactive keys removed after 180d
   "     keys_zone=cas:8m"  ; nar cache meta data: ~64K keys
   "     max_size=50g;"         ; total cache data size max

   ;; cache for build logs
   "proxy_cache_path /var/cache/nginx/logs"
   "     levels=2"
   "     inactive=60d"          ; inactive keys removed after 60d
   "     keys_zone=logs:8m"     ; narinfo meta data: ~64K keys
   "     max_size=4g;"          ; total cache data size max

   ;; cache for static data
   "proxy_cache_path /var/cache/nginx/static"
   "     levels=1"
   "     inactive=10d"	       ; inactive keys removed after 10d
   "     keys_zone=static:1m"   ; nar cache meta data: ~8K keys
   "     max_size=200m;"        ; total cache data size max

   ;; If Hydra cannot honor these delays, then something is wrong and
   ;; we'd better drop the connection and return 504.
   "proxy_connect_timeout 10s;"
   "proxy_read_timeout 20s;"
   "proxy_send_timeout 20s;"

   ;; Cache timeouts for a little while to avoid increasing pressure.
   "proxy_cache_valid 504 30s;"

   ;; Rate limit mumi graphql endpoint.
   "limit_req_zone $binary_remote_addr zone=mumigraphqlzone:1m rate=10r/s;"))

(define %nginx-configuration
  (nginx-configuration
   (server-blocks %berlin-servers)
   (server-names-hash-bucket-size 128)
   (modules
    (list
     ;; Module to redirect users to the localized pages of their choice.
     (file-append nginx-accept-language-module
                  "/etc/nginx/modules/ngx_http_accept_language_module.so")))
   (global-directives
    ;; This is a 72-core machine, but let's not use all of them for nginx.
    '((worker_processes . 16)
      (pcre_jit . on)
      (events . ((worker_connections . 1024)))))
   (extra-content
    (string-join %extra-content "\n"))))

(define %zabbix-nginx-server
  (nginx-server-configuration
   (root #~(string-append #$zabbix-server:front-end "/share/zabbix/php"))
   (listen '("443 ssl"))
   (server-name '("monitor.guix.gnu.org"))
   (ssl-certificate (le "monitor.guix.gnu.org"))
   (ssl-certificate-key (le "monitor.guix.gnu.org" 'key))
   (index '("index.php"))
   (raw-content
    (append
     %tls-settings
     (list
      "access_log  /var/run/anonip/https.access.log;"
      "proxy_set_header X-Forwarded-Host $host;"
      "proxy_set_header X-Forwarded-Port $server_port;"
      "proxy_set_header X-Forwarded-For  $proxy_add_x_forwarded_for;"
      ;; For client cert authentication
      "ssl_client_certificate /etc/ssl-ca/certs/ca.crt;"
      "ssl_verify_client on;")))
   (locations
    (let ((php-location (nginx-php-location)))
      (list (nginx-location-configuration ;certbot
             (uri "/.well-known")
             (body (list "root /var/www;")))
            (nginx-location-configuration
             (inherit php-location)
             (body (cons "if ($ssl_client_verify != SUCCESS) { return 403; }"
                         (append (nginx-location-configuration-body php-location)
                                 (list "
fastcgi_param PHP_VALUE \"post_max_size = 16M
                          max_execution_time = 300\";
"))))))))))

(define %zabbix-nginx-local-server
  (nginx-server-configuration
   (inherit %zabbix-front-end-configuration-nginx)
   (listen '("127.0.0.1:15412"))))
