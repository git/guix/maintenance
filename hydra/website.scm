(define (website-services)
  "Return the list of Guix website services."
  (list
   ;; Runnning guix.gnu.org.
   (service guix-web-site-service-type)

   ;; The bootstrappable.org web site.
   (service static-web-site-service-type
            (list (static-web-site-configuration
                   (git-url
                    "https://git.savannah.gnu.org/git/guix/bootstrappable.git")
                   (period (* 24 3600))           ;check once per day
                   (directory "/srv/bootstrappable.org"))))

   ;; GWL web site.
   (service gwl-web-service-type
            (@ (gnu packages package-management) gwl))))
