(use-modules (thinkpad))

(define names '("kranji" "marsiling" "yishun" "tampines"))

(map
  (lambda (name)
    (machine
      (operating-system (thinkpad-os name))
      (environment managed-host-environment-type)
      (configuration
        (machine-ssh-configuration
          (host-name (string-append name "x"))
          (system "x86_64-linux")))))
  names)

