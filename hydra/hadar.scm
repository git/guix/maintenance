;; Guix System configuration file for a SolidRun Honeycomb LX2 machine
;; Copyright © 2016, 2017, 2018 Ludovic Courtès <ludo@gnu.org>
;; Copyright © 2021, 2022 Christopher Baines <mail@cbaines.net>
;; Released under the GNU GPLv3 or any later version.

(use-modules (guix) (gnu))
(use-service-modules networking mcron ssh monitoring guix web certbot)
(use-package-modules screen ssh linux certs)

(use-modules (gnu services shepherd) (guix packages)
             (gnu packages admin))

(define (sysadmin name full-name)
  (user-account
   (name name)
   (comment full-name)
   (group "users")
   (supplementary-groups '("wheel" "kvm"))
   (home-directory (string-append "/home/" name))))

(define %accounts
  (list (sysadmin "cbaines" "Christopher Baines")
        (sysadmin "ludo" "Ludovic Courtès")))

(operating-system
  (host-name "hadar")
  (timezone "Europe/London")
  (locale "en_US.utf8")

  (bootloader (bootloader-configuration
               (bootloader grub-efi-bootloader)
               (targets '("/boot/efi"))))

  (kernel linux-libre-arm64-honeycomb)
  (initrd-modules '())
  (kernel-arguments '("arm-smmu.disable_bypass=0"
                      "iommu.passthrough=1"))

  (file-systems (cons* (file-system
                         (device "/dev/nvme0n1p2")
                         (mount-point "/")
                         (type "ext4"))
                       (file-system
                         (device "/dev/nvme0n1p1")
                         (mount-point "/boot/efi")
                         (type "vfat"))
                       %base-file-systems))

  (swap-devices (list (swap-space (target "/swapfile"))))

  (users (append %accounts %base-user-accounts))
  (packages
   (cons* screen
          %base-packages))
  (services
   (cons* (service dhcp-client-service-type)

          (service mcron-service-type
                   (mcron-configuration
                    (jobs
                     (list #~(job '(next-hour '(3)) "guix gc -F 250G")))))

          (service agetty-service-type
                   (agetty-configuration
                    (tty "ttyAMA0")
                    (keep-baud? #t)
                    (term "vt220")
                    (baud-rate "115200,38400,9600")))

          (service openssh-service-type
                   (openssh-configuration
                    (password-authentication? #f)))

          (service ntp-service-type)
          (service prometheus-node-exporter-service-type)

          (service guix-build-coordinator-agent-service-type
                   (guix-build-coordinator-agent-configuration
                    (coordinator "https://coordinator.bordeaux.guix.gnu.org")
                    (authentication
                     (guix-build-coordinator-agent-password-file-auth
                      (uuid "6f2c2ceb-bbf9-4704-a6b3-0a95bb22b6d5")
                      (password-file
                       "/etc/guix-build-coordinator-agent-password")))
                    (max-parallel-builds 8)
                    (max-allocated-builds 512)
                    (max-parallel-uploads 8)
                    (max-1min-load-average 8)
                    (systems '("aarch64-linux" "armhf-linux"))
                    (derivation-substitute-urls
                     (list "https://data.guix.gnu.org"
                           "https://data.qa.guix.gnu.org"))
                    (non-derivation-substitute-urls
                     (list "https://bordeaux.guix.gnu.org"))))

          (modify-services %base-services
            (guix-service-type
             config => (guix-configuration
                        (inherit config)
                        (substitute-urls
                         '("https://bordeaux.guix.gnu.org"))

                        (max-silent-time (* 12 3600))
                        (timeout (* 72 3600))

                        (build-accounts 32)

                        (authorized-keys
                         (list
                          (local-file "keys/guix/bordeaux.guix.gnu.org-export.pub")
                          (local-file "keys/guix/data.guix.gnu.org.pub")
                          (local-file "keys/guix/data.qa.guix.gnu.org.pub")))))))))
