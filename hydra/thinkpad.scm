(define-module (thinkpad)
  #:use-module (gnu)
  #:use-module (gnu services desktop)
  #:use-module (gnu services guix)
  #:use-module (gnu services mcron)
  #:use-module (gnu services networking)
  #:use-module (gnu services ssh)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages vim)
  #:export (thinkpad-os))

(define (thinkpad-os name)
  (operating-system
    (locale "en_US.utf8")
    (timezone "Europe/Paris")
    (keyboard-layout (keyboard-layout "us"))
    (host-name name)

    (users (cons* (user-account
                    (name "andreas")
                    (comment "Andreas")
                    (group "users")
                    (home-directory "/home/andreas")
                    (supplementary-groups '("wheel" "netdev" "audio" "video")))
                  %base-user-accounts))

    (packages (append (list git upower vim)
                      %base-packages))

    (bootloader (bootloader-configuration
                  (bootloader grub-efi-bootloader)
                  (targets (list "/boot/efi"))
                  (keyboard-layout keyboard-layout)))

    (swap-devices (list (swap-space
                          (target "/dev/nvme0n1p2"))))

    (file-systems (cons* (file-system
                           (mount-point "/boot/efi")
                           (device "/dev/nvme0n1p1")
                           (type "vfat"))
                         (file-system
                           (mount-point "/")
                           (device "/dev/nvme0n1p3")
                           (type "ext4"))
                         %base-file-systems))

    (services
      (append
        (modify-services %base-services
          (guix-service-type config =>
            (guix-configuration
              (substitute-urls '("https://bordeaux.guix.gnu.org"))
              (authorized-keys
                (list
                  (local-file "keys/guix/bordeaux.guix.gnu.org-export.pub")
                  (local-file "keys/guix/data.guix.gnu.org.pub")
                  (local-file "keys/guix/data.qa.guix.gnu.org.pub")
                  (local-file "keys/guix/andreas-jurong.pub")))
              (max-silent-time (* 24 3600))
              (timeout (* 48 3600)))))
        (list
          (service dhcp-client-service-type)
          (service upower-service-type)
          (service openssh-service-type
            (openssh-configuration
              (permit-root-login 'prohibit-password)
              (authorized-keys
                `(("andreas" ,(local-file "keys/ssh/andreas.pub"))
                  ("root" ,(local-file "keys/ssh/andreas.pub"))))))
          (service mcron-service-type
            (mcron-configuration
              (jobs (list #~(job '(next-hour '(3)) "guix gc -F 150G")))))
          (service guix-build-coordinator-agent-service-type
            (guix-build-coordinator-agent-configuration
              (coordinator "https://coordinator.bayfront.guix.gnu.org")
              (authentication
                (guix-build-coordinator-agent-dynamic-auth-with-file
                  (agent-name name)
                  (token-file
                    "/etc/guix-build-coordinator/guix-build-coordinator-agent-token")))
              (derivation-substitute-urls
                '("https://data.guix.gnu.org" "https://data.qa.guix.gnu.org"))
              (non-derivation-substitute-urls
                '("https://bordeaux.guix.gnu.org"))
              (systems '("x86_64-linux" "i686-linux"))
              (max-parallel-builds 8)
              (max-parallel-uploads 2)
              (max-1min-load-average 8))))))))

