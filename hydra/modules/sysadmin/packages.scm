;;; Packages for GNU Guix project systems.
;;;
;;; Copyright © 2023 Christopher Baines <mail@cbaines.net>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (sysadmin packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages guile-xyz)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages gdb)
  #:use-module (gnu packages uml)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages web)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26))

(define-public qa-frontpage
  (let ((commit "ae7577476e2ba1caf7860d5e0e9807727a8ff3c2")
        (revision "34"))
    (package
      (name "guix-qa-frontpage")
      (version (git-version "0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://git.savannah.gnu.org/git/guix/qa-frontpage.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "1g9h6xs7dnw0swxvffsgkxhx9wssfyqz6pyyvdvzdn0q0nqmmsls"))))
      (build-system gnu-build-system)
      (arguments
       (list
        #:modules `(((guix build guile-build-system)
                     #:select (target-guile-effective-version))
                    (guix build union)
                    ,@%default-gnu-imported-modules)
        #:imported-modules `((guix build guile-build-system)
                             (guix build union)
                             ,@%default-gnu-imported-modules)
        #:phases
        #~(modify-phases %standard-phases
            (add-after 'install 'wrap-executable
              (lambda* (#:key inputs outputs target #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                       (bin (string-append out "/bin"))
                       (guile (assoc-ref inputs "guile-next"))
                       (version (target-guile-effective-version guile))
                       (scm (string-append out "/share/guile/site/" version))
                       (go  (string-append out "/lib/guile/" version "/site-ccache")))
                  (for-each
                   (lambda (file)
                     (simple-format (current-error-port) "wrapping: ~A\n" file)
                     (wrap-program file
                       `("PATH" ":" prefix
                         (,(string-append (assoc-ref inputs "git")
                                          "/bin")
                          ,(string-append (assoc-ref inputs "openssh")
                                          "/bin")))
                       `("GUILE_LOAD_PATH" ":" prefix
                         (,scm ,(getenv "GUILE_LOAD_PATH")))
                       `("GUILE_LOAD_COMPILED_PATH" ":" prefix
                         (,go ,(getenv "GUILE_LOAD_COMPILED_PATH")))))
                   (find-files bin)))))
            (add-after 'install 'install-debug-helpers
              (lambda* (#:key inputs #:allow-other-keys)
                ;; Grafts can mean that the outputs in use don't match
                ;; up with the outputs referenced by debug outputs,
                ;; which in turn causes problems with debugging
                ;; symbols in gdb.
                ;;
                ;; Work around this by including lib/debug in this
                ;; package that references usable debug outputs that
                ;; can be used in GDB_DEBUG_FILE_DIRECTORY.
                (union-build (string-append #$output "/lib/debug")
                             (search-path-as-list
                              '("lib/debug")
                              (map cdr inputs)))
                (call-with-output-file
                    (string-append #$output "/share/guix-qa-frontpage/gdb-init")
                  (lambda (port)
                    (simple-format
                     port
                     "
set debug-file-directory ~A

set auto-load safe-path /gnu/store/*/lib\n"
                     (string-append #$output "/lib/debug"))))
                (let ((gdb-script
                       (string-append #$output "/bin/guix-qa-frontpage-gdb")))
                  (call-with-output-file gdb-script
                    (lambda (port)
                      (simple-format
                       port
                       "#!~A
~A -ix ~A/share/guix-qa-frontpage/gdb-init ~A /var/lib/qa-frontpage/core\n"
                       (which "sh")
                       (which "gdb")
                       #$output
                       (string-append (assoc-ref inputs "guile-next") "/bin/guile"))))
                  (chmod gdb-script #o555)))))))
      (inputs
       (list guix
             guix-data-service
             guile-json-4
             guile-fibers
             guile-knots
             guile-kolam
             guile-git
             guile-debbugs
             guile-readline
             guile-prometheus
             guix-build-coordinator
             guile-next
             (list guile-next "debug")
             (list guile-gnutls "debug")
             (list gnutls "debug")
             ;; TODO This doesn't seem to match up with the glibc used
             ;; by guile?
             (list glibc "debug")
             gdb
             git
             openssh))
      (native-inputs
       (list autoconf
             automake
             pkg-config
             emacs-minimal
             emacs-htmlize
             plantuml))
      (synopsis "QA Frontpage for Guix")
      (description
       "This service assists with quality assurance within Guix.  Currently
that means assisting with testing patches, but in the intended scope
is any and all quality assurance tasks.")
      (home-page "https://git.cbaines.net/guix/qa-frontpage")
      (license license:agpl3+))))

(define-public guix-cran-scripts
  (let ((commit "ea28d84e90491fcfdebacf6015bddd98e7198495")
        (revision "10"))
  (package
    (name "guix-cran-scripts")
    (version (git-version "0" revision commit))
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/guix-science/guix-cran-scripts.git")
             (commit commit)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0p29wv42wmwy6ap72mhmqqgnqhllc3zfij5zx0mrbndpwb38wrr1"))))
    (build-system trivial-build-system)
    (arguments
     (list
      #:modules '((guix build utils))
      #:builder
      #~(begin
          (use-modules (guix build utils))
          (let* ((source (assoc-ref %build-inputs "source"))
                 (bin (string-append #$output "/bin"))
                 (import-scm (string-append bin "/import.scm"))
                 (update-sh (string-append bin "/update.sh"))
                 (shell #$(file-append bash-minimal "/bin/bash")))
            (mkdir-p bin)
            (copy-file (string-append source "/import.scm") import-scm)
            (copy-file (string-append source "/update.sh") update-sh)
            ;; No interpreter, cannot use wrap-script.
            ;; Don't wrap, because it has to be called with `guix repl`
            #;
            (wrap-program import-scm
              #:sh shell
              `("PATH" ":" prefix (,#$(file-append git "/bin"))))
            (substitute* update-sh
              (("import.scm") import-scm))
            (chmod update-sh #o555)
            (wrap-program update-sh
              #:sh shell
              `("PATH" ":" prefix (,#$(file-append coreutils "/bin")
                                   ,#$(file-append git "/bin"))))))))
    (home-page "https://github.com/guix-science/guix-cran-scripts")
    (synopsis "Automated CRAN to Guix import")
    (description "Script, which automates the import of all packages from
CRAN into a separate Guix channel.")
    (license license:gpl3+))))
