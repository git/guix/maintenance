;;; SolidRun Honeycomb LX2 build machines.
;;;
;;; Copyright © 2016 2018, 2020-2022, 2024 Ludovic Courtès <ludo@gnu.org>
;;; Copyright © 2020, 2021 Mathieu Othacehe <othacehe@gnu.org>
;;; Copyright © 2021 Christopher Baines <mail@cbaines.net>
;;; Copyright © 2021, 2022 Ricardo Wurmus <rekado@elephly.net>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (sysadmin honeycomb)
  #:use-module (gnu)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader u-boot)
  #:use-module (gnu packages bootloaders)
  #:use-module (gnu packages screen)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages certs)
  #:use-module (gnu services avahi)
  #:use-module (gnu services cuirass)
  #:use-module (gnu services networking)
  #:use-module (gnu services mcron)
  #:use-module (gnu services ssh)
  #:use-module (gnu services vpn)
  #:use-module ((sysadmin services) #:select (berlin-wireguard-peer))
  #:use-module (guix base32)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:export (honeycomb-system))

;; This is just like extlinux-configuration-file but with a fixed FDTDIR.
;;
;; The generated Grub configuration tells u-boot to look for the
;; device tree file in the kernel directory.  The device tree file has
;; an unexpected name, though, so the system doesn't boot as the file
;; cannot be found.  We arranged for the file to be stored in
;; /boot/extlinux under the expected name.  So we need to set the
;; FDTDIR to that location in the generated
;; /boot/extlinux/extlinux.conf.
(define* (honeycomb-extlinux-configuration-file
          config entries #:key
          (system (%current-system))
          (old-entries '())
          #:allow-other-keys)
  "Return the U-Boot configuration file corresponding to CONFIG, a
<u-boot-configuration> object, and where the store is available at STORE-FS, a
<file-system> object.  OLD-ENTRIES is taken to be a list of menu entries
corresponding to old generations of the system."

  (define all-entries
    (append entries (bootloader-configuration-menu-entries config)))

  (define (menu-entry->gexp entry)
    (let ((label (menu-entry-label entry))
          (kernel (menu-entry-linux entry))
          (kernel-arguments (menu-entry-linux-arguments entry))
          (initrd (menu-entry-initrd entry)))
      #~(format port "LABEL ~a
  MENU LABEL ~a
  KERNEL ~a
  FDTDIR /boot/extlinux
  INITRD ~a
  APPEND ~a
~%"
                #$label #$label
                #$kernel #$initrd
                (string-join (list #$@kernel-arguments)))))

  (define builder
    #~(call-with-output-file #$output
        (lambda (port)
          (let ((timeout #$(bootloader-configuration-timeout config)))
            (format port "# This file was generated from your Guix configuration.  Any changes
# will be lost upon reconfiguration.
UI menu.c32
MENU TITLE GNU Guix Boot Options
PROMPT ~a
TIMEOUT ~a~%"
                    (if (> timeout 0) 1 0)
                    ;; timeout is expressed in 1/10s of seconds.
                    (* 10 timeout))
            #$@(map menu-entry->gexp all-entries)

            #$@(if (pair? old-entries)
                   #~((format port "~%")
                      #$@(map menu-entry->gexp old-entries)
                      (format port "~%"))
                   #~())))))

  (computed-file "extlinux.conf" builder
                 #:options '(#:local-build? #t
                                            #:substitutable? #f)))

(define honeycomb-bootloader
  (bootloader
   (inherit u-boot-bootloader)
   (name 'u-boot-for-honeycomb)
   (configuration-file-generator honeycomb-extlinux-configuration-file)))

(define (sysadmin name full-name)
  (user-account
   (name name)
   (comment full-name)
   (group "users")
   (supplementary-groups '("wheel" "kvm"))
   (home-directory (string-append "/home/" name))))

(define %accounts
  (list (sysadmin "ludo" "Ludovic Courtès")
        (sysadmin "rekado" "Ricardo Wurmus")
        (sysadmin "mathieu" "Mathieu Othacehe")
        (user-account
         (name "hydra")
         (comment "Hydra User")
         (group "users")
         (home-directory (string-append "/home/" name)))))

(define %authorized-guix-keys
  ;; List of authorized 'guix archive' keys.
  (list (local-file "../../keys/guix/berlin.guixsd.org-export.pub")))

(define make-linux-libre*
  (@@ (gnu packages linux) make-linux-libre*))

(define linux-libre-arm64-honeycomb
  (make-linux-libre*
   linux-libre-5.15-version
   linux-libre-5.15-gnu-revision
   linux-libre-5.15-source
   '("aarch64-linux")
   #:extra-version "arm64-honeycomb"
   #:extra-options
   ;; See
   ;; https://github.com/SolidRun/lx2160a_build/blob/master/configs/linux/lx2k_additions.config
   (append
       `(("CONFIG_GPIO_SYSFS" . #true)
         ("CONFIG_GPIO_MPC8XXX" . #true)
         ("CONFIG_NET_PKTGEN" . #true)
         ("CONFIG_USB_SERIAL" . #true)
         ("CONFIG_USB_SERIAL_CONSOLE" . #true)
         ("CONFIG_USB_SERIAL_GENERIC" . #true)
         ("CONFIG_USB_SERIAL_SIMPLE" . #true)
         ("CONFIG_USB_SERIAL_FTDI_SIO" . #true)
         ("CONFIG_USB_ACM" . #true)
         ("CONFIG_USB_NET_DRIVERS" . #true)
         ("CONFIG_USB_USBNET" . #true)
         ("CONFIG_USB_NET_CDCETHER" . #true)
         ("CONFIG_USB_NET_CDC_NCM" . #true)
         ("CONFIG_USB_NET_NET1080" . #true)
         ("CONFIG_USB_NET_CDC_SUBSET_ENABLE" . #true)
         ("CONFIG_USB_NET_CDC_SUBSET" . #true)
         ("CONFIG_USB_ARMLINUX" . #true)
         ("CONFIG_BLK_DEV_NVME" . #true)
         ("CONFIG_NVMEM_BCM_OCOTP" . #true)
         ("CONFIG_DRM_AMDGPU" . #true)
         ("CONFIG_DRM_AMDGPU_SI" . #true)
         ("CONFIG_DRM_AMDGPU_CIK" . #true)
         ("CONFIG_DRM_AMDGPU_USERPTR" . #true)
         ("CONFIG_DRM_AMD_DC" . #true)
         ("CONFIG_CHASH" . #true)
         ("CONFIG_PMBUS" . #true)
         ("CONFIG_SENSORS_PMBUS" . #true)
         ("CONFIG_REGULATOR" . #true)
         ("CONFIG_REGULATOR_FIXED_VOLTAGE" . #true)
         ("CONFIG_REGULATOR_PWM" . #true)
         ("CONFIG_SENSORS_AMC6821" . #true)
         ("CONFIG_SENSORS_LM90" . #true)
         ("CONFIG_SENSORS_LTC2978" . #true)
         ("CONFIG_SENSORS_LTC2978_REGULATOR" . #true)
         ("CONFIG_TMPFS" . #true)
         ("CONFIG_TMPFS_POSIX_ACL" . #true)
         ("CONFIG_TMPFS_XATTR" . #true)
         ;;("CONFIG_BLK_DEV_RAM_SIZE" . 524288)
         ("CONFIG_POWER_RESET_GPIO" . #true)
         ("CONFIG_CRYPTO_USER_API_HASH" . #true)
         ("CONFIG_CRYPTO_USER_API_SKCIPHER" . #true)
         ("CONFIG_CRYPTO_USER_API_RNG" . #true)
         ("CONFIG_CRYPTO_USER_API_AEAD" . #true)

         ;; For connecting to ci.guix.gnu.org over VPN.
         ("CONFIG_WIREGUARD" . m))
       ((@@ (gnu packages linux) default-extra-linux-options)
        linux-libre-5.15-version))))

(define gc-job
  ;; Run 'guix gc' at 3AM every day.
  #~(job '(next-hour '(3)) "guix gc -F 50G"))

(define* (honeycomb-system name #:key wireguard-ip)
  (operating-system
    (host-name name)
    (timezone "Europe/Berlin")
    (locale "en_US.UTF-8")

    (bootloader (bootloader-configuration
                 (bootloader honeycomb-bootloader)
                 (target "/boot/efi")))

    (kernel linux-libre-arm64-honeycomb)
    (initrd-modules '())
    (firmware '())
    (kernel-arguments '("arm-smmu.disable_bypass=0"
                        "default_hugepagesz=1024m"
                        "hugepagesz=1024m"
                        "hugepages=2"
                        "isolcpus=1-15"
                        "iommu.passthrough=1"))

    (file-systems (cons* (file-system
                           (device (file-system-label "my-root"))
                           (mount-point "/")
                           (type "ext4"))
                         (file-system
                           (device (file-system-label "EFI"))
                           (mount-point "/boot/efi")
                           (type "vfat"))
                         %base-file-systems))

    ;;(swap-devices '("/swapfile"))

    (users (append %accounts %base-user-accounts))
    (services (cons* (service dhcp-client-service-type)
                     (service avahi-service-type)
                     (simple-service
                      'ci-host
                      hosts-service-type
                      (list (host "141.80.167.131" "ci.guix.gnu.org")))
                     (service openssh-service-type
                              (openssh-configuration
                               (password-authentication? #f)
                               (authorized-keys
                                `(("ludo" ,(local-file "../../keys/ssh/ludo.pub"))
                                  ("rekado" ,(local-file "../../keys/ssh/rekado.pub"))
                                  ("mathieu" ,(local-file "../../keys/ssh/mathieu.pub"))
                                  ("maxim" ,(local-file "../../keys/ssh/maxim.pub"))))
                               ;; Allow root login from berlin.guix.gnu.org
                               ;; for deployment purposes.
                               (extra-content "\
Match Address 141.80.167.131
  PermitRootLogin yes
Match Address 10.0.0.1
  PermitRootLogin yes\n")))
                     (service agetty-service-type
                              (agetty-configuration
                               (tty "ttyAMA0")
                               (keep-baud? #t)
                               (term "vt220")
                               (baud-rate "115200,38400,9600")))
                     (service cuirass-remote-worker-service-type
                              (cuirass-remote-worker-configuration
                               (workers 4)
                               (server "10.0.0.1:5555") ;berlin
                               (substitute-urls '("http://10.0.0.1"))
                               (systems '("armhf-linux" "aarch64-linux"))))
                     (service wireguard-service-type
                              (wireguard-configuration
                               (addresses (list wireguard-ip))
                               (peers
                                (list berlin-wireguard-peer))))

                     (service ntp-service-type)
                     (modify-services %base-services
                       (guix-service-type config =>
                                          (guix-configuration
                                           (inherit config)
                                           (max-silent-time (* 12 3600))
                                           (timeout (* 24 3600))
                                           (build-accounts 50)
                                           (authorized-keys
                                            %authorized-guix-keys)
                                           (substitute-urls
                                            '("http://10.0.0.1")) ;berlin
                                           (extra-options
                                            '("--max-jobs=4" "--cores=16")))))))
    (packages (cons* screen openssh strace
                     %base-packages))))
