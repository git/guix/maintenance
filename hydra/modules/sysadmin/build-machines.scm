;;; GNU Guix system administration tools.
;;;
;;; Copyright © 2016-2017, 2019-2024 Ludovic Courtès <ludo@gnu.org>
;;; Copyright © 2017, 2018, 2019, 2024 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2020 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (sysadmin build-machines)
  #:use-module (gnu)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader grub)
  #:use-module (gnu services avahi)
  #:use-module (gnu services base)
  #:use-module (gnu services cuirass)
  #:use-module (gnu services ssh)
  #:use-module (gnu services mcron)
  #:use-module (gnu services monitoring)
  #:use-module (gnu services networking)
  #:use-module (gnu services virtualization)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (sysadmin people)
  #:use-module (gnu packages ssh)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:export (build-machine-os
            berlin-new-build-machine-os
            childhurd-ip?
            build-vm-ip?
            berlin-offloading-service-type))

;;; Commentary:
;;;
;;; Configuration of build machines.
;;;
;;; Code:

(define* (build-machine-os host-name sysadmins
                           #:key (authorized-guix-keys '()))
  "Return the <operating-system> declaration for a build machine called
HOST-NAME and accessibly by SYSADMINS, with the given AUTHORIZED-GUIX-KEYS."
  (define gc-job
    ;; Run 'guix gc' at 3AM every day.
    #~(job '(next-hour '(3))
           "guix gc -F 40G"))

  (operating-system
    (host-name host-name)
    (timezone "Europe/Paris")
    (locale "en_US.UTF-8")

    (name-service-switch %mdns-host-lookup-nss)
    (bootloader (bootloader-configuration
                 (bootloader grub-bootloader)
                 (target "/dev/sda")))
    (file-systems (cons* (file-system
                           (device (file-system-label "my-root"))
                           (mount-point "/")
                           (type "ext4"))
                         (file-system
                           (device "tmpfs")
                           (mount-point "/var/guix/temproots")
                           (type "tmpfs")
                           (flags '(no-suid no-dev no-exec))
                           (check? #f))
                         %base-file-systems))

    (services (cons* (service sysadmin-service-type sysadmins)
                     (service openssh-service-type)
                     (service dhcp-client-service-type)
                     (service avahi-service-type
                              (avahi-configuration (debug? #t)))
                     (service mcron-service-type (list gc-job))
                     (modify-services %base-services
                       (guix-service-type config =>
                                          (guix-configuration
                                           (inherit config)
                                           (authorized-keys
                                            authorized-guix-keys))))))))

(define (childhurd-ip? ip)
  "Return #t if IP should be running a Childhurd."
  (member ip '("141.80.167.158" "141.80.167.159"
               "141.80.167.160" "141.80.167.161"
               "141.80.167.162" "141.80.167.163"
               "141.80.167.164" "141.80.167.165"
               "141.80.167.166" "141.80.167.167")))

(define (build-vm-ip? ip)
  "Return #t if IP should be running a virtual build machine."
  (member ip '("141.80.167.182" "141.80.167.183"
               "141.80.167.184" "141.80.167.185")))

(define* (berlin-new-build-machine-os id
                                      #:key
                                      (authorized-guix-keys '())
                                      (emulated-architectures '())
                                      (systems
                                       '("x86_64-linux" "i686-linux"))
                                      childhurd?
                                      build-vm?
                                      (max-jobs 4)
                                      (max-cores 16)
                                      (build-accounts-to-max-jobs-ratio 4))
  "Return the <operating-system> declaration for a build machine for
berlin.guixsd.org with integer ID, with the given AUTHORIZED-GUIX-KEYS.  Add a
'qemu-binfmt-service' for EMULATED-ARCHITECTURES, unless it's empty.  Start a
Cuirass remote worker building substitutes for the given SYSTEMS."

  (define gc-job
    ;; Run 'guix gc' at 3AM and 3PM every day.
    #~(job '(next-hour '(3 15))
           "guix gc -F 150G"))

  (define childhurd-gc-job
    ;; Run 'guix gc' at 2AM and 2PM every day.
    #~(job '(next-hour '(2 14))
           "guix gc -F 2G"))

  (define childhurd-os
    (operating-system
      (inherit %hurd-vm-operating-system)
      (host-name (format #f "berlin-childhurd-~3,'0d" id))
      (services
       (cons* (service mcron-service-type
                       (mcron-configuration
                        (jobs (list childhurd-gc-job))))
              (simple-service
               'ci-host
               hosts-service-type
               (list (host "141.80.167.131" "ci.guix.gnu.org")))
              (modify-services
               (operating-system-user-services %hurd-vm-operating-system)
               (guix-service-type
                config =>
                (guix-configuration
                 (inherit config)
                 (use-substitutes? #f)))
               (openssh-service-type
                 config =>
                 (openssh-configuration
                  (inherit config)
                  (authorized-keys
                   `(("hydra"
                      ,(local-file "../../keys/ssh/berlin.guixsd.org.pub")))))))))))

  (define (childhurd-net-options config)
    "Expose SSH and VNC ports on 0.0.0.0; for first Childhurd VM those
are 10022 and 15900.  Keep secret-service port local."
    `("--device" "rtl8139,netdev=net0"
      "--netdev" ,(string-append
                   "user,id=net0"
                   ",hostfwd=tcp:127.0.0.1:"
                   (number->string (hurd-vm-port
                                    config
                                    (@@ (gnu services virtualization)
                                        %hurd-vm-secrets-port)))
                   "-:1004"
                   ",hostfwd=tcp:127.0.0.1:"
                   (number->string (hurd-vm-port
                                    config
                                    (@@ (gnu services virtualization)
                                        %hurd-vm-ssh-port)))
                   "-:22"
                   ",hostfwd=tcp:127.0.0.1:"
                   (number->string (hurd-vm-port
                                    config
                                    (@@ (gnu services virtualization)
                                        %hurd-vm-vnc-port)))
                   "-:5900")))

  (define sysadmins
    (list (sysadmin (name "ludo")
                    (full-name "Ludovic Courtès")
                    (ssh-public-key (local-file "../../keys/ssh/ludo.pub")))
          (sysadmin (name "rekado")
                    (full-name "Ricardo Wurmus")
                    (ssh-public-key (local-file "../../keys/ssh/rekado.pub")))
          (sysadmin (name "mathieu")
                    (full-name "Mathieu Othacehe")
                    (ssh-public-key
                     (local-file "../../keys/ssh/mathieu.pub")))
          (sysadmin (name "maxim")
                    (full-name "Maxim Cournoyer")
                    (ssh-public-key (local-file "../../keys/ssh/maxim.pub")))
          (sysadmin (name "nckx")
                    (full-name "Tobias Geerinckx-Rice")
                    (ssh-public-key (local-file "../../keys/ssh/nckx.pub")))
          (sysadmin (name "cbaines")
                    (full-name "Christopher Baines")
                    (ssh-public-key (local-file "../../keys/ssh/cbaines.pub")))
          (sysadmin (name "hydra")      ;fake sysadmin
                    (full-name "Hydra User")
                    (restricted? #t)
                    (ssh-public-key
                     (local-file "../../keys/ssh/berlin.guixsd.org.pub")))))
  (operating-system
    (host-name (format #f "hydra-guix-~3,'0d" id))
    (timezone "Europe/Berlin")
    (locale "en_US.utf8")
    (name-service-switch %mdns-host-lookup-nss)
    (kernel-arguments '("console=tty0" "console=ttyS0,57600n8"))
    (keyboard-layout
     (keyboard-layout "us" "altgr-intl"))
    (bootloader
     (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (targets (list "/boot/efi"))
      (keyboard-layout keyboard-layout)
      (terminal-inputs '(serial))
      (terminal-outputs '(serial))))
    (initrd-modules (append (list "megaraid_sas")
                            %base-initrd-modules))
    (swap-devices (list (swap-space
                         (target "/dev/sda2"))))
    (file-systems
     (cons* (file-system
              (mount-point "/boot/efi")
              (device "/dev/sda1")
              (type "vfat"))
            (file-system
              (mount-point "/")
              (device (file-system-label "my-root"))
              (type "ext4"))
            %base-file-systems))
    (packages (cons* openssh %base-packages))
    (services
     (cons* (simple-service 'guile-load-path-in-global-env
                            session-environment-service-type
                            `(("GUILE_LOAD_PATH"
                               . "/run/current-system/profile/share/guile/site/3.0")
                              ("GUILE_LOAD_COMPILED_PATH"
                               . ,(string-append "/run/current-system/profile/lib/guile/3.0/site-ccache:"
                                                 "/run/current-system/profile/share/guile/site/3.0"))))
            (service agetty-service-type
                     (agetty-configuration
                      (tty "ttyS0")
                      (baud-rate "57600")))
            (service dhcp-client-service-type)
            (simple-service
             'ci-host
             hosts-service-type
             (list (host "141.80.167.131" "ci.guix.gnu.org")))
            (service zabbix-agent-service-type
                     (zabbix-agent-configuration
                      (server '("141.80.167.131"))
                      (server-active '("141.80.167.131"))))
            (service avahi-service-type
                     (avahi-configuration (debug? #t)))
            (service cuirass-remote-worker-service-type
                     (cuirass-remote-worker-configuration
                      (workers max-jobs)
                      (systems systems)
                      (substitute-urls '("http://141.80.167.131"))))
            (service mcron-service-type
                     (mcron-configuration
                      (jobs (list gc-job))))
            (service ntp-service-type
                     (ntp-configuration
                      (allow-large-adjustment? #t)))
            (service sysadmin-service-type sysadmins)
            (service openssh-service-type
                     (openssh-configuration
                      (extra-content "\
Match Address 141.80.167.131
  PermitRootLogin yes")))

            `(,@(if (null? emulated-architectures)
                    '()
                    (list (service qemu-binfmt-service-type
                                   (qemu-binfmt-configuration
                                    (platforms
                                     (apply lookup-qemu-platforms
                                            emulated-architectures))))))

              ,@(if (not childhurd?)
                    '()
                    (list (service hurd-vm-service-type
                                   (hurd-vm-configuration
                                    (os childhurd-os)
                                    ;; 6G should be enough to build 'hello'
                                    (disk-size (* 50000 (expt 2 20))) ;50G
                                    (memory-size (* 4 1024)) ;4GiB
                                    (net-options
                                     (childhurd-net-options this-record))))))

              ,@(if build-vm?
                    ;; A virtual build machine to build software in the past
                    ;; when needed.
                    (list (service virtual-build-machine-service-type
                                   (virtual-build-machine
                                    (cpu-count 16)
                                    (memory-size 8192))))
                    '())

              ,@(modify-services %base-services
                  (guix-service-type
                   config =>
                   (guix-configuration
                    (inherit config)
                    (authorized-keys
                     authorized-guix-keys)

                    ;; Enable substitution requests debug traces to
                    ;; investigate: https://issues.guix.gnu.org/48468.
                    (environment
                     '("GUIX_SUBSTITUTE_DEBUG=1"))

                    ;; Fetch only from berlin.guix.gnu.org, using its local
                    ;; IP address.
                    (substitute-urls '("http://141.80.167.131"))

                    (max-silent-time 3600)
                    (timeout (* 6 3600))
                    (build-accounts
                     (* build-accounts-to-max-jobs-ratio max-jobs))
                    (extra-options
                     (list "--max-jobs"
                           (number->string max-jobs)
                           "--cores"
                           (number->string max-cores)))))))))
    ;; Allow sysadmins (sudoers) to use 'sudo' without a password so
    ;; they can 'guix deploy' these machines as their own user.
    (sudoers-file
     (plain-file "sudoers"
                 (string-join
                  (append (remove (cut string-prefix? "%wheel" <>)
                                  (string-split
                                   (string-trim-right (plain-file-content
                                                       %sudoers-specification))
                                   #\newline))
		          (list "%wheel ALL = NOPASSWD: ALL\n")) "\n")))))


;;;
;;; List of build machines for offloading.
;;;

;; These are all hosted at the MDC in Berlin Buch.  They are connected to
;; a dedicated VLAN and can only be accessed from berlin.guix.gnu.org.
(define hosts
  '(;;; New machines.  We should use DNS for them in the future.
    ;; hydra-guix-101
    ("141.80.167.158"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGqLy+LVndyfuzwZmln/nrHylAN7FotSmso9kZaYPpzo"
     128)
    ;; hydra-guix-102
    ("141.80.167.159"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEq4YoQHA0ShXIVbk7E4Jh4KZRPrt1EN9DYniraR8oYj"
     128)
    ;; hydra-guix-103
    ("141.80.167.160"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICiFpDx+NIVHD4ffZotDyJDdEiwo8Cy8fAQU6cLt6mT/"
     128)
    ;; hydra-guix-104
    ("141.80.167.161"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINuVkwaeU+ddDpDQoxyFboiBnRNyhGDT8yOy8VAyJxZ6"
     128)
    ;; hydra-guix-105
    ("141.80.167.162"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIH9o9VrkR2OKoGeuyJkzSsLIaDVApkbHEQvgr8aywQf8"
     128)
    ;; hydra-guix-106
    ("141.80.167.163"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBWN8i6YSGaRddTUgjodvQ4+g+6qYRe+0t9Mi8zOXawG"
     128)
    ;; hydra-guix-107
    ("141.80.167.164"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAII+nI0XnLKShi3tZEdPdEVQ1VLlZjgQNSKMTK55FwH/4"
     128)
    ;; hydra-guix-108
    ("141.80.167.165"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHvMT+OlslyCzp7PvIvG/m9aCNhk3jnGS4kh8Cxh26CK"
     128)
    ;; hydra-guix-109
    ("141.80.167.166"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHkmH+o9P2kmgtjyGU9/vLEmFbxwUlq62lWu3lLc1J5o"
     128)
    ;; hydra-guix-110
    ("141.80.167.167"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM2A2GxCw3oF6W2a5P9/K/jw1BWNJdAy9cr7NLRWvHVl"
     128)
    ;; hydra-guix-111
    ("141.80.167.168"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILJoZitLeltTfd7dDAnRbuP1uCWmTsYjIKALcadXknMl"
     128)
    ;; hydra-guix-112
    ("141.80.167.169"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFS6PDp6MVutJiieJgDaLvub83oeTvWYLJnELxqCyO7x"
     128)
    ;; hydra-guix-113
    ("141.80.167.170"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMn5ujp4uTRVwYGPr2kgh7YMXISj+WyRxe8cGxzb1KrL"
     128)
    ;; hydra-guix-114
    ("141.80.167.171"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIF4ST+J7Xdmrft+sD1HEOAjADA+QZ+hMXRV3PnN0Rs+A"
     128)
    ;; hydra-guix-115
    ("141.80.167.172"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG9zXGZ5b6QroN4RybnKLIMZwKtFuMpsNypkUXdFmH88"
     128)
    ;; hydra-guix-116
    ("141.80.167.173"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFg0y4UyuTkYoa3hwqj2ByQXYBMQdbPKz7nEz7I1lquL"
     128)
    ;; hydra-guix-117
    ("141.80.167.174"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINPPjhX6Z3bgt7EZmIfUdsgFnqp3yLr4msccjwsD2Q8F"
     128)
    ;; hydra-guix-118
    ("141.80.167.175"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJfJfTctnzEzVBLZxIq4WIOWY0s9JHcvIztdIYSFlklH"
     128)
    ;; hydra-guix-119
    ("141.80.167.176"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGE6FwL94/YDJIioQsLqh/MnwGcXmKYARd/kBGs+RWM7"
     128)
    ;; hydra-guix-120
    ("141.80.167.177"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIH3rXQZCQbVliJUgavSxNWvA4XUX7cXj7zd5VvUggCbv"
     128)
    ;; hydra-guix-121
    ("141.80.167.178"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGNVaPCyKRrprBivEWYmtVecaJ+DIkET3gCYzGOuRAcz"
     128)
    ;; hydra-guix-122
    ("141.80.167.179"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHladb6HkAEmITzNOmI1kH7A4R1MiKp0Y72aPJNwuIDB"
     128)
    ;; hydra-guix-123
    ("141.80.167.180"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOM29Lj7rNDDsU5JOuDgFGfepWY9WHs6WaMLj9/7IceX"
     128)
    ;; hydra-guix-124
    ("141.80.167.181"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIODiKP7qIkkDeqvzKG2JsrDlNRe3CTN+icGgQ1J5ZUP+"
     128)
    ;; hydra-guix-125
    ("141.80.167.182"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPrlasUtgZgKfJ0oNhBQx/2QIQ+J+jbAT842VoJlBhor"
     192)
    ;; hydra-guix-126
    ("141.80.167.183"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIUprP1E2cRkMrwBnl1FkeCQ5UhZRin6dKQrB9p4WrV6"
     192)
    ;; hydra-guix-127
    ("141.80.167.184"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHdrEcEoo2AQ6aDXhLUWxLhp4kTq+DJLwXxvgu4As1bo"
     192)
    ;; hydra-guix-128
    ("141.80.167.185"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAIomlYpFmdaTiWGf4DWs6sc831zbNlU5XBjicHmZINA"
     192)
    ;; hydra-guix-129
    ("141.80.167.186"
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMuCdrMoF25T9ejPLAAcS92b6lVIz5+U0avyYPQTG5NI"
     192)
    ;; hydra-guix-130
    ;; FIXME: Disabled Nov 19 2022; waiting troubleshooting from
    ;; Madalin (segfaults in libc).
    ;; ("141.80.167.187"
    ;;  "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICZilog+9Jdim9k07baYK6QZfkZRZbQQriExjtOEfjQ5"
    ;;  192)
    ))

(define template-x86_64
  (match-lambda
    ;; Prefer building on the new nodes.
    ((ip key 128)
     #~(build-machine
        (name #$ip)
        (user "hydra")
        (systems
         ;; Some of these machines run a childhurd, which they offload to (we
         ;; effectively have two-level offloading, then).
         '#$(append (if (childhurd-ip? ip)
                        '("i586-gnu")
                        '())
                    '("x86_64-linux" "i686-linux")))
        (host-key #$key)
        (compression "no")
        (speed 3)                                 ;don't increase it too much
                                                  ;or everything goes there
        (parallel-builds 8)))
    ((name key 192)
     #~(build-machine
        (name #$name)
        (user "hydra")
        (systems '("x86_64-linux" "i686-linux"))
        (host-key #$key)
        (compression "no")
        (speed 3)                                 ;don't increase it too much
                                                  ;or everything goes there
        (parallel-builds 8)))
    ((ip key ram)
     #~(build-machine
        (name #$ip)
        (user "hydra")
        (systems '("x86_64-linux" "i686-linux"))
        (host-key #$key)
        (compression "no")
        (speed 2)
        (parallel-builds 2)))))

(define overdrive
  ;; The SoftIron OverDrive 1000 donated by ARM:
  ;; CPU: AMD A1100 (4 Cortex A57 cores)
  ;; RAM: 8 GB
  (list #~(build-machine
           ;;overdrive1
           (name "10.0.0.3")
           (user "hydra")
           (overload-threshold 1.2)
           (systems '("aarch64-linux" "armhf-linux"))
           (host-key
            "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPf2f93c90oi9s9qGVGWC3sDgG7kEBvIEwR021NsfG+z root@overdrive")
           (parallel-builds 2))

        ;; 2022-02-17: cannot be reached.
        #;
        (build-machine
         ;;dover
         (name "10.0.0.4")
         (user "hydra")
         (overload-threshold 1.2)
         (systems '("aarch64-linux" "armhf-linux"))
         (host-key
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJLRYD5RXZ3Espe+Kv1SzZl8Qc3NZ356Bq+cGjnKsDHY root@linux")
         (parallel-builds 2))))

(define honeycomb
  ;; SolidRun LX2160A Honeycomb
  ;; CPU: 16 ARM Cortex-A72 cores
  ;; RAM: 32 GB
  (list #~(build-machine
           ;;pankow
           (name "10.0.0.8")
           (user "hydra")
           (overload-threshold 1.2)
           (speed 2.0)                            ; prefer over overdrives
           (systems '("aarch64-linux" "armhf-linux"))
           (host-key
            "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMiOfBwh78K3KNEV1ZQf0pyVtYFSoLgWryMMy0GdMJ0H")
           (parallel-builds 4))
        #~(build-machine
           ;;kreuzberg
           (name "10.0.0.9")
           (user "hydra")
           (overload-threshold 1.2)
           (speed 2.0)                            ; prefer over overdrives
           (systems '("aarch64-linux" "armhf-linux"))
           (host-key
            "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFjixKdkTBoEUflxX/n/flhg7GoqbfkfoVrhD0GROZxl")
           (parallel-builds 4))
        #~(build-machine
           ;;grunewald
           (name "10.0.0.10")
           (user "hydra")
           (overload-threshold 1.2)
           (speed 2.0)                            ; prefer over overdrives
           (systems '("aarch64-linux" "armhf-linux"))
           (host-key
            "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIB9slskCGIBFwRRzsWmePIsMJ8W1muqvDIgPG3xQeu6")
           (parallel-builds 4))))

(define powerpc64le
  (list
   ;; guixp9 - A VM donated/hosted by OSUOSL & administered by nckx.
   ;; 8 POWER9 2.2 (pvr 004e 1202) cores, 16 GiB RAM, 160 GB storage.
   #~(build-machine
      (name "10.0.0.7")
      (user "hydra")
      (systems '("powerpc64le-linux"))
      (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJEbRxJ6WqnNLYEMNDUKFcdMtyZ9V/6oEfBFSHY8xE6A nckx"))
   ;; sjd-p9 - A VM donated/hosted by Simon Joseffson, but blame nckx for any problems.
   ;; 32 POWER9 2.3 (pvr 004e 1203) cores, 64 GiB RAM, 16 GB / + 256 GB /gnu storage.
   #~(build-machine
      (parallel-builds 16)
      (speed 4.0)
      (name "10.0.0.13")
      (user "hydra")
      (systems '("powerpc64le-linux"))
      (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMUkktI2HAycb4nqWwVBn5OCe5dyF4pbjqvyPTICz/9A nckx"))))

(define %berlin-build-machines
  (append overdrive
          honeycomb
          powerpc64le
          (map template-x86_64 hosts)))

(define berlin-offloading-service-type
  (service-type
   (name 'berlin-offloading)
   (extensions
    (list (service-extension guix-service-type
                             (const
                              (guix-extension
                               (build-machines %berlin-build-machines))))))
   (description "Offloading setup for the Berlin build farm.")
   (default-value #t)))

;;; build-machines.scm end here
