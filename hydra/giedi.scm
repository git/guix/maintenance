;; Guix System configuration file for a second generation Ivy Bridge NUC
;; Copyright © 2022 Christopher Baines <mail@cbaines.net>
;; Released under the GNU GPLv3 or any later version.

(use-modules (gnu))
(use-service-modules desktop networking ssh xorg guix monitoring)

(operating-system
  (host-name "giedi")
  (timezone "Europe/London")
  (locale "en_GB.utf8")

  (keyboard-layout (keyboard-layout "gb"))

  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (target "/dev/sda")
               (keyboard-layout keyboard-layout)))

  (swap-devices
    (list (uuid "2f702555-e270-473a-b572-e9d29fcb2786")))
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device
               (uuid "70a0c633-e805-4794-89bc-9ce45ce4a883"
                     'ext4))
             (type "ext4"))
           %base-file-systems))

  (users (cons* (user-account
                  (name "cbaines")
                  (comment "Chris")
                  (group "users")
                  (home-directory "/home/cbaines")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))

  (services
   (cons*
    (service openssh-service-type
             (openssh-configuration
              (password-authentication? #f)))
    (service dhcp-client-service-type)

    (service guix-build-coordinator-agent-service-type
             (guix-build-coordinator-agent-configuration
              (coordinator "https://coordinator.bordeaux.guix.gnu.org")
              (authentication
               (guix-build-coordinator-agent-password-file-auth
                (uuid "c8a787dd-1e6c-4a31-a2ba-cbab7917fafd")
                (password-file
                 "/etc/guix-build-coordinator-agent-password")))
              (max-parallel-builds 6)
              (max-1min-load-average 5)
              (systems '("x86_64-linux" "i686-linux"))
              (derivation-substitute-urls
               (list "https://data.guix.gnu.org"
                     "https://data.qa.guix.gnu.org"))
              (non-derivation-substitute-urls
               (list "https://bordeaux.guix.gnu.org"))))

    (service prometheus-node-exporter-service-type)

    (modify-services
        %base-services
      (guix-service-type config =>
                         (guix-configuration
                          (inherit config)
                          (max-silent-time (* 12 3600))
                          (timeout (* 72 3600))
                          (authorized-keys
                           (list
                            (local-file "keys/guix/bordeaux.guix.gnu.org-export.pub")
                            (local-file "keys/guix/data.guix.gnu.org.pub")
                            (local-file "keys/guix/data.qa.guix.gnu.org.pub")))
                          (substitute-urls
                           (list "https://bordeaux.guix.gnu.org"))
                          (extra-options '("--max-jobs=5"))))))))
