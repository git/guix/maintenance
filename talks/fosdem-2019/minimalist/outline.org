#+TITLE: Building a whole distro on top of a minimalistic language

* Summary

GNU Guix is a package manager, GNU/Linux distribution, and more
generally a “software deployment toolbox” featuring key features such as
transactional upgrades and rollbacks, declarative operating system
configuration, and more.  Guix has become a relatively large piece of
software but a key aspect of it is that Guix builds on Scheme, a Lisp
dialect known for its emphasis on minimalism.  From day one, use of
Scheme as its single implementation language has shaped Guix.

This talk will be about what it means for Guix to be implemented as a
set of libraries on top of this tiny core that Scheme is.  Together we
will have a guided tour through Guix’ interfaces, be they “programming”
or “user” interfaces; we’ll look at its embedded domain-specific
languages (EDSLs) that make it just as clear as your favorite
YAML/JSON/XML thing while retaining the full power (and joy!) of a
general-purpose programming language.  We’ll discuss the features of
Scheme that make it possible.  I will share my thoughts on how such a
design can empower users and how well this has worked in practice.

* Abstract

GNU Guix is a package manager and GNU/Linux distribution that builds on
Scheme, a Lisp dialect known for its emphasis on minimalism.  This talk
will be an exploration of Guix’ use of Scheme’s extensibility to build
high-level abstractions and user interfaces in a broad sense.

* intro

** Babel tower of GNU/Linux

** proliferation of specific “languages”

** “auberge espagnole” or “potluck”

** a strength, but also a hindrance for users: practical user freedom?

* Lisp mystique

** “not by piling feature upon feature”

** “The truth is that Lisp …” (GHM 2014)

** [[https://cacm.acm.org/magazines/2018/3/225475-a-programmable-programming-language/fulltext][“language-oriented programming” with Racket]]

** “escaping DSL hell by having parentheses all the way down”

* GNU/Linux distro as a Scheme library

** minimalism: 500K SLOC
** thesis: embedding the DSL provides tools: lint, refresh, etc.

*** full-blown language supports abstraction ("package" objects, etc.)

*** EDSL thanks to macros (deep or shallow embedding)

*** expose data structures

*** IDE

** why stop here? ELS diagram Nix/Guix side by side

* Unification beyond the distro


** the problem

  - lots of glueing, no big picture
    + different languages, approaches, config file syntax
    + sometimes redundant

** The initrd: 'expression->initrd'

** VMs: 'expression->linux-vm'

** PID 1: example Shepherd service

*** dependency graph of services

*** static checks

** more services: mcron

** service in container

