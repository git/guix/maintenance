(use-modules (gnu) (guix) (srfi srfi-1))
(use-service-modules desktop mcron networking xorg)
(use-package-modules certs fonts xorg)

(operating-system
  [...]
  (packages (append (list font-bitstream-vera nss-certs)
                    %base-packages))
  (services
   (append (list (service xfce-desktop-service-type)
                 (simple-service 'cron-jobs mcron-service-type
                    (list auto-update-resolution-crutch))
                 (service dhcp-client-service-type)))))
