(use-modules (gnu) (guix) (srfi srfi-1))
(use-service-modules desktop mcron networking xorg)
(use-package-modules certs fonts xorg)

(operating-system
  (host-name "gnu")
  (keyboard-layout (keyboard-layout "us" "altgr-intl"))

  (users (cons (user-account
                (name "guest")
                (password "") ;no password
                (group "users"))
               %base-user-accounts))

  (packages (append (list font-bitstream-vera nss-certs)
                    %base-packages))

  (services
   (append (list (service xfce-desktop-service-type)
                 (simple-service 'cron-jobs mcron-service-type
                                 (list auto-update-resolution-crutch))
                 (service dhcp-client-service-type)))))
