\documentclass[aspectratio=169]{beamer}

\usetheme{default}

\usefonttheme{structurebold}
\usepackage{helvet}
\usepackage{multimedia}         % movie
\usecolortheme{seagull}         % white on black

\usepackage[utf8]{inputenc}
\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref,xspace,multicol}
\usepackage[absolute,overlay]{textpos}
\usepackage{tikz}
\usetikzlibrary{arrows,shapes,trees,shadows,positioning, decorations.pathreplacing}
\usepackage{fancyvrb}           % for \Verb

\usepackage{ulem}

% Remember the position of every picture.
\tikzstyle{every picture}+=[remember picture]

\tikzset{onslide/.code args={<#1>#2}{%
  \only<#1>{\pgfkeysalso{#2}} % \pgfkeysalso doesn't change the path
}}

% Colors.
\definecolor{guixred1}{RGB}{226,0,38}  % red P
\definecolor{guixorange1}{RGB}{243,154,38}  % guixorange P
\definecolor{guixyellow}{RGB}{254,205,27}  % guixyellow P
\definecolor{guixred2}{RGB}{230,68,57}  % red S
\definecolor{guixred3}{RGB}{115,34,27}  % dark red
\definecolor{guixorange2}{RGB}{236,117,40}  % guixorange S
\definecolor{guixtaupe}{RGB}{134,113,127} % guixtaupe S
\definecolor{guixgrey}{RGB}{91,94,111} % guixgrey S
\definecolor{guixdarkgrey}{RGB}{46,47,55} % guixdarkgrey S
\definecolor{guixblue1}{RGB}{38,109,131} % guixblue S
\definecolor{guixblue2}{RGB}{10,50,80} % guixblue S
\definecolor{guixgreen1}{RGB}{133,146,66} % guixgreen S
\definecolor{guixgreen2}{RGB}{157,193,7} % guixgreen S

\setbeamerfont{title}{size=\huge}
\setbeamerfont{frametitle}{size=\huge}
\setbeamerfont{normal text}{size=\Large}

% White-on-black color theme.
\setbeamercolor{structure}{fg=guixorange1,bg=black}
\setbeamercolor{title}{fg=white,bg=black}
\setbeamercolor{date}{fg=guixorange1,bg=black}
\setbeamercolor{frametitle}{fg=white,bg=black}
\setbeamercolor{titlelike}{fg=white,bg=black}
\setbeamercolor{normal text}{fg=white,bg=black}
\setbeamercolor{alerted text}{fg=guixyellow,bg=black}
\setbeamercolor{section in toc}{fg=white,bg=black}
\setbeamercolor{section in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsubsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsubsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{frametitle in toc}{fg=white,bg=black}
\setbeamercolor{local structure}{fg=guixorange1,bg=black}

\newcommand{\highlight}[1]{\alert{\textbf{#1}}}
\newcommand{\orange}[1]{\textcolor{guixorange1}{\textbf{#1}}}
\newcommand{\comment}[1]{\textcolor{guixgreen2}{#1}}
\newcommand{\red}[1]{\textcolor{guixred2}{#1}}


\title{Computational reproducibility \\ with GNU Guix: \\
  \Large{application to multiparametric cytometry}}

\author{Simon Tournier${}^1$ \& Nicolas Vallet${}^2$}
\date{iPOP-UP, September 9th, 2021}
\institute{%
  \texttt{simon.tournier@u-paris.fr}
  \  \
  \texttt{nicolas.vallet@inserm.fr}}

\setbeamertemplate{navigation symbols}{} % remove the navigation bar

\AtBeginSection[]{
  \begin{frame}
    \frametitle{}
    \tableofcontents[currentsection]
  \end{frame}
}


\newcommand{\screenshot}[1]{
  \begin{frame}[plain]
    \begin{tikzpicture}[remember picture, overlay]
      \node [at=(current page.center), inner sep=0pt]
        {\includegraphics[width=\paperwidth]{#1}};
    \end{tikzpicture}
  \end{frame}
}


\begin{document}


\begin{frame}[plain, fragile]
  \vspace{10mm}
  \titlepage

  \vfill{}
  ${}^1$Research Eng. Sci. Computing \ \scriptsize{(+little admin-sys)}

  \normalsize
  ${}^2$PhD student \ \scriptsize{(and MD)}

  \normalsize
  \hfill{%
    \begin{minipage}{0.2\paperwidth}
      \begin{tabular}{c|c}
        \includegraphics[width=0.1\paperwidth]{images/diderot_u-paris}
        &
          \includegraphics[width=0.04\paperwidth]{images/diderot_u-paris_bis}
      \end{tabular}
    \end{minipage}
  }
\end{frame}


\begin{frame}[plain]
  \begin{center}
    \begin{tabular}{rcl}
      \textcolor{guixtaupe}{Science} & \textcolor{guixtaupe}{=} & \textcolor{guixtaupe}{Transparency}
      \\
      \textcolor{guixblue1}{Scientific result} & \textcolor{guixblue1}{=} &
                                                                            \textcolor{guixblue1}{Experiment
                                                                            +
                                                                            \only<1>{{Numerical treatment}}
                                                                            \only<2>{\emph{Numerical treatment}}}
    \end{tabular}
  \end{center}

%  \vfill{}

  \begin{center}
    \highlight{Science at the numerical age:}
  \end{center}

  \begin{tabular}{cll}
    \orange{1.} & Open Article & HAL, BioArxiv
    \\
    \orange{2.} & Open Data & Zenodo
    \\
    \orange{3.} & Open Source & GitHub, Software Heritage
    \\
    \uncover<2->{\orange{4.}} & \uncover<2->{\fbox{\comment{Computational env.}}} &  \uncover<2->{\alert{?}}
  \end{tabular}

  \begin{center}
    {and how to \only<1>{glue}\only<2>{\emph{glue}} all that?}
  \end{center}

  \vfill{}

    $\left(
    \begin{tabular}{lcl}
      reproductibility &=& verification\\
      replicability &=& validation
    \end{tabular}
    \right)$

    \vspace{-0.75cm}

  \begin{flushright}
    \href{https://scibian.org/up/201609/Software_Heritage.pdf}%
    {\small{R. Di Cosmo@Scibian2016 \sout{(pdf)}}}\\
    \href{https://aramis.resinfo.org/wiki/lib/exe/fetch.php?media=pleniaires:aramis_keynote_enjeux-et-defis-recherche-reproductible_konrad_hinsen.pdf}%
    {\small{K. Hinsen@Aramis2019 (pdf)}}
  \end{flushright}
\end{frame}


\begin{frame}{Redo (reproduce or replicate) a result?}
  \begin{tabular}{clclclcl}
           &&& \orange{audit} && \red{opaque} && \comment{depend?}
    \\ \hline \\
    & result &$\longleftarrow$& \orange{paper} &+& \red{data} &+& \comment{analysis}
    \\ \\
    & data &$\longleftarrow$& \orange{protocol} &+& \red{instruments} &+& \comment{materials} (reagent, etc.)
    \\
    \uncover<2->{\orange{$\rhd$}} & analysis &$\longleftarrow$& \orange{script} &+& \red{data} &+&
                                               \only<-3>{\comment{environment}}\uncover<4->{\fbox{\comment{environment}}}
    % \\ \\ \hline \\
    % & environment &$\longleftarrow$& \orange{source} &+& \red{builder} (package) &+& packages
    % \\
    % & package &$\longleftarrow$& \orange{source} &+& \red{executable} &$\Big($+& packages$\Big)$
  \end{tabular}

  \vfill
  \begin{center}
    \begin{minipage}[c]{.8\linewidth}
    \begin{itemize}
    \item \orange{audit} is the ``easy'' part
    \item \red{opaque} is generally the hard part
    \item<3-> how to evacuate \comment{depend?} from the equations%
      \uncover<4->{\dots \\ \hfill \dots at least let speak about \comment{environment}}
    \end{itemize}
    \end{minipage}
  \end{center}
\end{frame}


\begin{frame}{Concrete scenarii about environment}
  \begin{itemize}
  \item Alice used \emph{numerical tools} \texttt{R@4.1.1}, \texttt{FlowCore@2.4} and \texttt{CATALYST@1.16.2}.
  \item Carole \emph{collaborates} with Alice \dots \\
    \hfill{but also requires \texttt{R@4.0.4}, \texttt{FlowCore@2.0} for another project.}
  \item Charlie upgrades their system then all is broken.
  \item Bob runs the \emph{same versions} as Alice \dots \\
    \hfill{but does \emph{not} get the \emph{same outputs}.}
  \item Dan tries to \emph{redo} the analysis months (years?) later \dots\\
    \hfill{but hits the \emph{dependencies hell}\footnote{at best :-)}.}
  \end{itemize}
  \uncover<2->{
  \begin{center}
    \highlight{Solution(s)}
  \end{center}

  \vspace{-0.6cm}

  \begin{tikzpicture}[decoration=brace]
    \node[text width=0.6\paperwidth] (box)
    {
      \begin{enumerate}
      \item \textbf{package manager}: \texttt{apt}, \texttt{yum}, etc.
      \item \textbf{virtual environment}: \texttt{conda}, \texttt{modulefiles}, etc.
      \item \textbf{container}: Docker, Singularity, etc.
      \end{enumerate}
    };
    \draw [decorate] (box.north east) --node[right=10pt]{\alert{Guix}} (box.south east);
  \end{tikzpicture}
  }
\end{frame}


\begin{frame}[fragile]{Why is it complicated?}
  \begin{semiverbatim}
    alice@laptop\$ R -e '1+2'
    > 1+2
    [1] 3
  \end{semiverbatim}

  \vspace{-1cm}
  \uncover<2->{
  \begin{center}
    What is the issue?
    \orange{R is Open Source}
    \\
    \href{https://svn.r-project.org/R/trunk/src/main/main.c}%
    {https://svn.r-project.org/R/trunk/src/main/\orange{main.c}}
  \end{center}
  }

  \vspace{-0.25cm}
  \uncover<3->{
  \begin{semiverbatim}
    alice@laptop\$ file \only<-8>{/usr/lib/R/bin/exec/R}\only<9->{\red{/usr/lib/R/bin/exec/R}}

    /usr/lib/R/bin/exec/R: \only<3>{executable}\only<4->{\red{executable}}, dynamically \only<3-8>{linked}\only<9->{\comment{linked}}, ...
  \end{semiverbatim}
  }

  \uncover<5-8>{
    Recipe to make an \only<5>{yogurt}\only<6->{\sout{yogurt} executable}:
    \begin{center}
      \begin{tabular}{lclcl}
        Yogurt &$\longleftarrow$& Milk &+& Skyr\only<5-8>{\footnote{Icelandic strained yogurt}}
        \uncover<6->{
        \\
        \red{executable} && \orange{source} && \only<5-7>{\red{executable}}
        } \uncover<7->{\\
        R && \texttt{main.c} && C compiler
        } \uncover<8->{\\
          && && \red{executable} $\leftarrow$ \orange{source} ~+~ \red{executable}
        }
      \end{tabular}
    \end{center}
  }

  \vspace{-2.25cm}
  \begin{semiverbatim}
  \uncover<10-11>{
    alice@laptop\$ ldd \red{/usr/lib/R/bin/exec/R}\only<10>{
        libblas.so.3 => /usr/lib/x86_64-linux-gnu/libblas.so.3
        libR.so => /usr/lib/libR.so
        libgfortran.so.5 => /usr/lib/x86_64-linux-gnu/libgfortran.so.5
        libquadmath.so.0 => /usr/lib/x86_64-linux-gnu/libquadmath.so.0}\only<11->{
        libblas.so.3 => \red{/usr/lib/x86_64-linux-gnu/libblas.so.3}
        libR.so => \red{/usr/lib/libR.so}
        libgfortran.so.5 => \red{/usr/lib/x86_64-linux-gnu/libgfortran.so.5}
        libquadmath.so.0 => \red{/usr/lib/x86_64-linux-gnu/libquadmath.so.0}}
        ...}
  \end{semiverbatim}

  \only<12->{
  \vspace{-1.25cm}
  \begin{flushright}
    \href{https://webcast.in2p3.fr/video/les-enjeux-et-defis-de-la-recherche-reproductible}%
    {\small{K. Hinsen@Aramis2019 (video)}}

    \href{https://hpc.guix.info/blog/2020/01/reproducible-computations-with-guix/}%
    {Blog: Reproducible computations with Guix (link)}
  \end{flushright}
  }
\end{frame}


\begin{frame}{Is it really so complicated?}
  \begin{itemize}
  \item Wait!  \highlight{Package managers}
    (\texttt{apt}, \texttt{yum}, \texttt{conda}, \texttt{brew}, etc.)
    do the \emph{job for us}.
  \item Yes, but\dots \ \uncover<7->{e.g., \highlight{Conda is not enough}}
    \begin{center}
      \begin{tabular}{cclclcl}
        \uncover<2->{
        Yogurt &$\longleftarrow$& Milk &+& Skyr &+& Utensils
        \\
        \red{executable} && \orange{source} && \red{executable} &&
                                                                   \comment{links}(executable too)
        \\
        R && \texttt{main.c} && C compiler && Deps. (libblas, etc.)
        } \uncover<3->{
        \\ \\
        \hline
        \\
        R@4.1.1 &$\longleftarrow$& source\orange{@4.1.1} &+& executable\red{@A} &+& links\comment{@C}
        \\
        \uncover<5->{$\left|\right| ?$}
        \\
        \uncover<4->{R@4.1.1 &$\longleftarrow$& source\orange{@4.1.1} &+& executable\red{@B} &+& links\comment{@D}}
        }
      \end{tabular}
    \end{center}
    \uncover<6->{
  \item Too many combinations to have the same environment on different
    machines
    \highlight{when something is going wrong,
      it is hard to say from where it comes}
    \begin{center}
      is it the environment? is it the analysis? something else?
    \end{center}
    }
  \end{itemize}
  \uncover<7->{
  \highlight{Conclusion}: Tools allowing to \emph{capture the \comment{environment}} (= \orange{@4.1.1}, \red{@A}, \comment{@C})
  }

\end{frame}


\begin{frame}[fragile]
  \begin{itemize}
  \item<1-> Alice says she uses R@4.1.1
    \begin{semiverbatim}
  alice@laptop\$ install r\orange{@4.1.1} \uncover<5->{\comment{# gcc-toolchain}\red{@7.5.0}}
  alice@laptop\$ R --version
  R version \orange{4.1.1} (2021-08-10) -- "Kick Things"
    \end{semiverbatim}
  \item<2-> Bob runs this same version @4.1.1
    \begin{semiverbatim}
   bob@cluster\$ install r\orange{@4.1.1} \uncover<5->{\comment{# gcc-toolchain}\red{@9.4.0}}
   bob@cluster\$ R --version
  R version \orange{4.1.1} (2021-08-10) -- "Kick Things"
    \end{semiverbatim}
  \item<3-> but Bob does not get the same result (e.g., precision, performance, other)
  \item<4-> \highlight{why? what is different?} both uses R\orange{@4.1.1}
    \uncover<5->{
    \begin{semiverbatim}
      \orange{$\rhd$} \$ diff with-\{\red{7.5.0},\red{9.4.0}\}/lib/R/bin/exec/R

       \quad \ Binary files differ
    \end{semiverbatim}
    \orange{$\rhd$} \ some \comment{links} (\texttt{libquadmath.so}) are at different versions
    }
  \end{itemize}
 \uncover<6->{
  \highlight{Conclusion}: Version \orange{@4.1.1} of \orange{source} is
  \emph{not enough} for reproducing

  \begin{center}
    Reproducibility requires to capture the \comment{environment}
  \end{center}
}
\end{frame}


\begin{frame}{Concretely, just R requires a lot!}
  %%% guix graph r-minimal | dot -Tpng -Gsize=50,25\!  -Gratio=fill -Nstyle=filled > images/r-minimal.png
  \includegraphics[width=\textwidth]{images/r-minimal}
\end{frame}


\begin{frame}{Usual (soon?) published result}
  \begin{tabular}{cc}
    \begin{minipage}{0.7\textwidth}
      \includegraphics[width=\textwidth]{images/figures}
    \end{minipage}
    &
      \begin{minipage}{0.3\textwidth}
        \begin{itemize}
        \item Cytometry CyTOF: 43 parameters
        \item zillions of events (cells)
        \item FlowJo (cleaning)
        \item R plus 19 packages from BioConductor
        \end{itemize}
    \end{minipage}
  \end{tabular}
\end{frame}


\begin{frame}{How to redo this (soon?) published result}
  \begin{center}
    \highlight{Assuming nothing is lost and all is transparent}
  \end{center}
  \begin{tabular}{lclclcl}
           && \orange{audit} && \red{opaque} && \comment{depend?}
    \\ \hline \\
    result &$\longleftarrow$& \orange{paper} &+& \red{data} &+& \comment{analysis}
    \\ \\
    data &$\longleftarrow$& \orange{protocol} &+& \red{instruments} &+& \comment{materials}
    \\
    analysis &$\longleftarrow$& \orange{script} &+& \red{data} &+& \fbox{\comment{environment}}
    \\ \\ \hline

  \end{tabular}
  \begin{center}
    \comment{environment} = collection of packages
  \end{center}
  \begin{tabular}{lclclcl}
    % environment &$\longleftarrow$& \orange{source} &+& \red{builder} (package) &+& packages
    % \\
    package &$\longleftarrow$& \orange{source} &+& \red{executable} &$\Big($+& others packages$\Big)$
  \end{tabular}

  \uncover<2->{
    Alice says she uses \texttt{R@4.1.1} with \texttt{CATALYST@1.16.2} from
    Bioconductor means:}
  \uncover<3->{
  \begin{itemize}
  \item \texttt{R}: 40 input packages and 40 build packages (but 408
    closure packages)
  \item \texttt{CATALYST}: 27 input packages and 48 build packages
    \\ \hfill (but 907 closure packages)
  \end{itemize}
  }
\end{frame}


\begin{frame}{Redo data is complicated}
  \vspace{-1.35cm}
  \begin{center}
    \includegraphics[width=\textwidth]{images/pipe}
  \end{center}
\end{frame}


\begin{frame}{Redo analysis: use Guix!}

  \begin{itemize}
  \item<1-> Alice lists the packages with the file
    \orange{\texttt{requirements.scm}}
    \only<1>{
      \begin{semiverbatim}
(specifications->manifest

\quad (list

\quad\quad "r"

\quad\quad "r-catalyst"

\quad\quad "r-flowcore"))
      \end{semiverbatim}

    }
  \item<2-> Alice describe her current state
    \begin{semiverbatim}
  alice@laptop\$ guix describe

  Generation 65	Aug 16 2021 20:42:44	(current)

  \quad guix \comment{a9eb969}

\quad\quad    repository URL: https://git.savannah.gnu.org/git/guix.git

\quad\quad    branch: master

\quad\quad    commit: a9eb969bb63fc421e5cb2a699ef1f1e7c5cbd1b4

  alice@laptop\$ guix describe -f channels > \comment{alice-state.scm}
  \end{semiverbatim}

  \item<3-> Bob recreates the \emph{exact} same environment as Alice
  \begin{semiverbatim}
      bob@cluster\$ guix time-machine -C \comment{alice-state.scm} \\

\quad\quad\quad\quad\quad\quad\quad -- package -m \orange{requirements.scm}
  \end{semiverbatim}
  \end{itemize}

  \uncover<4->{
  \highlight{Conclusion}: All \red{executable} composing the
  \comment{environment} are captured by the state.
  }
\end{frame}


\begin{frame}{Generate containers (Docker, Singularity)}
  \begin{itemize}
  \item<1-> \texttt{guix time-machine} allows to rebuild the same
    \comment{environment} from \orange{source}
  \item<2-> Bob runs analysis on another infrastructure
  \begin{semiverbatim}
    bob@cluster\$ guix time-machine -C \comment{alice-state.scm} \

    \quad\quad\quad\quad\quad\quad\quad
    -- pack -f docker -m \orange{requirements.txt}

    bob@IFB\$ docker load < \red{image}.tar.gz

    bob@IFB\$ docker run ...
  \end{semiverbatim}
\end{itemize}

\uncover<3->{
\begin{center}
  \highlight{This Docker image disappears}
\end{center}
}
\begin{itemize}
\item<4-> Later, Dan rebuilds this same Docker image
    \begin{semiverbatim}
     dan@laptop\$ guix time-machine -C \comment{alice-state.scm} \

    \quad\quad\quad\quad\quad\quad\quad
    -- pack -f docker -m \orange{requirements.txt}
    \end{semiverbatim}
  \item<5-> Dan runs on another infrastructure using the exact same tools as Alice
    \begin{semiverbatim}
      dan@AWS\$ docker load < \red{image}.tar.gz

      dabn@AWS\$ docker run ...
    \end{semiverbatim}
  \end{itemize}
  \uncover<6->{
  $\Big($Lack real examples to ensure this scenario fully works ;-$\Big)$
      \begin{flushright}
      \href{https://hpc.guix.info/static/videos/atelier-reproductibilit\%C3\%A9-2021/arnaud-legrand.webm}{%
        A. Legrand@GuixHPC2021 (video)}%
      \href{https://hpc.guix.info/static/doc/atelier-reproductibilit\%C3\%A9-2021/arnaud-legrand-debian-docker.pdf}{%
        (pdf)}
    \end{flushright}
    }
\end{frame}


\begin{frame}{Long-term with Software Heritage}
  \begin{center}
    \highlight{What happens if the package on GitHub disappears?}
  \end{center}

  \begin{semiverbatim}
dan@workstation\$ guix install hi

\red{fatal: could not read Username for 'https://github.com': No such device or address}

Trying content-addressed mirror at berlin.guix.gnu.org...

Trying to download from Software Heritage...

\orange{SWH: found revision} e1eefd033b8a2c4c81babc6fde08ebb116c6abb8

at 'https://archive.softwareheritage.org/api/1/directory/c3e538ed2de412.../'
\end{semiverbatim}

The \orange{source} of \texttt{hi} were at
\href{https://github.com/zimoun/hello-example}%
{\sout{https://github.com/zimoun/hello-example}}.

$\Big($See Software Heritage ambassador \href{https://cupnet.net/about/}{Pierre Poulain} :-$\Big)$

\begin{flushright}
  $\Big($
  Package defined
  \href{https://github.com/zimoun/channel-example/blob/main/hello.scm}{\emph{here}}.
  $\Big)$
\end{flushright}

\end{frame}


\begin{frame}{Guix in a nutshell}

  \begin{center}
    \highlight{Tools helping in managing complexity}
  \end{center}
  \vfill{}

  \begin{enumerate}
  \item package manager: functional and transactional paradigm
  \item environment manager: switch between projects
  \item generate container: Docker, Singularity
  \item provide tools (Scheme library) to extend for user specificities
  \end{enumerate}

  \vfill{}
  \begin{center}
    \highlight{(fine) Control over the dependency chain}
  \end{center}

  \vfill{}

  \begin{tabular}{cll}
  \alert{$\star\star$} & Binary reproductibility &
    \scriptsize{(at least to track it)}\normalsize \\
    \alert{$\star\star$} & \emph{Bootstrap} &
    \scriptsize{(new yogurt $\leftarrow$ milk + old yogurt)}\normalsize
  \end{tabular}
\end{frame}


\begin{frame}[fragile]
  \begin{center}
    Similar to other package manager
  \end{center}

  \begin{semiverbatim}
    \small{
      \comment{\# Alice}
      guix \alert{install} foo@1.2 bar@3.4 baz@5.6       \comment{\# -m requirements.scm}
      guix \alert{describe} -f channels > \red{alice-conf.scm}

      \comment{\# Carole}
      guix install \alert{--profile}=./with-alice foo@1.2 bar@3.4 baz@5.6
      guix install foo@7.8 bar@9.0

      \comment{\# Charlie}
      guix package \alert{--roll-back}                   \comment{\# generations}

      \comment{\# Bob}
      guix \alert{pull --channels}=\red{alice-conf.scm}
      guix \alert{pack} -f docker ...                    \comment{# -f squashfs}

      \comment{\# Dan}
      guix \alert{time-machine} -C \red{alice-conf.scm} -- install foo@1.2 bar@3.4 baz@5.6
    }
  \end{semiverbatim}
\end{frame}


\setbeamercolor{normal text}{fg=black,bg=white}
\begin{frame}[plain]{The \emph{ideal} target}
  \vspace{-0.9cm}
  \includegraphics[width=\textwidth]{images/big-picture-3}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}


\begin{frame}{Clusters using Guix}
  \large{
    \begin{tabular}{lrrl}
      \highlight{GriCAD}&\highlight{Grenoble}:& 72-node  &(1000+ cores)\\
      \highlight{CCIPL}&\highlight{Nantes}:& 230-node  &(4000+ cores)\\
      \highlight{PlaFRIM Inria}&\highlight{Bordeaux}:& 120-node   &(3000+ cores)\\
      \highlight{Max Delbrück Center}&\highlight{Berlin}:& 250-node &+ workstations \\
      \highlight{UMC Utrecht}&:& 68-node  &(1,000+ cores)\\
      \quad (Univ. Paris ?)&:& ?9-node? &+ 2 workstations
    \end{tabular}
  }
  \begin{center}
    \includegraphics[height=0.3\paperheight]{images/guixhpc-logo-transparent}

    \url{https://hpc.guix.info}
  \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[plain]
  \vfill{
    \begin{minipage}[c]{1.0\linewidth}
      \highlight{\texttt{simon.tournier@u-paris.fr}}
      \hfill
      \highlight{\texttt{nicolas.vallet@inserm.fr}}
    \end{minipage}
    \begin{center}
      \url{https://hpc.guix.info}
      \\
      \includegraphics[width=0.3\textwidth]{images/Guix-white}
    \end{center}
    \texttt{help-guix@gnu.org}\hfill{\alert{\url{https://guix.gnu.org}}}
  }
\end{frame}

\begin{frame}{}

  \begin{textblock}{12}(2, 8)
    \tiny{
      Copyright \copyright{} 2021 Simon Tournier
      \texttt{simon.tournier@u-paris.fr}.\\
      Copyright \copyright{} 2021 Nicolas Vallet \texttt{nicolas.vallet@inserm.fr}.\\[3.0mm]
      GNU Guix logo, CC-BY-SA 4.0, \url{https://gnu.org/s/guix/graphics}

      GNU~Guix Reference Card under GFDL~1.3+.

      Copyright of other images included in this document is held by
      their respective owners; especially from Ludovic Courtès.
      \\[3.0mm]
      This work is licensed under the \alert{Creative Commons
        Attribution-Share Alike 3.0} License.  To view a copy of this
      license, visit
      \url{http://creativecommons.org/licenses/by-sa/3.0/} or send a
      letter to Creative Commons, 171 Second Street, Suite 300, San
      Francisco, California, 94105, USA.
      \\[2.0mm]
      At your option, you may instead copy, distribute and/or modify
      this document under the terms of the \alert{GNU Free Documentation
        License, Version 1.3 or any later version} published by the Free
      Software Foundation; with no Invariant Sections, no Front-Cover
      Texts, and no Back-Cover Texts.  A copy of the license is
      available at \url{https://www.gnu.org/licenses/gfdl.html}.
      \\[2.0mm]
      % Give a link to the 'Transparent Copy', as per Section 3 of the GFDL.
      The source of this document is available from
      \url{https://git.sv.gnu.org/cgit/guix/maintenance.git}.
      \url{TODO}.
    }
  \end{textblock}
\end{frame}


\begin{frame}{Container is a smoothie}

  \begin{itemize}
  \item Container = a binary box that stores (captures) all the executables
  \item How to generate one?\\
    \hfill For instance, \texttt{Dockerfile} based on some Linux distribution
  \item What happens if this binary container is lost?  Can I rebuild it?\\
    \hfill The answer is no, you cannot!

    Well, require a lot of hard and low-level work.
    \begin{flushright}
      \href{https://hpc.guix.info/static/videos/atelier-reproductibilit\%C3\%A9-2021/arnaud-legrand.webm}{%
        A. Legrand@GuixHPC2021 (video)}%
      \href{https://hpc.guix.info/static/doc/atelier-reproductibilit\%C3\%A9-2021/arnaud-legrand-debian-docker.pdf}{%
        (pdf)}
    \end{flushright}
  \end{itemize}

  \bigskip

  \highlight{Conclusion 1}: Container is \highlight{useful} to move binaries from one place to another

  \medskip

  \highlight{Conclusion 2}: Container is \highlight{not the solution} for the Reproducibility Crisis\\
  \hfill (= run the same analysis on different machines at different moments)

\end{frame}

\end{document}

% Local Variables:
% coding: utf-8
% comment-start: "%"
% comment-end: ""
% ispell-local-dictionary: "french"
% End:
