(list (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (branch "master")
        (commit
          "791069737c8c51582cc021438dae32eb0fb7b8e0")))
