% Make sure URLs are broken on hyphens.
% See <https://tex.stackexchange.com/questions/3033/forcing-linebreaks-in-url>.
\RequirePackage[hyphens]{url}

\documentclass[aspectratio=169]{beamer}

\usetheme{default}

\usefonttheme{structurebold}

% Nice sans-serif font.
\usepackage[sfdefault,lining]{FiraSans} %% option 'sfdefault' activates Fira Sans as the default text font
\renewcommand*\oldstylenums[1]{{\firaoldstyle #1}}

% Nice monospace font.
\usepackage{inconsolata}
%% \renewcommand*\familydefault{\ttdefault} %% Only if the base font of the document is to be typewriter style
%% \usepackage[T1]{fontenc}

\usepackage[utf8]{inputenc}
\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref,xspace,multicol}

\usecolortheme{seagull}         % white on black
\usepackage[absolute,overlay]{textpos}
\usepackage{tikz,verbatimbox}
\usetikzlibrary{arrows,shapes,trees,shadows,positioning}
\usepackage{fancyvrb}           % for '\Verb'
\usepackage{xifthen}            % for '\isempty'

% Remember the position of every picture.
\tikzstyle{every picture}+=[remember picture]

\tikzset{onslide/.code args={<#1>#2}{%
  \only<#1>{\pgfkeysalso{#2}} % \pgfkeysalso doesn't change the path
}}

% Colors.
\definecolor{guixred1}{RGB}{226,0,38}  % red P
\definecolor{guixorange1}{RGB}{243,154,38}  % guixorange P
\definecolor{guixyellow}{RGB}{254,205,27}  % guixyellow P
\definecolor{guixred2}{RGB}{230,68,57}  % red S
\definecolor{guixred3}{RGB}{115,34,27}  % dark red
\definecolor{guixorange2}{RGB}{236,117,40}  % guixorange S
\definecolor{guixtaupe}{RGB}{134,113,127} % guixtaupe S
\definecolor{guixgrey}{RGB}{91,94,111} % guixgrey S
\definecolor{guixdarkgrey}{RGB}{46,47,55} % guixdarkgrey S
\definecolor{guixblue1}{RGB}{38,109,131} % guixblue S
\definecolor{guixblue2}{RGB}{10,50,80} % guixblue S
\definecolor{guixgreen1}{RGB}{133,146,66} % guixgreen S
\definecolor{guixgreen2}{RGB}{157,193,7} % guixgreen S

\definecolor{rescienceyellow}{RGB}{254,246,91}

\setbeamerfont{title}{size=\huge}
\setbeamerfont{frametitle}{size=\huge}
\setbeamerfont{normal text}{size=\Large}

% White-on-black color theme.
\setbeamercolor{structure}{fg=guixorange1,bg=black}
\setbeamercolor{title}{fg=white,bg=black}
\setbeamercolor{date}{fg=guixorange1,bg=black}
\setbeamercolor{frametitle}{fg=white,bg=black}
\setbeamercolor{titlelike}{fg=white,bg=black}
\setbeamercolor{normal text}{fg=white,bg=black}
\setbeamercolor{alerted text}{fg=guixyellow,bg=black}
\setbeamercolor{section in toc}{fg=white,bg=black}
\setbeamercolor{section in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsubsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsubsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{frametitle in toc}{fg=white,bg=black}
\setbeamercolor{local structure}{fg=guixorange1,bg=black}

\newcommand{\highlight}[1]{\alert{\textbf{#1}}}

\title{Reproducible Software Deployment with GNU Guix}

\author{Ludovic Courtès}
\date{10 January 2025}

\setbeamertemplate{navigation symbols}{} % remove the navigation bar

\AtBeginSection[]{
  \begin{frame}
    \frametitle{}
    \tableofcontents[currentsection]
  \end{frame}
}


\newcommand{\screenshot}[2][width=\paperwidth]{
  \begin{frame}[plain]
    \begin{tikzpicture}[remember picture, overlay]
      \node [at=(current page.center), inner sep=0pt]
        {\includegraphics[{#1}]{#2}};
    \end{tikzpicture}
  \end{frame}
}

\newcommand{\screenshotwithurl}[3][width=\paperwidth]{
  \begin{frame}[plain]
    \begin{tikzpicture}[overlay]
      \node [at=(current page.center), inner sep=0pt]
        {\includegraphics[{#1}]{#2}};
      \node [at=(current page.south), anchor=south, rounded corners,
        inner sep=3mm, fill=guixdarkgrey, text opacity=1, opacity=.6,
        text=white, outer sep=1mm] {
        \url{#3}
      };
    \end{tikzpicture}
  \end{frame}
}

\begin{document}

% https://fosdem.org/2025/schedule/event/fosdem-2025-5720-the-shepherd-minimalism-in-pid-1/
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[overlay]
    % https://commons.wikimedia.org/wiki/File:Rumunia_5806.jpg
    \node [at=(current page.center), fill=black, opacity=.9] {
      \includegraphics[width=1.2\textwidth]{images/romanian-shepherd}
    };
  \end{tikzpicture}

  \vspace{35mm}
  \Huge{\textbf{The Shepherd: \\ Minimalism in PID 1}}
  \\[6mm]
  \large{Ludovic Courtès}
  \\[2mm]
  \oldstylenums{FOSDEM, 2 February 2025}
\end{frame}

\begin{frame}[plain, fragile]
  \begin{tikzpicture}[overlay]
    % https://commons.wikimedia.org/wiki/File:Rumunia_5806.jpg
    \node [at=(current page.center), fill=black, opacity=.3] {
      \includegraphics[width=1.2\textwidth]{images/romanian-shepherd}
    };
  \end{tikzpicture}

  \begin{semiverbatim}
\textbf{From}: Wolfgang Jaehrling
\textbf{Subject}: Announcement: dmd -0.7
\textbf{Date}: Wed, 2 Apr 2003 21:44:52 +0200
;; Announcement: dmd -0.7                           -*- scheme -*-

(define about-this-release
  "This is the second public release of dmd, the `Daemon managing
Daemons' (or `Daemons-managing Daemon'?).  This version adds many
convenient features and is thus a huge step forward, but still it is
experimental software and you should expect it to break.")

(define about-the-software
  "The dmd program is a service manager, i.e. on the GNU system (which
is the primary target), it replaces /sbin/init completely, on systems
which are similar to Unix (e.g. GNU/Linux) it replaces the part of
/sbin/init that is responsible for switching runlevels (/etc/rc?.d and
/etc/init.d come to mind), respawning services (/etc/inittab comes to
mind) and similar things.")
  \end{semiverbatim}

  \begin{tikzpicture}[overlay,
      box/.style = { inner sep=5mm, rotate=-10, rounded corners=2mm,
           fill=guixorange1, text=white, opacity=1,
           drop shadow={opacity=0.4} } ]
    \node<2-> (revived) at (current page) [box] {
      \LARGE{\textbf{Revived in 2013!}}
    };
    \node<3-> [below=of revived] [box, rotate=10] {
      \Huge{\textbf{1.0 in December 2024!}}
    };
  \end{tikzpicture}
\end{frame}


\setbeamercolor{normal text}{bg=guixblue1}
\begin{frame}
  \begin{overlayarea}{\textwidth}{8cm}
  \begin{tikzpicture}[kernel/.style = {
                        text width=10cm, minimum height=1.4cm,
                        text centered,
                        rounded corners=2mm,
                        fill=white, text=black
                      },
                      userland/.style = {
                        draw=guixorange1, very thick,
                        fill=white, text=black, text width=6cm,
                        rounded corners=2mm, minimum height=1.4cm,
                        text centered
                      }]
    \matrix[row sep=6mm, column sep=1cm] {
      \node(kernel)[kernel]{\textbf{\Large{Linux-libre}}};
      \\

      \node<2->(initrd)[userland]{\textbf{\Large{initial RAM disk}}};
      \\

      \node<4->(shepherd)[userland]{\textbf{\Large{PID 1: The Shepherd!}}
        \\ services...};
      \\

      \node<6->(user)[userland, dashed]{\textbf{\Large{applications}}};
      \\
    };

    \path[->, very thick, draw=white]<2->
      (kernel) edge (initrd);
    \path[->, very thick, draw=white]<4->
      (initrd) edge (shepherd);
    \path[->, very thick, draw=white]<6->
      (shepherd) edge (user);

  \end{tikzpicture}
  \end{overlayarea}

  \begin{tikzpicture}[overlay,
                      guile/.style = {
                         fill=guixyellow, text=black, rotate=30,
                         rounded corners=4mm, text width=3cm,
                         opacity=.75, text opacity=1, text centered,
                         minimum height=1.3cm
                      }]
    \node<3->(labelinitrd) [guile] at (initrd.east) {%
      \Large{Guile}
    };
    \node<5->(labelinitrd) [guile] at (shepherd.east) {%
      \Large{Guile}
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=guixdarkgrey}
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[overlay]
    \node[shape=star, star points=9, fill=guixorange2,
      inner sep=5mm, at=(current page.center),
      font=\Huge] {
      \textbf{Demo time!}
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}[fragile, plain]
  \large{
  \begin{semiverbatim}
(\alert{define} sshd
  (\alert{service}
    '(sshd ssh-daemon)  ;for convenience, give it two names
    \alert{#:start} (make-forkexec-constructor
             '("/usr/sbin/sshd" "-D")
             \alert{#:pid-file} "/etc/ssh/sshd.pid")
    \alert{#:stop} (make-kill-destructor)
    \alert{#:respawn?} #t))

(register-services (list sshd))
(start-in-the-background '(sshd))
  \end{semiverbatim}
  }
\end{frame}

\begin{frame}[fragile, plain]
  \large{
    \begin{semiverbatim}
(\alert{define} sshd
  (\alert{service}
    '(sshd ssh-daemon)
    \alert{#:start} (make-inetd-constructor
             '("/usr/sbin/sshd" "-D" "-i")
             (list (\alert{endpoint}
                    (make-socket-address AF_INET INADDR_ANY 22))
                   (\alert{endpoint}
                    (make-socket-address AF_INET6 IN6ADDR_ANY 22)))
             \alert{#:max-connections} 10)
    \alert{#:stop} (make-inetd-destructor)
    \alert{#:respawn?} #t))
    \end{semiverbatim}
  }
\end{frame}

\begin{frame}[fragile, plain]
  \begin{semiverbatim}
(\alert{use-modules} (shepherd service timer))

(\alert{define} updatedb                 ;for 'locate'
  (\alert{service}
    '(updatedb)
    \alert{#:start} (make-timer-constructor
              ;; Fire at midnight and noon everyday.
              (calendar-event \alert{#:hours} '(0 12) \alert{#:minutes} '(0))
              (command '("/usr/bin/updatedb"
                         "--prunepaths=/tmp")))
    \alert{#:stop} (make-timer-destructor)
    \alert{#:actions} (list timer-trigger-action)))
  \end{semiverbatim}
\end{frame}

% config sample: socket activation

\begin{frame}[fragile, plain]
  \large{
  \begin{semiverbatim}
(\alert{define} file-system-/mnt/disk
  (\alert{service}
    '(file-system-/mnt/disk)
    \alert{#:start} (\alert{lambda} _
              (mount "/dev/sdb2" "/mnt/disk" "ext4"))
    \alert{#:stop} (\alert{lambda} _
             (umount "/mnt/disk"))))
  \end{semiverbatim}
  }
\end{frame}

\setbeamercolor{normal text}{bg=guixblue1}
\begin{frame}[fragile, plain]
  \Huge{\textbf{Blurring the line \\[2mm]
      between users and developers \\[2mm]
      to increase \alert{user autonomy}.
  }}
\end{frame}

%% \setbeamercolor{normal text}{bg=white}
%% \begin{frame}[plain, fragile]
%%   \begin{tikzpicture}[shorten >=1pt, node distance=2cm,
%%       on grid, auto, text=black, draw=black,
%%       state/.style = { rounded corners=2mm, inner sep=2mm,
%%         draw=guixorange1, font=\Large },
%%       start/.style = { draw=none },
%%       accepting/.style = { draw=guixorange2, text=black }
%%     ]
%%     \draw[help lines] (0,0) grid (6,3);

%%     \node[state] (fork) {spawn};
%%     \node[state] (accept) [at=(current page.center)] {accept};
%%     \node (readcommand) [right=of accept] {read command};

%%     \node[state, below=of fork] (stopped) {stopped};
%%     \node[state, below right=of stopped] (starting) {starting};
%%     \node[state, below left=of starting] (started) {started};
%%     \node[state, below left=of stopped] (stopping) {stopping};

%%     \path[->] (fork) edge [loop below] node {\texttt{SIGCHLD}} ()
%%               (accept) edge [loop below] node {} ();

%%     \path[->] (stopped) edge (starting);
%%     \path[->] (started) edge (stopping);
%%     \path[->] (stopped) edge (stopped);
%%   \end{tikzpicture}
%% \end{frame}

\setbeamercolor{normal text}{bg=white}
\screenshot[height=0.8\textheight]{images/fork+exec-SIGCHLD}
\screenshot[height=0.8\textheight]{images/accept-read-commands}
\screenshot[height=0.8\textheight]{images/fork+exec-pid-file}
\screenshot[height=0.8\textheight]{images/service-states}
% TODO terminate with grace period
\begin{frame}[plain, fragile]
  \begin{tikzpicture}
    \matrix [row sep=6mm, column sep=30mm] {
      \node{\includegraphics[height=0.4\textheight]{images/fork+exec-SIGCHLD}};
      &
      \node{\includegraphics[height=0.4\textheight]{images/accept-read-commands}};
      \\

      \node{\includegraphics[height=0.4\textheight]{images/service-states}};
      &
      \node{\includegraphics[height=0.45\textheight]{images/fork+exec-pid-file}};
      \\
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=guixdarkgrey}
\begin{frame}[plain]
  \Large{
  \begin{semiverbatim}
After a \alert{fork()} in a multithreaded program, the child can safely
call only async-signal-safe functions (see \alert{signal-safety(7)})
until such time as it calls \alert{execve(2)}.
  \end{semiverbatim}
  }
\end{frame}

\setbeamercolor{normal text}{bg=black}
\screenshot{images/systemd-auto-restart}

\setbeamercolor{normal text}{bg=guixdarkgrey}

\begin{frame}[plain, fragile]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.north east), anchor=north east, inner sep=3mm] {
      \includegraphics[height=0.8\textheight]{images/fork+exec-SIGCHLD}
    };
  \end{tikzpicture}

  \large{
    \begin{semiverbatim}
(\alert{let} loop ()
  (handle-signal-port signal-port)
  (loop))
\pause
(\alert{define} (handle-signal-port port)
  (let ((signal (consume-signalfd-siginfo \tikz[baseline]{\node[anchor=base](port){port};})))
    (if (= SIGCHLD signal)
        (match (\alert{waitpid} WAIT_ANY WNOHANG)
          ((pid . status)
           (put-message (current-process-monitor)
                        `(handle-process-termination ,pid ,status))))
        \textrm\dots{})))
    \end{semiverbatim}
    }

  \begin{tikzpicture}[overlay]
    \node<2->[above right=of port, fill=white, text=black, rounded
      corners=2mm, inner sep=3mm, anchor=south] (box) {
      \textbf{reading won't block!}
    };
    \path<2->[->, very thick, draw=white] (box) edge (port);
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain, fragile]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.north east), anchor=north east, inner sep=3mm] {
      \includegraphics[height=0.8\textheight]{images/accept-read-commands}
    };
  \end{tikzpicture}

  \large{
  \begin{semiverbatim}
(\alert{let} next-client ()
  (match (\alert{accept} sock (logior SOCK_NONBLOCK SOCK_CLOEXEC))
    ((command-source . client-address)
     (\alert{spawn-fiber}
      (lambda ()
        ;; Read and execute client commands concurrently.
        (process-connection command-source)))))
  (next-client))
  \end{semiverbatim}
  }
\end{frame}

\begin{frame}[plain, fragile]
  \large{
    \begin{semiverbatim}
(\alert{define*} (terminate-process pid signal
                            #:key (grace-period 5))
  ;; Send signal to PID, or SIGKILL after grace period.
  (let ((reply (make-channel)))
    (\alert{put-message} (current-process-monitor)
                 `(\alert{await} ,pid ,reply))
    (catch-system-error (\alert{kill} pid signal))

    (match (\alert{get-message*} reply grace-period #f)
      (#f
       ;; Grace period is over, send SIGKILL.
       (catch-system-error (\alert{kill} pid SIGKILL))
       (\alert{get-message} reply))
      (status
       ;; Terminated on time!
       status))))
    \end{semiverbatim}
  }
\end{frame}

\begin{frame}[plain, fragile]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.north east), anchor=north east, inner sep=3mm] {
      \includegraphics[height=0.8\textheight]{images/service-states}
    };
  \end{tikzpicture}

  \large{
    \begin{semiverbatim}
(\alert{define} (service-controller channel)
  (let loop ((status 'stopped)
             (value #f)
             ...)
    (match (\alert{get-message} channel)
      (('status reply)     ;return the current status
       (put-message reply status)
       (loop status value ...))
      (('start reply)      ;start the service
       ...)
      (('stop reply)       ;stop it
       ...)
      ...)))
    \end{semiverbatim}
  }
\end{frame}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain, fragile]
  \begin{tikzpicture}
    \matrix [row sep=6mm, column sep=25mm] {
      \node{\includegraphics[height=0.4\textheight]{images/service-states}};
      &
      \node{\includegraphics[height=0.4\textheight]{images/service-states}};
      &
      \node{\includegraphics[height=0.4\textheight]{images/service-states}};
      &
      \\
      \node{\includegraphics[height=0.4\textheight]{images/service-states}};
      &
      \node{\includegraphics[height=0.4\textheight]{images/service-states}};
      &
      \node{\includegraphics[height=0.4\textheight]{images/service-states}};
      &
      \\
    };
    \node<2-> [at=(current page.center), text=black] {
      \Huge{\textbf{Concurrent sequential processes.}}
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=guixred1}
\begin{frame}[plain]
  \Huge{\textbf{Sweat \& tears.}}
\end{frame}

\setbeamercolor{normal text}{bg=guixred3}
\begin{frame}[plain, fragile]
  \begin{myverbbox}{\sONE}
(define (alice channel)               
  (let loop ()                        
    (match (get-message channel)      
      (('say-hi-to-me reply)          
       (put-message reply 'hi!)))))
  \end{myverbbox}

  \begin{myverbbox}{\sTWO}
(define (bob friend)                  
  (let ((reply (make-channel))) 
    (put-message friend         
      `(say-hi-to-me ,reply))   
    'bye-bye!))
  \end{myverbbox}

  \begin{tikzpicture}[overlay]
    \node (alice) [at=(current page.center), anchor=east] {\sONE};
    \node (bob) [right=of alice] {\sTWO};

    \node<2->[at=(current page.center), fill=guixorange2, text=white,
      rounded corners=2mm, inner sep=5mm, font=\LARGE] {
      \textbf{Do not miss a rendez-vous.}
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}[fragile]
  \begin{quotation}
    \Huge{
    ``Hello, I wrote my own service \\ and now \texttt{herd status}
    hangs.''
    }
  \end{quotation}
\end{frame}

\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}[fragile]
  \Huge{\texttt{thread apply all bt}}
  \\[10mm]
  ... would be nice.
\end{frame}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[
      screenshot/.style = { drop shadow={opacity=0.4},
        text=black }
    ]

    \node[screenshot,
      rotate=5]{\includegraphics[height=0.6\textheight]{images/fibers-leak1}};
    \node<2->[screenshot,
      rotate=-10]{\includegraphics[height=0.5\textheight]{images/fibers-leak2}};

    \node<3->[text=guixred3,
      inner sep=0mm, at=(current page.center), font=\Huge] {
      \textbf{Uh-oh!}
    };

  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=guixgreen1}
\begin{frame}[plain, fragile]
  \center{\Huge{\texttt{herd status}}}
\end{frame}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[overlay]
    \node (logo) [at=(current page.center), anchor=south] {\includegraphics[width=0.4\textwidth]{images/linux-magazine-logo}};
    \node [at=(current page.center), anchor=north] {\includegraphics[width=1.1\textwidth]{images/linux-magazine-headline}};
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=guixtaupe}
\begin{frame}[fragile]
  \LARGE{
    \begin{itemize}
    \item \highlight{1.0.x} bug-fix releases rolling
    \item \highlight{Guix integration}: replacing rottlog, mcron,
      syslogd
    \item \highlight{Fibers} is solid! (and needs $\heartsuit$)
    \item \highlight{7.4K lines of code!}
    \item miss something from systemd? \highlight{let's hack!}
    \end{itemize}
  }
\end{frame}

\screenshot{images/spritely-shepherd}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setbeamercolor{normal text}{bg=black}
\begin{frame}[plain]

\vfill{
  \vspace{3cm}
  \center{\includegraphics[width=0.5\textwidth]{images/shepherd-logo}}\\[1.0cm]
  \center{\alert{\url{https://gnu.org/s/shepherd}}} \\[0.2cm]
  \texttt{ludo@gnu.org |} @civodul@toot.aquilenet.fr
  \\[1.2cm]
}
\end{frame}

\setbeamercolor{normal text}{bg=black}
\begin{frame}{}
  \begin{textblock}{12}(2, 5)
    \tiny{
      Copyright \copyright{} 2025 Ludovic Courtès \texttt{ludo@gnu.org}.
      \\[3.0mm]
      Picture of Shepherd in Făgăraș Mountains, Romania, by ``friend of
      Darwinek'', under CC-BY-SA 3.0,
      \url{https://commons.wikimedia.org/wiki/File:Rumunia_5806.jpg}
      \\[2.0mm]
      Goblins + Spritely drawing by Spritely Institute,
      \url{https://spritely.institute/news/spritely-nlnet-grants-december-2023.html}
      \\[2.0mm]
      Shepherd logo by Luis Felipe López Acevedo, under CC-BY-SA 4.0,
      \url{https://git.savannah.gnu.org/cgit/shepherd/graphics.git}.
      \\[3.0mm]
      This work is licensed under the \alert{Creative Commons
        Attribution-Share Alike 3.0} License.  To view a copy of this
      license, visit
      \url{https://creativecommons.org/licenses/by-sa/3.0/} or send a
      letter to Creative Commons, 171 Second Street, Suite 300, San
      Francisco, California, 94105, USA.
      \\[2.0mm]
      At your option, you may instead copy, distribute and/or modify
      this document under the terms of the \alert{GNU Free Documentation
        License, Version 1.3 or any later version} published by the Free
      Software Foundation; with no Invariant Sections, no Front-Cover
      Texts, and no Back-Cover Texts.  A copy of the license is
      available at \url{https://www.gnu.org/licenses/gfdl.html}.
      \\[2.0mm]
      % Give a link to the 'Transparent Copy', as per Section 3 of the GFDL.
      The source of this document is available from
      \url{https://git.sv.gnu.org/cgit/guix/maintenance.git}.
    }
  \end{textblock}
\end{frame}

\end{document}

% Local Variables:
% coding: utf-8
% comment-start: "%"
% comment-end: ""
% ispell-local-dictionary: "american"
% compile-command: "guix shell -m ../../beamer-manifest.scm -- rubber --pdf talk.tex"
% End:
