% The comment below tells Rubber to compile the .dot files.
%
% rubber: module graphics
% rubber: rules rules.ini

% Make sure URLs are broken on hyphens.
% See <https://tex.stackexchange.com/questions/3033/forcing-linebreaks-in-url>.
\RequirePackage[hyphens]{url}

\documentclass[aspectratio=169]{beamer}

\usetheme{default}

\usefonttheme{structurebold}
\usepackage{helvet}
\usecolortheme{seagull}         % white on black

\usepackage[utf8]{inputenc}
\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref,xspace,multicol}

\usepackage[absolute,overlay]{textpos}
\usepackage{tikz}
\usetikzlibrary{arrows,shapes,trees,shadows,positioning}
\usepackage{fancyvrb}           % for '\Verb'
\usepackage{xifthen}            % for '\isempty'

% Remember the position of every picture.
\tikzstyle{every picture}+=[remember picture]

\tikzset{onslide/.code args={<#1>#2}{%
  \only<#1>{\pgfkeysalso{#2}} % \pgfkeysalso doesn't change the path
}}

% Colors.
\definecolor{guixred1}{RGB}{226,0,38}  % red P
\definecolor{guixorange1}{RGB}{243,154,38}  % guixorange P
\definecolor{guixyellow}{RGB}{254,205,27}  % guixyellow P
\definecolor{guixred2}{RGB}{230,68,57}  % red S
\definecolor{guixred3}{RGB}{115,34,27}  % dark red
\definecolor{guixorange2}{RGB}{236,117,40}  % guixorange S
\definecolor{guixtaupe}{RGB}{134,113,127} % guixtaupe S
\definecolor{guixgrey}{RGB}{91,94,111} % guixgrey S
\definecolor{guixdarkgrey}{RGB}{46,47,55} % guixdarkgrey S
\definecolor{guixblue1}{RGB}{38,109,131} % guixblue S
\definecolor{guixblue2}{RGB}{10,50,80} % guixblue S
\definecolor{guixgreen1}{RGB}{133,146,66} % guixgreen S
\definecolor{guixgreen2}{RGB}{157,193,7} % guixgreen S

\setbeamerfont{title}{size=\huge}
\setbeamerfont{frametitle}{size=\huge}
\setbeamerfont{normal text}{size=\Large}

% White-on-black color theme.
\setbeamercolor{structure}{fg=guixorange1,bg=black}
\setbeamercolor{title}{fg=white,bg=black}
\setbeamercolor{date}{fg=guixorange1,bg=black}
\setbeamercolor{frametitle}{fg=white,bg=black}
\setbeamercolor{titlelike}{fg=white,bg=black}
\setbeamercolor{normal text}{fg=white,bg=black}
\setbeamercolor{alerted text}{fg=guixyellow,bg=black}
\setbeamercolor{section in toc}{fg=white,bg=black}
\setbeamercolor{section in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsubsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsubsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{frametitle in toc}{fg=white,bg=black}
\setbeamercolor{local structure}{fg=guixorange1,bg=black}

\newcommand{\highlight}[1]{\alert{\textbf{#1}}}

\title{Towards reproducible Jupyter notebooks}

\author{Ludovic Courtès\\Pierre-Antoine Rouby}
\date{\small{JCAD, Toulouse\\ 9 octobre 2019}}

\setbeamertemplate{navigation symbols}{} % remove the navigation bar

\AtBeginSection[]{
  \begin{frame}
    \frametitle{}
    \tableofcontents[currentsection]
  \end{frame} 
}


\newcommand{\screenshot}[2][width=\paperwidth]{
  \begin{frame}[plain]
    \begin{tikzpicture}[remember picture, overlay]
      \node [at=(current page.center), inner sep=0pt]
        {\includegraphics[{#1}]{#2}};
    \end{tikzpicture}
  \end{frame}
}


\begin{document}

\begin{frame}[plain, fragile]
  \vspace{10mm}
  \titlepage

  \vfill{}                      % TODO: logo 2019
  \hfill{\includegraphics[width=0.2\paperwidth]{images/inria-logo-inverse-en-2017}}
\end{frame}

\setbeamercolor{normal text}{bg=guixred3}
\begin{frame}[plain]
  \Huge{Jupyter = reproducible science\uncover<2->{?}}
\end{frame}

\setbeamercolor{normal text}{bg=white}
\screenshot{images/jupyter-matplotlib}
\screenshot{images/pip-install-scared}
\screenshot{images/conda-broke-tweet}
\screenshot{images/mybinder-web}
\screenshot{images/binder-config-files}


\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}[plain]
  \Huge{What if notebooks were self-contained, ``deployment-aware''?}
\end{frame}

%% \setbeamercolor{normal text}{bg=white}
%% \begin{frame}[plain]
%%   \center{\includegraphics[width=0.6\textwidth]{images/Guix-horizontal-print}}\\[1.0cm]
%% \end{frame}

\setbeamercolor{normal text}{bg=guixtaupe}
\begin{frame}[fragile]%{``Virtual environments''}
  \LARGE{
    \begin{semiverbatim}
\$ guix \alert{environment} --ad-hoc \\
      python python-numpy python-scipy \\
      -- python3
    \end{semiverbatim}
  }
\end{frame}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain, fragile]
  \center{\includegraphics[width=0.35\textwidth]{images/guix-jupyter}}

  \begin{tikzpicture}[overlay]
    \node<2->[rounded corners=4, text centered,
          fill=guixorange1, text width=3cm,
          inner sep=2mm, rotate=5, opacity=.75, text opacity=1,
          drop shadow={opacity=0.5}] at (5, 4) {
            \textbf{\large{first release today!  :-)}}
          };
    \node<2->[text=guixdarkgrey, anchor=south, at=(current page.south)]
      {\url{https://hpc.guix.info/blog/2019/10/towards-reproducible-jupyter-notebooks}};
  \end{tikzpicture}

\end{frame}

\screenshot{images/jupyter-select-kernel}
\screenshot{images/environment}

\begin{frame}[fragile]
  \begin{tikzpicture}[remember picture, overlay]
    % https://commons.wikimedia.org/wiki/File:TeamTimeCar.com-BTTF_DeLorean_Time_Machine-OtoGodfrey.com-JMortonPhoto.com-07.jpg
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=\paperwidth]{images/delorean}};
    \node [rounded corners=4, text centered, anchor=north,
           text width=10cm,
          inner sep=3mm, opacity=.75, text opacity=1]
      at (current page.center) {
            \textbf{\Huge{travel in space \emph{and} time!}}
          };
  \end{tikzpicture}
\end{frame}

\screenshot{images/pin}

\begin{frame}[fragile]
  \begin{tikzpicture}[client/.style = {
                        text width=35mm, minimum height=2cm,
                        text centered,
                        rounded corners=2mm,
                        fill=guixorange1, text=white
                      },
                      kernel/.style = {
                        rectangle, text width=35mm, text centered,
                        rounded corners=2mm, minimum height=15mm,
                        top color=guixorange1,
                        bottom color=guixyellow,
                        text=black
                      }]
    \matrix[row sep=3mm, column sep=1cm] {
      & \node(jupyter)[client]{\large{\textbf{Jupyter}}}; & \\

      & \node(guixjupyter)[client]{\large{Guix-Jupyter}}; & \\[1.2cm]

      \node(kernel1)[kernel]{\large{IPykernel}}; &
      \node(kernel2)[kernel]{\large{IRkernel}}; &
      \node(kernel3)[kernel]{\large{IJulia}};
      \\
    };
  \end{tikzpicture}

  \begin{tikzpicture}[overlay]
    \path[very thick, draw=guixorange1]
      (jupyter.south) edge [->] (guixjupyter.north);
    \path[very thick, draw=guixorange1]
      (guixjupyter.south) edge [out=30, in=90, ->] (kernel1.north);
    \path[very thick, draw=guixorange1]
      (guixjupyter.south) edge [out=-100, in=90, ->] (kernel2.north);
    \path[very thick, draw=guixorange1]
      (guixjupyter.south) edge [out=-30, in=120, ->] (kernel3.north);
  \end{tikzpicture}
\end{frame}


\screenshot{images/container}
\screenshot{images/download}
\screenshot{images/eelco-pointer-discipline}

\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}[plain]
  \Huge{\textbf{Wrap-up.}}
\end{frame}


\setbeamercolor{normal text}{bg=guixdarkgrey}
\begin{frame}
  \Huge{\textbf{Open issues}}\\[2cm]

  \Large{
  \begin{itemize}
  \item how can we improve the \textbf{user interface}?
  \item should deployment be \textbf{built into Jupyter}?
  \item what about \textbf{interoperability}?
  \item ...
  \end{itemize}
  }
\end{frame}

\begin{frame}[plain]
  \center{\Huge{Guix-Jupyter =}}\\[1cm]

  \Large{
  \begin{itemize}
  \item \textbf{self-contained} notebooks
  \item automatic \& \textbf{reproducible deployment}
  \item code runs in \textbf{isolated environment}
  \end{itemize}
  }
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setbeamercolor{normal text}{bg=black}
\begin{frame}[plain]

\vfill{
  \vspace{3cm}
  \center{\includegraphics[width=0.5\textwidth]{images/guixhpc-logo-transparent}}\\[1.0cm]
  \center{\alert{\url{https://hpc.guix.info}}} \\[0.2cm]
  \texttt{ludovic.courtes@inria.fr |} @GuixHPC
  \\[1.2cm]
}
\end{frame}


\begin{frame}{}
  \begin{textblock}{12}(2, 6)
    \tiny{
      Copyright \copyright{} 2010, 2012--2019 Ludovic Courtès \texttt{ludo@gnu.org}.\\[3.0mm]
      GNU Guix logo, CC-BY-SA 4.0, \url{https://gnu.org/s/guix/graphics}.
      \\[1.5mm]
      DeLorean time machine picture \copyright{} 2014 Oto Godfrey and
      Justin Morton, CC-BY-SA 4.0,
      \url{https://commons.wikimedia.org/wiki/File:TeamTimeCar.com-BTTF_DeLorean_Time_Machine-OtoGodfrey.com-JMortonPhoto.com-07.jpg}.
      \\[1.5mm]
      Copyright of other images included in this document is held by
      their respective owners.
      \\[3.0mm]
      This work is licensed under the \alert{Creative Commons
        Attribution-Share Alike 3.0} License.  To view a copy of this
      license, visit
      \url{https://creativecommons.org/licenses/by-sa/3.0/} or send a
      letter to Creative Commons, 171 Second Street, Suite 300, San
      Francisco, California, 94105, USA.
      \\[2.0mm]
      At your option, you may instead copy, distribute and/or modify
      this document under the terms of the \alert{GNU Free Documentation
        License, Version 1.3 or any later version} published by the Free
      Software Foundation; with no Invariant Sections, no Front-Cover
      Texts, and no Back-Cover Texts.  A copy of the license is
      available at \url{https://www.gnu.org/licenses/gfdl.html}.
      \\[2.0mm]
      % Give a link to the 'Transparent Copy', as per Section 3 of the GFDL.
      The source of this document is available from
      \url{https://git.sv.gnu.org/cgit/guix/maintenance.git}.
    }
  \end{textblock}
\end{frame}

\end{document}

% Local Variables:
% coding: utf-8
% comment-start: "%"
% comment-end: ""
% ispell-local-dictionary: "francais"
% compile-command: "rubber --pdf talk.tex"
% End:

%%  LocalWords:  Reproducibility
