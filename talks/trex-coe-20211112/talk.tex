% The comment below tells Rubber to compile the .dot files.
%
% rubber: module graphics
% rubber: rules rules.ini

% Make sure URLs are broken on hyphens.
% See <https://tex.stackexchange.com/questions/3033/forcing-linebreaks-in-url>.
\RequirePackage[hyphens]{url}

\documentclass[aspectratio=169]{beamer}

\usetheme{default}

\usefonttheme{structurebold}

% Nice sans-serif font.
\usepackage[sfdefault,lining]{FiraSans} %% option 'sfdefault' activates Fira Sans as the default text font
\renewcommand*\oldstylenums[1]{{\firaoldstyle #1}}

% Nice monospace font.
\usepackage{inconsolata}
%% \renewcommand*\familydefault{\ttdefault} %% Only if the base font of the document is to be typewriter style
\usepackage[T1]{fontenc}

\usepackage[utf8]{inputenc}
\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref,xspace,multicol}

\usecolortheme{seagull}         % white on black
\usepackage[absolute,overlay]{textpos}
\usepackage{tikz}
\usetikzlibrary{arrows,shapes,trees,shadows,positioning}
\usepackage{fancyvrb}           % for '\Verb'
\usepackage{xifthen}            % for '\isempty'

% Remember the position of every picture.
\tikzstyle{every picture}+=[remember picture]

\tikzset{onslide/.code args={<#1>#2}{%
  \only<#1>{\pgfkeysalso{#2}} % \pgfkeysalso doesn't change the path
}}

% Colors.
\definecolor{guixred1}{RGB}{226,0,38}  % red P
\definecolor{guixorange1}{RGB}{243,154,38}  % guixorange P
\definecolor{guixyellow}{RGB}{254,205,27}  % guixyellow P
\definecolor{guixred2}{RGB}{230,68,57}  % red S
\definecolor{guixred3}{RGB}{115,34,27}  % dark red
\definecolor{guixorange2}{RGB}{236,117,40}  % guixorange S
\definecolor{guixtaupe}{RGB}{134,113,127} % guixtaupe S
\definecolor{guixgrey}{RGB}{91,94,111} % guixgrey S
\definecolor{guixdarkgrey}{RGB}{46,47,55} % guixdarkgrey S
\definecolor{guixblue1}{RGB}{38,109,131} % guixblue S
\definecolor{guixblue2}{RGB}{10,50,80} % guixblue S
\definecolor{guixgreen1}{RGB}{133,146,66} % guixgreen S
\definecolor{guixgreen2}{RGB}{157,193,7} % guixgreen S

\definecolor{rescienceyellow}{RGB}{254,246,91}

\setbeamerfont{title}{size=\huge}
\setbeamerfont{frametitle}{size=\huge}
\setbeamerfont{normal text}{size=\Large}

% White-on-black color theme.
\setbeamercolor{structure}{fg=guixorange1,bg=black}
\setbeamercolor{title}{fg=white,bg=black}
\setbeamercolor{date}{fg=guixorange1,bg=black}
\setbeamercolor{frametitle}{fg=white,bg=black}
\setbeamercolor{titlelike}{fg=white,bg=black}
\setbeamercolor{normal text}{fg=white,bg=black}
\setbeamercolor{alerted text}{fg=guixyellow,bg=black}
\setbeamercolor{section in toc}{fg=white,bg=black}
\setbeamercolor{section in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsubsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsubsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{frametitle in toc}{fg=white,bg=black}
\setbeamercolor{local structure}{fg=guixorange1,bg=black}

\newcommand{\highlight}[1]{\alert{\textbf{#1}}}

\title{Au-delà des conteneurs : \\
  environnements reproductibles \\
  avec GNU Guix}

\author{Ludovic Courtès}
\date{12 November 2021}

\setbeamertemplate{navigation symbols}{} % remove the navigation bar

\AtBeginSection[]{
  \begin{frame}
    \frametitle{}
    \tableofcontents[currentsection]
  \end{frame} 
}


\newcommand{\screenshot}[2][width=\paperwidth]{
  \begin{frame}[plain]
    \begin{tikzpicture}[remember picture, overlay]
      \node [at=(current page.center), inner sep=0pt]
        {\includegraphics[{#1}]{#2}};
    \end{tikzpicture}
  \end{frame}
}


\begin{document}

\begin{frame}[plain, fragile]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.center), fill=guixorange2, opacity=.4, text opacity=.8] {
      \includegraphics[width=1.2\textwidth]{images/post-office-parcel-people}
    };
  \end{tikzpicture}

  \vspace{10mm}
  \Huge{\textbf{Reproducible deployment \\ with GNU~Guix: \\
    theory \& packaging}}
  \\[13mm]
  \large{Ludovic Courtès}
  \\[2mm]
  \alert{TREX CoE build system hackathon \\ \oldstylenums{12 November 2021}}

  \vfill{}
  \hfill{\includegraphics[width=0.2\paperwidth]{images/inria-white-2019}}
  \vspace{2mm}
\end{frame}


\setbeamercolor{normal text}{fg=black,bg=white}
% http://www.nature.com/ngeo/journal/v7/n11/full/ngeo2294.html
%% \screenshot{images/nature-transparency}

% https://www.nature.com/nmeth/journal/v12/n12/full/nmeth.3686.html
%% \screenshot{images/nature-reviewing-computational-methods}
% http://blogs.nature.com/methagora/2014/02/guidelines-for-algorithms-and-software-in-nature-methods.html

% http://www.acm.org/publications/policies/artifact-review-badging
%% \screenshot[height=\paperheight]{images/acm-artifact-review-and-badging}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]{
      \includegraphics[height=.8\textheight]{images/acm-artifacts-functional}
    };
    \node [at=(current page.south), anchor=south,
      text=guixdarkgrey, fill=white, text opacity=1]{
      \small{\url{https://www.acm.org/publications/policies/artifact-review-badging}}
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]{
      \includegraphics[height=.8\textheight]{images/rescience}
    };
    \node [at=(current page.south), anchor=south,
      text=guixdarkgrey, fill=white, text opacity=1]{
      \small{\url{https://rescience.github.io/}}
    };
  \end{tikzpicture}
\end{frame}

%% \begin{frame}[plain]
%%   \begin{tikzpicture}[remember picture, overlay]
%%     \node [at=(current page.center), inner sep=0pt]{
%%       \includegraphics[width=.9\textwidth]{images/repeatability-study}
%%     };
%%     \node [at=(current page.south east), anchor=south east,
%%       text=guixdarkgrey, fill=white, text opacity=1]{
%%       \small{\url{http://reproducibility.cs.arizona.edu/}}
%%     };
%%   \end{tikzpicture}
%% \end{frame}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \includegraphics[width=\textwidth]{images/big-picture-1}
\end{frame}
\begin{frame}[plain]
  \includegraphics[width=\textwidth]{images/big-picture-2}
\end{frame}
\begin{frame}[plain]
  \includegraphics[width=\textwidth]{images/big-picture-3}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\begin{frame}[plain]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.center), rotate=8] {
      \includegraphics[width=1.2\textwidth]{images/autoconf-snippet}
    };
    \node [at=(current page.center), text width=\paperwidth, text
      height=\paperheight, fill=black, opacity=.4] {};
  \end{tikzpicture}
    
  \Huge{\textbf{So you have your\\ beautiful build system...}}
  \\[5mm]
  \Huge{\textbf{... now what?}}
\end{frame}

%% \begin{frame}[plain]
%%   \begin{tikzpicture}[remember picture, overlay]
%%     \node [at=(current page.center), inner sep=0pt]
%%     {\includegraphics[width=\paperwidth]{images/IBM_Blue_Gene_P_supercomputer}};

%%     \node[at=(current page.center), rounded corners=4, text centered,
%%           inner sep=3mm, opacity=.75, text opacity=1]{
%%             \Huge{\textbf{HPC = cutting edge?}}
%%     };
%%   \end{tikzpicture}
%% \end{frame}

\setbeamercolor{normal text}{fg=white,bg=guixtaupe}
\begin{frame}[plain, fragile]
  \LARGE{
  \begin{semiverbatim}
\alert{./configure} && make && make install

\textsf{Or:}

\alert{cmake} .. && make && make install
  \end{semiverbatim}
  }
\end{frame}

\setbeamercolor{normal text}{fg=black,bg=white}
\screenshot{images/environment-modules}
\setbeamercolor{normal text}{fg=white,bg=black}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.north), anchor=north,
      fill=white, text width=\paperwidth, text centered, inner sep=0pt,
      inner sep=0.2\paperheight]
          {\includegraphics[width=0.3\paperwidth]{images/spack}};
    \node [at=(current page.center), anchor=center,
      text width=\paperwidth, text centered,
      inner sep=0.2\paperheight]
          {\includegraphics[width=0.4\paperwidth]{images/conda}};
    \node [at=(current page.south), anchor=south,
      text width=\paperwidth, text centered, inner sep=25pt,
      text height=0.5\paperheight]
          {\includegraphics[width=0.3\paperwidth]{images/easybuild}};

          % https://github.com/LLNL/spack/blob/develop/share/spack/logo/spack-logo-text-64.png
          % https://github.com/LLNL/spack/blob/develop/share/spack/logo/spack-logo-white-text-48.png
          % https://docs.conda.io/en/latest/_images/conda_logo.svg
  \end{tikzpicture}
\end{frame}

%% \screenshot[width=\paperwidth]{images/easybuild-bug}

% https://github.com/spack/spack/issues?q=is%3Aissue+is%3Aopen+label%3Abuild-error
%% \screenshot[width=\paperwidth]{images/spack-bug}
\screenshot[width=\paperwidth]{images/spack-build-errors}
\screenshot[width=.9\paperwidth]{images/conda-broke-tweet}

\setbeamercolor{normal text}{bg=guixorange1}
\begin{frame}[plain, fragile]
  \vspace{1cm}
  \Huge{\textbf{Containers to the rescue?}}

  \begin{tikzpicture}[overlay]
    \node at (current page.south east) [anchor=south east, outer sep=3mm] {
      \includegraphics[width=.3\paperwidth]{images/docker}
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=white}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
      {\includegraphics[width=1.3\textwidth]{images/smoothie}};
    \node [at=(current page.south east), anchor=south east, text=guixgrey]
      {\small{courtesy of Ricardo Wurmus}};
  \end{tikzpicture}
\end{frame}

% https://xkcd.com/1988/

\screenshot[height=0.95\paperheight]{images/singularity-def-file}
%% \screenshot[width=\paperwidth]{images/rena-container-ship-wreck-nz}

\setbeamercolor{normal text}{bg=white,fg=guixorange1}
\begin{frame}[fragile]
  \begin{tikzpicture}[overlay]
    \node(logo) [at=(current page.center), inner sep=0pt]
      {\includegraphics[width=\textwidth]{images/guixhpc-logo-transparent-white}};
    %% \node [at=(logo.south), anchor=north, text=black, inner sep=10pt]
    %%   {\Large{\textbf{Reproducible software deployment\\for high-performance computing.}}};
    \node [at=(current page.south), anchor=south, text=guixdarkgrey, inner sep=20pt]
      {\Large{\url{https://hpc.guix.info}}};
  \end{tikzpicture}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% \begin{frame}
%%   \LARGE{
%%     \begin{enumerate}
%%     \item transactional package manager
%%     \item software environment manager
%%     \item APIs \& tools to customize environments
%%     \item container provisioning tools
%%     \end{enumerate}
%%   }
%% \end{frame}

\begin{frame}
  \Large{
  \begin{itemize}
    \item Guix started in 2012
    \item \highlight{$\approx$20,000 packages}, all free software
    \item \highlight{4.5 architectures}:\\
      x86\_64, i686, ARMv7, AArch64, POWER9
    %% \item binaries available
    \item \highlight{Guix-HPC effort (Inria, MDC, UBC, UTHCS) started in 2017}
    \item \textbf{Guix 1.3.0 released May 2021}
  \end{itemize}
  }
\end{frame}

%% \setbeamercolor{normal text}{bg=white}
%% \screenshot[width=.7\paperwidth]{images/Guix-1-0}
%% \setbeamercolor{normal text}{bg=black}


\begin{frame}[fragile]

  \begin{semiverbatim}
    \LARGE{
guix \alert{install} gcc-toolchain openmpi hwloc

eval `guix package \alert{--search-paths}=prefix`

guix package \alert{--roll-back}

guix \alert{environment} --ad-hoc \\
     gcc-toolchain@5.5 hwloc@1
}
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]
  \begin{semiverbatim}
    \LARGE{
guix package \alert{--manifest}=my-packages.scm



    (\alert{specifications->manifest}
      '("gcc-toolchain" "openmpi"
        "scotch" "mumps"))
}
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{bg=guixdarkgrey}
\begin{frame}[fragile]
  \begin{semiverbatim}
    \Large{
bob@laptop$ guix package \alert{--manifest}=my-packages.scm
bob@laptop$ guix \alert{describe}
  guix cabba9e
    repository URL: https://git.sv.gnu.org/git/guix.git
    commit: cabba9e15900d20927c1f69c6c87d7d2a62040fe

\pause


alice@supercomp$ guix \alert{pull} --commit=cabba9e
alice@supercomp$ guix package \alert{--manifest}=my-packages.scm
}
  \end{semiverbatim}
\end{frame}


\begin{frame}[fragile]
  \begin{tikzpicture}[remember picture, overlay]
    % https://commons.wikimedia.org/wiki/File:TeamTimeCar.com-BTTF_DeLorean_Time_Machine-OtoGodfrey.com-JMortonPhoto.com-07.jpg
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=\paperwidth]{images/delorean}};
    \node [rounded corners=4, text centered, anchor=north,
           text width=10cm,
          inner sep=3mm, opacity=.75, text opacity=1]
      at (current page.center) {
            \textbf{\Huge{travel in space \emph{and} time!}}
          };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=guixgrey}
\begin{frame}[fragile]
  \begin{semiverbatim}
    \LARGE{
guix \alert{time-machine} --commit=cabba9e -- \\
     install hello
    }
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]
  \begin{semiverbatim}
(define pastix
  (\alert{package}
    (name "pastix")
    (home-page "https://gitlab.inria.fr/solverstack/pastix")
    (\alert{source} (origin
              (method git-fetch)
              (uri (git-reference
                     (\alert{url} home-page)
                     (\alert{commit} "2f30ff07a")\tikz{\node(commit){};}
                     (recursive? #t)))
              (sha256
               (base32
                "106rf402cvfdhc2yf\textrm{...}"))))
    \textrm{...}))
  \end{semiverbatim}

  \begin{tikzpicture}[overlay]
    \node<2->(swh) [inner sep=3mm, rounded corners, fill=black,
                    opacity=.3, text opacity=1] at (12,5) {
       % https://annex.softwareheritage.org/public/logo/
       \includegraphics[width=0.33\textwidth]{images/software-heritage-logo-title-white}
    };
    \node<2->      [at=(current page.south), anchor=south,
                    inner sep=2mm, rounded corners, fill=black, text width=13cm,
                    opacity=.3, text opacity=1] {
       \url{https://www.softwareheritage.org/2019/04/18/software-heritage-and-gnu-guix-join-forces-to-enable-long-term-reproducibility/}
    };

    \path<2->[very thick, draw=guixorange1]
      (swh) edge [out=-90, in=0, ->] (commit);
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=guixred3}
\begin{frame}
  \Large{
    \begin{itemize}
      % http://zvfak.blogspot.ch/2015/07/gnu-guix-for-easily-managing.html
    \item \highlight{PlaFRIM} (FR): Inria Bordeaux (3,000+ cores)
    \item \highlight{GriCAD} (FR): Grenoble (1,000+ cores)
    \item \highlight{CCIPL} (FR): Nantes (4,000+ cores)
    \item \highlight{Grid'5000} (FR): 8 sites (12,000+ cores)
    \item \highlight{Max Delbrück Center} (DE): 250-node cluster +
      workstations
      % https://ubc.uu.nl/infrastructure/
      % https://wiki.bioinformatics.umcutrecht.nl/pub/HPC/WebHome/HPC_Flyer.png
    \item \highlight{UMC Utrecht} (NL): 68-node cluster (1,000+ cores)
      % https://www.qriscloud.org.au/support/qriscloud-documentation/75-euramoo-datasheet
      % https://www.qriscloud.org.au/support/qriscloud-documentation/76-flashlite-datasheet
    \item ...
    \end{itemize}
  }
\end{frame}

\begin{frame}[fragile]%{Container provisioning}
  \begin{tikzpicture}[overlay]
    \node<2>      [at=(current page.south), anchor=south,
                    inner sep=2mm, rounded corners, fill=black, text width=10cm,
                    opacity=.3, text opacity=1] {
       \url{https://hpc.guix.info/blog/2020/05/faster-relocatable-packs-with-fakechroot/}
    };
  \end{tikzpicture}

  \LARGE{
    \begin{semiverbatim}
\$ guix \alert{pack}\only<2>{ --relocatable}\only<3>{ --format=squashfs}\only<4->{ --format=docker} \\
      python python-numpy python-scipy
\textrm{...}
/gnu/store/\textrm{...}-\only<1-2>{pack.tar.gz}\only<3>{singularity-image.tar.gz}\only<4->{docker-image.tar.gz}
    \end{semiverbatim}
  }
\end{frame}

\setbeamercolor{normal text}{bg=white}
\screenshot[width=.9\paperwidth]{images/docker-guix-lol}

\setbeamercolor{normal text}{bg=guixdarkgrey}
\begin{frame}[fragile]
  \begin{semiverbatim}
\LARGE{
guix pack hwloc \\
  \alert{--with-source}=./hwloc-2.1rc1.tar.gz


guix install mumps \\
  \alert{--with-input}=scotch=pt-scotch
}
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.center)] {
      \includegraphics[width=.8\paperwidth]{images/parcel}
    };
    \node [at=(current page.center), text width=\paperwidth, text
      height=\paperheight, fill=guixblue2, opacity=.5] {};
  \end{tikzpicture}
  \Huge{\textbf{Your first package.}}
\end{frame}

\setbeamercolor{normal text}{bg=guixdarkgrey}
\begin{frame}[plain]
  \vspace{2cm}
  \LARGE{
    \begin{enumerate}
    \setcounter{enumi}{-1}
    \item \textbf{Install} Guix: \url{https://guix.gnu.org/en/download}
    \item Create a \textbf{Git repository}---a \emph{channel}
    \item \textbf{Write} (or generate) a \emph{package definition}
    \item \textbf{Test} it with \texttt{guix build}
    \item \textbf{Iterate} :-)
    \item \textbf{Commit}, push, enjoy!
    \item<2-> (\emph{optional}) \textbf{Publish} binaries with
      \texttt{guix publish}
    \end{enumerate}
  }
\end{frame}

\begin{frame}[plain, fragile]
  \Huge{Feeling lucky?}

  \vspace{3cm}
  \LARGE{
  \begin{semiverbatim}
\alert{guix import} pypi my-package > ~/my-def.scm
  \end{semiverbatim}
  }
\end{frame}

\begin{frame}[plain, fragile]
  \begin{semiverbatim}
    \large{
(\alert{define-public} hello-trex
  (\alert{package}
    (name "hello-trex")
    (version "1.0")
    (source (\alert{origin}
              (method url-fetch)
              (uri (string-append
                    "http://example.org/hello-" version
                    ".tar.gz"))
              (sha256 (base32 \tikz[baseline]{\node[anchor=base](hash){"0wqd\textrm{...}dz6"};}))))
    (\alert{build-system} \only<1-3>{\tikz[baseline]{\node[anchor=base](gbs){gnu-build-system};}}\only<4->{\alert<4>{cmake-build-system}})\only<5->{
   \tikz[baseline]{\node[anchor=base](deps){(\alert<5-6>{inputs} `(("openmpi" ,\tikz[baseline]{\node[anchor=base](depvar){\alert<5-6>{openmpi}};}) ("petsc" ,\tikz[baseline]{\node[anchor=base](depvarright){\alert<5-6>{petsc}};})))};}}
    (synopsis "The great package")
    (description "The tyrannosaurus Rex is back.")
    (home-page "https://example.org")
    (license license:gpl3+)))
}
  \end{semiverbatim}

  \begin{tikzpicture}[remember picture, overlay,
      label/.style = {
        rounded corners, fill=white, text=black,
        opacity=.7, text opacity=1, inner sep=3mm
      },
      labelarrow/.style = {
        very thick, draw=guixorange1, ->
      }]
      
    \node<2>(guixhash) [label] at (11,7) {
      \texttt{guix hash hello-1.0.tar.gz}
    };
    \path<2>[labelarrow]
      (guixhash) edge [out=-90, in=0, ->] (hash);

    \node<3>(labelgbs)[label] at (7,8) {
      \texttt{./configure \&\& make install}...
    };
    \node<3>(labelgbsdeps)[label] at (11,6) {
      depends on \texttt{gcc}, \texttt{make}, \texttt{bash}, etc.
    };
    \path[labelarrow]<3>(labelgbs) edge (gbs);
    \path[labelarrow]<3>(labelgbsdeps) edge (gbs);

    \node<6>(labeldeps)[label] at (3,6) {dependencies};
    \node<6>(labeldepvar)[label] at (12,5) {reference to a variable};
    \path[labelarrow]<6>(labeldeps) edge (deps);
    \path[labelarrow]<6>(labeldepvar) edge (depvar);
    \path[labelarrow]<6>(labeldepvar) edge (depvarright);
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain, fragile]
  \Large{
  \begin{semiverbatim}
(\alert{define-module} (\tikz[baseline]{\node(modulename)[anchor=base]{\alert{\only<1-2>{t-rex}\only<3>{t-rex hello}}};})
  #:use-module (guix)
  #:use-module (guix build-system cmake)
  #:use-module ((guix licenses) #:prefix license:))

(\alert{define-public} hello-trex
  (\alert{package}
    \textrm{\dots{}}))
  \end{semiverbatim}

  \begin{tikzpicture}[remember picture, overlay,
      label/.style = {
        rounded corners, fill=white, text=black,
        opacity=.7, text opacity=1, inner sep=3mm
      },
      labelarrow/.style = {
        very thick, draw=guixorange1, ->
      }]
      
    \node<2-3>(modulenamelabel) [label] at (8,3) {
      for a file called \texttt{\only<2>{t-rex.scm}\only<3>{t-rex/hello.scm}}
    };
    \path[labelarrow]<2-3> (modulenamelabel) edge (modulename);
  \end{tikzpicture}
  }
\end{frame}

\setbeamercolor{normal text}{bg=guixtaupe}
\begin{frame}[plain]
  \Huge{\textbf{Time to build it!}}
\end{frame}

\setbeamercolor{normal text}{bg=guixdarkgrey}
\begin{frame}[plain, fragile]
  \Large{
  \begin{semiverbatim}
$ \alert{guix build} -L ~/src/trex hello-trex
\uncover<2->{ice-9/eval.scm:223:20: In procedure proc:
error: openmpi: unbound variable
\alert{hint}: Did you forget `(use-modules (gnu packages mpi))'?
\uncover<3->{
# \textsf{Edit file, add \texttt{#:use-module (gnu packages mpi)}, save...}
\uncover<4->{
$ \alert{guix build} -L ~/src/trex hello-trex
\textrm{\dots{}}
/gnu/store/\alert{\textrm{\dots{}}-hello-trex-1.0}
}}}
  \end{semiverbatim}
  }
\end{frame}

\begin{frame}[plain, fragile]
  \Large{
    \begin{semiverbatim}
\alert{guix install} -L ~/src/trex hello-trex

\alert{guix environment} -L ~/src/trex hello-trex

\alert{guix pack} -f docker -L ~/src/trex hello-trex

\textrm{...}
    \end{semiverbatim}
  }
\end{frame}

\begin{frame}[plain, fragile]
  \Huge{Last steps}
  \\[4mm]
  \Large{
  \begin{itemize}
  \item Publish Git repository
  \item{ Have users extend \texttt{\~/.config/guix/channels.scm}:

\large{
  \begin{semiverbatim}
(\alert{append} (\alert{list} (\alert{channel}
                (name 'my-channel)
                (url "https://example.org/my-channel.git"))
        %default-channels)
  \end{semiverbatim}
  }
  }
  \item ... and run \texttt{guix pull}
  \end{itemize}
  }
\end{frame}

\setbeamercolor{normal text}{bg=guixgreen1}
\begin{frame}[plain, fragile]
  \Huge{Need help?}
  \\[7mm]
  \Large{
    \begin{itemize}
    \item \url{https://guix.gnu.org/en/help}
    \item \url{https://guix.gnu.org/manual/en/html_node/Defining-Packages.html}
    \end{itemize}
  }
\end{frame}

\setbeamercolor{normal text}{bg=guixblue1}
\begin{frame}[plain]
  \Huge{\textbf{Wrap-up.}}
\end{frame}

\setbeamercolor{normal text}{bg=white}
\screenshot[width=\textwidth]{images/big-picture-3}

%% \setbeamercolor{normal text}{fg=white,bg=black}
%% \begin{frame}
%%   \LARGE{
%%     \begin{itemize}
%%     \item \highlight{reproduce} software environments
%%     \item \highlight{declare \& publish} complete environments
%%     \item beyond replication: precision \highlight{experimentation}
%%     \item a foundation for \highlight{``deployment-aware'' apps}
%%     \end{itemize}
%%   }
%% \end{frame}

\screenshot{images/guix-scope}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[fragile]
  \vspace{-2cm}
  \begin{tikzpicture}
    \matrix[row sep=10mm, column sep=1cm]{
      % https://git-scm.com/downloads/logos
      \node {\includegraphics[width=0.2\textwidth]{images/Git-Logo-2Color}}; &
      \node {\includegraphics[width=0.15\textwidth]{images/arrow-right}}; &
      \node {\includegraphics[width=0.24\textwidth]{images/Guix-horizontal-print}};
      \\
    };
  \end{tikzpicture}

  \begin{tikzpicture}[overlay]
    \node [at=(current page.center), anchor=north,
           text=black, text width=.9\textwidth]{
      \Huge{Let's add\\ \textbf{reproducible deployment}\\
        to our best practices book. \par
      }};
  \end{tikzpicture}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setbeamercolor{normal text}{bg=black}
\begin{frame}[plain]

\vfill{
  \vspace{3cm}
  \center{\includegraphics[width=0.5\textwidth]{images/guixhpc-logo-transparent}}\\[1.0cm]
  \texttt{ludovic.courtes@inria.fr |} @GuixHPC
  \center{\alert{\url{https://hpc.guix.info}}}
  \\[1cm]
}
\end{frame}

\setbeamercolor{normal text}{bg=guixred2}
\begin{frame}
  \Huge{\textbf{Bonus slides!}}
\end{frame}

\setbeamercolor{normal text}{bg=black}
\begin{frame}[fragile]
  %% \frametitle{Bit-Reproducible Builds$^*$}
  %% \framesubtitle{$^*$ almost!}

  \begin{semiverbatim}
\Large{
\$ guix build hwloc
\uncover<2->{/gnu/store/\tikz[baseline]{\node[anchor=base](nixhash){\alert<2>{h2g4sf72\textrm{...}}};}-hwloc-1.11.2}

\uncover<3->{\$ \alert<3>{guix gc --references /gnu/store/\textrm{...}-hwloc-1.11.2}
/gnu/store/\textrm{...}-glibc-2.24
/gnu/store/\textrm{...}-gcc-4.9.3-lib
/gnu/store/\textrm{...}-hwloc-1.11.2
}}
  \end{semiverbatim}

  \begin{tikzpicture}[overlay]
    \node<1>(labelnixhash) [fill=white, text=black, inner sep=0.5cm,
       rounded corners] at (current page.center) {%
      \Large{\textbf{isolated build}: chroot, separate name spaces, etc.}
    };

    \node<2>(labelnixhash) [fill=white, text=black] at (4cm, 2cm) {%
      hash of \textbf{all} the dependencies};
    \path[->]<2>(labelnixhash.north) edge [bend left, in=180, out=-45] (nixhash.south);

    \draw<4-> (-10pt, 105pt) [very thick, color=guixorange2, rounded corners=8pt]
      arc (10:-50:-50pt and 110pt);
    \node<4->[fill=white, text=black, text opacity=1, opacity=.7,
          rounded corners=2mm, inner sep=5mm]
      at (7, 2) {\textbf{\Large{(nearly) bit-identical for everyone}}};
  \end{tikzpicture}

\end{frame}

\setbeamercolor{normal text}{fg=black,bg=white}
\begin{frame}[fragile]{}
  \begin{tikzpicture}[tools/.style = {
                        text width=65mm, minimum height=3cm,
                        text badly ragged,
                        rounded corners=2mm,
                        fill=black, text=white
                      },
                      tool/.style = {
                        fill=black, text=white, text width=6cm,
                        text centered
                      },
                      daemon/.style = {
                        rectangle, text width=50mm, text centered,
                        rounded corners=2mm, minimum height=15mm,
                        top color=guixorange1,
                        bottom color=guixyellow,
                        text=black
                      },
                      builders/.style = {
                        draw=guixorange1, very thick, dashed,
                        fill=white, text=black, text width=5cm,
                        rounded corners=2mm,
                      },
                      builder/.style = {
                        draw=guixred2, thick, rectangle,
                        fill=guixgrey, rotate=90
                      }]
    \matrix[row sep=7mm, column sep=18mm] {
      \node(builders)[builders, text height=5cm]{}
          node[fill=white, text=black] at (0, 2) {\large{\textbf{build processes}}}
          node[fill=white, text=black] at (0, 1.5) {chroot, separate UIDs}
          node[builder, onslide=<1-2>{white}] at (-1,-0.5) {\alert<3->{Guile}, make, etc.}
          node[builder, onslide=<1-2>{white}] at ( 0,-0.5) {\alert<3->{Guile}, make, etc.}
          node[builder, onslide=<1-2>{white}] at ( 1,-0.5) {\alert<3->{Guile}, make, etc.}; &
      \node[tools]{}
          node[fill=black, text=white] at (0, 1) {\large{\textbf{client commands}}}
          node(client)[tool] at (0, 0)
          {\texttt{guix build hello}};
      \\

      \node(daemon)[daemon]{\large{\textbf{build daemon}}}; &
      &
      \\
    };
  \end{tikzpicture}

  \begin{tikzpicture}[overlay]
    \path[very thick, draw=guixorange1]<2->
      (client.south) edge [out=-90, in=0, ->, text=black] node[below, sloped]{RPCs} (daemon.east);
    \path[->, very thick, draw=guixorange1]<3->
      (daemon) edge (builders);
  \end{tikzpicture}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=0.95\paperwidth]{images/snap-crypto-miner}};
    \node [at=(current page.south east), anchor=south east,
           text=black, text opacity=1, fill=white]{
      \small{\url{https://github.com/canonical-websites/snapcraft.io/issues/651}}
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=0.9\paperwidth]{images/lwn-docker-hello-world}};
    \node [at=(current page.south east), anchor=south east,
           text=white, fill=black, text opacity=1]{
      \small{\url{https://lwn.net/Articles/752982/}}
    };
  \end{tikzpicture}
\end{frame}



\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[height=\paperheight]{images/hwloc-graph}};
    \node [at=(current page.south west), anchor=south west, text=black] {
          \texttt{guix graph hwloc}
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[height=\paperheight]{images/hwloc-runtime-graph}};
    \node [at=(current page.south west), anchor=south west, text=black] {
          \texttt{guix graph --type=references hwloc}
    };
  \end{tikzpicture}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\begin{frame}[fragile]
  \begin{semiverbatim}
    \vspace{-1cm}
    \small{
(\alert{operating-system}
  (host-name "guixbox")
  (timezone "Europe/Brussels")
  (locale "fr_BE.utf8")
  (bootloader (\alert{bootloader-configuration}
                (bootloader grub-efi-bootloader)
                (target "/boot/efi")))
  (file-systems (append (list (\alert{file-system}
                                (device (file-system-label "my-root"))
                                (mount-point "/")
                                (type "ext4")))
                        %base-file-systems))
  (users (append (list (\alert{user-account}
                         (name "charlie")
                         (group "users")
                         (home-directory "/home/charlie")))
                 %base-user-accounts))
  (services (append (list (\alert{service} dhcp-client-service-type)
                          (\alert{service} openssh-service-type))
                    %base-services)))
    }
  \end{semiverbatim}

  \begin{tikzpicture}[overlay]
    \node<2-5> [at=(current page.center), fill=black, opacity=.6, text opacity=1,
        minimum width=\paperwidth, minimum height=\paperheight] {
      \LARGE{
      \texttt{guix system \alert{\only<2>{vm}\only<3>{docker-image}\only<4>{container}\only<5>{reconfigure}} config.scm}
      }
    };
    \node<6> [at=(current page.center), fill=black, opacity=.6, text opacity=1,
        minimum width=\paperwidth, minimum height=\paperheight] {
      \Huge{\textbf{The next step?}}
    };
  \end{tikzpicture}
\end{frame}


\begin{frame}{}
  \begin{textblock}{12}(2, 4)
    \tiny{
      Copyright \copyright{} 2010, 2012--2021 Ludovic Courtès \texttt{ludo@gnu.org}.\\[3.0mm]
      GNU Guix logo, CC-BY-SA 4.0, \url{https://gnu.org/s/guix/graphics}.
      \\[1.5mm]
      Smoothie image and hexagon image \copyright{} 2019 Ricardo Wurmus,
      CC-BY-SA 4.0.
      \\[1.5mm]
      Parcel image from
      \url{https://thumbs.dreamstime.com/z/parcel-illustration-drawing-engraving-ink-line-art-vector-what-made-pencil-paper-then-was-digitalized-143335396.jpg}
      \\[1.5mm]
      Hand-drawn arrows by Freepik from flaticon.com.
      \\[1.5mm]
      DeLorean time machine picture \copyright{} 2014 Oto Godfrey and
      Justin Morton, CC-BY-SA 4.0,
      \url{https://commons.wikimedia.org/wiki/File:TeamTimeCar.com-BTTF_DeLorean_Time_Machine-OtoGodfrey.com-JMortonPhoto.com-07.jpg}.
      \\[1.5mm]
      Copyright of other images included in this document is held by
      their respective owners.
      \\[3.0mm]
      This work is licensed under the \alert{Creative Commons
        Attribution-Share Alike 3.0} License.  To view a copy of this
      license, visit
      \url{https://creativecommons.org/licenses/by-sa/3.0/} or send a
      letter to Creative Commons, 171 Second Street, Suite 300, San
      Francisco, California, 94105, USA.
      \\[2.0mm]
      At your option, you may instead copy, distribute and/or modify
      this document under the terms of the \alert{GNU Free Documentation
        License, Version 1.3 or any later version} published by the Free
      Software Foundation; with no Invariant Sections, no Front-Cover
      Texts, and no Back-Cover Texts.  A copy of the license is
      available at \url{https://www.gnu.org/licenses/gfdl.html}.
      \\[2.0mm]
      % Give a link to the 'Transparent Copy', as per Section 3 of the GFDL.
      The source of this document is available from
      \url{https://git.sv.gnu.org/cgit/guix/maintenance.git}.
    }
  \end{textblock}
\end{frame}

\end{document}

% Local Variables:
% coding: utf-8
% comment-start: "%"
% comment-end: ""
% ispell-local-dictionary: "francais"
% compile-command: "guix time-machine --commit=c81457a5883ea43950eb2ecdcbb58a5b144bcd11 -- environment --ad-hoc texlive rubber -- rubber --pdf talk.tex"
% End:

%%  LocalWords:  Reproducibility
