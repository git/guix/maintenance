% The comment below tells Rubber to compile the .dot files.
%
% rubber: module graphics
% rubber: rules rules.ini

% Make sure URLs are broken on hyphens.
% See <https://tex.stackexchange.com/questions/3033/forcing-linebreaks-in-url>.
\RequirePackage[hyphens]{url}

\documentclass[aspectratio=169]{beamer}
\usepackage{ragged2e}           % for 'flushleft', etc.

\usetheme{default}

\usefonttheme{structurebold}

% Nice sans-serif font.
\usepackage[sfdefault,lining]{FiraSans} %% option 'sfdefault' activates Fira Sans as the default text font
%% \usepackage[fakebold]{firamath-otf}
\renewcommand*\oldstylenums[1]{{\firaoldstyle #1}}

% Nice monospace font.
\usepackage{inconsolata}
%% \renewcommand*\familydefault{\ttdefault} %% Only if the base font of the document is to be typewriter style
\usepackage[T1]{fontenc}


% Typeset maths using a slanted, serif font.
\def\mathfamilydefault{\rmdefault}

\ProcessOptionsBeamer

\usepackage{helvet}


%% \usepackage{fontspec}
%% \setmainfont[
%%     Ligatures=TeX,
%%     UprightFont = *-Boo,
%%     ItalicFont = *-BooObl,
%%     SmallCapsFont = *SC-Boo,
%%     BoldFont = *-Dem,
%%     BoldItalicFont = *-DemObl
%% ]{Futura}

\usepackage{multimedia}         % movie
\usecolortheme{seagull}         % white on black

\usepackage[utf8]{inputenc}
\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref,xspace,multicol}
\usepackage[absolute,overlay]{textpos}
\usepackage{tikz}
\usetikzlibrary{arrows,shapes,trees,shadows,positioning}
\usepackage{fancyvrb}           % for \Verb

% Remember the position of every picture.
\tikzstyle{every picture}+=[remember picture]

\tikzset{onslide/.code args={<#1>#2}{%
  \only<#1>{\pgfkeysalso{#2}} % \pgfkeysalso doesn't change the path
}}

% Colors.
\definecolor{guixred1}{RGB}{226,0,38}  % red P
\definecolor{guixorange1}{RGB}{243,154,38}  % guixorange P
\definecolor{guixyellow}{RGB}{254,205,27}  % guixyellow P
\definecolor{guixred2}{RGB}{230,68,57}  % red S
\definecolor{guixred3}{RGB}{115,34,27}  % dark red
\definecolor{guixorange2}{RGB}{236,117,40}  % guixorange S
\definecolor{guixtaupe}{RGB}{134,113,127} % guixtaupe S
\definecolor{guixgrey}{RGB}{91,94,111} % guixgrey S
\definecolor{guixdarkgrey}{RGB}{46,47,55} % guixdarkgrey S
\definecolor{guixblue1}{RGB}{38,109,131} % guixblue S
\definecolor{guixblue2}{RGB}{10,50,80} % guixblue S
\definecolor{guixgreen1}{RGB}{133,146,66} % guixgreen S
\definecolor{guixgreen2}{RGB}{157,193,7} % guixgreen S

\setbeamerfont{title}{size=\huge}
\setbeamerfont{frametitle}{size=\huge}
\setbeamerfont{normal text}{size=\Large}

% White-on-black color theme.
\setbeamercolor{structure}{fg=guixorange1,bg=black}
\setbeamercolor{title}{fg=white,bg=black}
\setbeamercolor{date}{fg=guixorange1,bg=black}
\setbeamercolor{frametitle}{fg=white,bg=black}
\setbeamercolor{titlelike}{fg=white,bg=black}
\setbeamercolor{normal text}{fg=white,bg=black}
\setbeamercolor{alerted text}{fg=guixyellow,bg=black}
\setbeamercolor{section in toc}{fg=white,bg=black}
\setbeamercolor{section in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsubsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsubsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{frametitle in toc}{fg=white,bg=black}
\setbeamercolor{local structure}{fg=guixorange1,bg=black}

\newcommand{\highlight}[1]{\alert{\textbf{#1}}}

\title{GNU Guix: Unifying provisioning, deployment, and package management}
\author{Ludovic Courtès}
\date{FOSDEM, 1 February 2020}

\setbeamertemplate{navigation symbols}{} % remove the navigation bar

\AtBeginSection[]{
  \begin{frame}
    \frametitle{}
    \tableofcontents[currentsection]
  \end{frame} 
}


\newcommand{\screenshot}[2][width=\paperwidth]{
  \begin{frame}[plain]
    \begin{tikzpicture}[remember picture, overlay]
      \node [at=(current page.center), inner sep=0pt]
        {\includegraphics[{#1}]{#2}};
    \end{tikzpicture}
  \end{frame}
}


\begin{document}

\begin{frame}[plain, fragile]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=\paperwidth]{images/sun}};
  \end{tikzpicture}

  {\color{guixdarkgrey}\Huge{\textbf{\\GNU Guix:\\Unifying provisioning,
  deployment,\\and package management}}}
  \vfill{}
  {\color{guixdarkgrey}{\Large{Ludovic Courtès}}}
  \\[4mm]
  {\color{guixorange2}{\Large{{FOSDEM, \oldstylenums{1 February 2020}}}}}
\end{frame}

\setbeamercolor{normal text}{bg=guixred3,fg=white}
\begin{frame}[plain]
  \begin{quotation}
    \noindent
    \begin{flushright}
    \LARGE{``The Linux distribution as we know it is \textbf{coming to
        an end}, and is being replaced by a new concept of
      containerized, multi-instance, multi-user applications [...]''}
    \end{flushright}
  \end{quotation}
  \hfill{--- Daniel Riek (\oldstylenums{2020})}

  \begin{tikzpicture}[overlay]
    \node [at=(current page.south east), anchor=south east]{
      \url{https://fosdem.org/2020/schedule/event/riek_kubernetes/}
    };
  \end{tikzpicture}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

% https://commons.wikimedia.org/wiki/Category:Sun?uselang=fr#/media/File:%22Sun%22.JPG
% https://en.wikipedia.org/wiki/Zenith#/media/File:Tropical-area-mactan-philippines.jpg
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=\paperwidth]{images/sun}};
    \node [at=(current page.center), text=black,
           text opacity=1, rounded corners=2pt]
          {\LARGE{\textbf{ Slackware {\tt |} Debian {\tt |} Red Hat }}};
  \end{tikzpicture}
\end{frame}


% https://commons.wikimedia.org/wiki/Category:Clouds_from_below?uselang=fr#/media/File:Cloud_(5018750171).jpg
% https://commons.wikimedia.org/wiki/Category:Clouds_from_below?uselang=fr#/media/File:Chigwell_Meadow_Essex_England_-_cumulus_clouds.jpg
% https://commons.wikimedia.org/wiki/Category:Clouds_from_below?uselang=fr#/media/File:Clouds_above_Lordship_Recreation_Ground_Haringey_London_England_1.jpg
% https://commons.wikimedia.org/wiki/Category:Clouds_and_blue_sky?uselang=fr#/media/File:2018_05_Havelland_IMG_1931.JPG
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=1.4\paperwidth]{images/clouds}};
    \node [at=(current page.center), text=white,
           text opacity=1, rounded corners=2pt]
          {\LARGE{\textbf{ modules {\tt |} Spack {\tt |} EasyBuild {\tt
                  |} VirtualEnv }}};
  \end{tikzpicture}
\end{frame}


% https://commons.wikimedia.org/wiki/Category:Cumulus_congestus_clouds?uselang=fr#/media/File:Cumulunimbus_IMG_5537.JPG
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=1.4\paperwidth]{images/cumulunimbus}};
    \node [at=(current page.center), text=white,
           text opacity=1, rounded corners=2pt]
          {\LARGE{\textbf{ Ansible {\tt |} Puppet {\tt |} Propellor }}};
  \end{tikzpicture}
\end{frame}

% https://commons.wikimedia.org/wiki/Category:Stratus_clouds?uselang=fr#/media/File:2018-05-18_18_27_24_Low_stratiform_clouds_(base_near_3,000_feet_AGL)_with_wavy,_bumpy_base_viewed_from_Mercer_County_Route_622_(North_Olden_Avenue)_in_Ewing_Township,_Mercer_County,_New_Jersey.jpg
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=1.4\paperwidth]{images/low-clouds}};
    \node [at=(current page.center), text=white, inner sep=5cm,
           text opacity=1, rounded corners=2pt, fill=black, opacity=.5]
          {\LARGE{\textbf{ pip {\tt |} Cabal {\tt |} Cargo {\tt |} CONDA
          {\tt |} Gradle }}};
  \end{tikzpicture}
\end{frame}


% https://commons.wikimedia.org/wiki/Category:Cloud-to-cloud_lightning#/media/File:004_2018_05_14_Extremes_Wetter.jpg
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=1.4\paperwidth]{images/thunder}};
    \node [at=(current page.center), text=white,
           text opacity=1, rounded corners=2pt]
          {\LARGE{\textbf{ Flatpak {\tt |} snap {\tt |} Docker {\tt |} Vagrant }}};
  \end{tikzpicture}
\end{frame}

% TODO 2048 vuln + Docker license opacity

\setbeamercolor{normal text}{bg=guixred3}
\begin{frame}[plain, fragile]
  \center{\Huge{\textbf{Are distros doomed?}}}
  %% \\[2cm]
  %% \uncover<2->{\center{Yes!} \par}
  %% \uncover<3->{\center{No!} \par}
\end{frame}
\setbeamercolor{normal text}{bg=black,fg=white}

\setbeamercolor{normal text}{bg=guixred3,fg=white}
\begin{frame}[plain]
  \begin{quotation}
    \begin{flushright}
    \LARGE{``Debian and other distributions are going to be \textbf{that
        thing you run docker on}, little~more.''}
    \end{flushright}
  \end{quotation}
  \hfill{--- Jos Poortvliet, ownCloud developer (\oldstylenums{2016})}

  \begin{tikzpicture}[overlay]
    \node [at=(current page.south east), anchor=south east]{
      \url{http://lwn.net/Articles/670566/}
    };
  \end{tikzpicture}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\setbeamercolor{normal text}{fg=white,bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    % https://github.com/owncloud-docker/server/blob/master/v19.10/Dockerfile.amd64
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[height=\paperheight]{images/owncloud-dockerfile}};

    \node [at=(current page.center), anchor=south west, overlay,
           inner sep=3mm, rounded corners,
           text=black, text opacity=1, fill=white, opacity=.5, text width=7cm]
          {\LARGE{\textbf{It's also that thing you run \emph{inside} Docker!}}};
  \end{tikzpicture}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
      {\includegraphics[width=1.3\textwidth]{images/smoothie}};
    \node [at=(current page.south east), anchor=south east, text=guixgrey]
      {\small{courtesy of Ricardo Wurmus}};
  \end{tikzpicture}
\end{frame}

% https://en.wikipedia.org/wiki/Don't_throw_the_baby_out_with_the_bathwater#/media/File:Murner.Nerrenbeschwerung.kind.jpg
\setbeamercolor{normal text}{bg=white}
\screenshot[height=.95\paperheight]{images/throwing-the-baby-out-with-the-bathwater}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=0.7\paperwidth]{images/Guix-horizontal-print}};
  \end{tikzpicture}
\end{frame}

\screenshot{images/guix-scope}
\setbeamercolor{normal text}{fg=white,bg=black}

% demo
\begin{frame}[fragile]

  \begin{semiverbatim}
    \LARGE{
guix \alert{install} gcc-toolchain openmpi hwloc

eval `guix package \alert{--search-paths}=prefix`

guix package \alert{--roll-back}

guix install \alert{--profile}=./experiment \\
     gcc-toolchain@5.5 hwloc@1
}
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]
  \begin{semiverbatim}
    \LARGE{
guix package \alert{--manifest}=my-packages.scm



    (\alert{specifications->manifest}
      '("gcc-toolchain" "emacs"
        "guile" "emacs-geiser"))
}
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{bg=guixdarkgrey}
\begin{frame}[fragile]
  \begin{semiverbatim}
    \Large{
bob@laptop$ guix package \alert{--manifest}=my-packages.scm
bob@laptop$ guix \alert{describe}
  guix cabba9e
    repository URL: https://git.sv.gnu.org/git/guix.git
    commit: cabba9e15900d20927c1f69c6c87d7d2a62040fe

\pause


alice@supercomp$ guix \alert{pull} --commit=cabba9e
alice@supercomp$ guix package \alert{--manifest}=my-packages.scm
}
  \end{semiverbatim}

  %% \begin{tikzpicture}[overlay]
  %%   \node<3>[rounded corners=4, text centered, anchor=north,
  %%         fill=guixorange1, text width=7cm,
  %%         inner sep=3mm, opacity=.75, text opacity=1]
  %%     at (current page.center) {
  %%           \textbf{\Large{bit-reproducible \& portable!}}
  %%         };
  %% \end{tikzpicture}
\end{frame}

\begin{frame}[fragile]
  \begin{tikzpicture}[remember picture, overlay]
    % https://commons.wikimedia.org/wiki/File:TeamTimeCar.com-BTTF_DeLorean_Time_Machine-OtoGodfrey.com-JMortonPhoto.com-07.jpg
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=\paperwidth]{images/delorean}};
    \node [rounded corners=4, text centered, anchor=north,
           text width=10cm,
          inner sep=3mm, opacity=.75, text opacity=1]
      at (current page.center) {
            \textbf{\Huge{travel in space \emph{and} time!}}
          };
  \end{tikzpicture}
\end{frame}

\begin{frame}[fragile]
  \begin{semiverbatim}
    \LARGE{
guix \alert{time-machine} --commit=cabba9e -- \\
     install hello
    }
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{bg=guixblue1}
\begin{frame}[fragile]%{``Virtual environments''}
  \LARGE{
    \begin{semiverbatim}
guix \alert{environment} --ad-hoc\uncover<2->{ \alert{--container}} \\
      python python-numpy python-scipy \\
      -- python3
    \end{semiverbatim}
  }
\end{frame}

\setbeamercolor{normal text}{bg=guixred3}
\begin{frame}[fragile]%{Container provisioning}
  \LARGE{
    \begin{semiverbatim}
\$ guix \alert{pack}\only<2>{ --relocatable}\only<3->{ --format=docker} \\
      python python-numpy
\textrm{...}
/gnu/store/\textrm{...}-\only<1-2>{pack.tar.gz}\only<3->{docker-image.tar.gz}
    \end{semiverbatim}
  }
\end{frame}

\setbeamercolor{normal text}{bg=white}
\screenshot[width=.9\paperwidth]{images/docker-guix-lol}
\setbeamercolor{normal text}{fg=white,bg=black}

\screenshot{images/guix-scope-systems}

\begin{frame}[fragile]
  \begin{semiverbatim}
    \Large{
<\textit{bob}> this is how Guix System works: you tell it
     what you want, and it puts all the pieces in place
     for you

<\textit{alice}> yeah you just need to speak its language

<\textit{civodul}> such a fine language, though :-)
}
  \end{semiverbatim}
  \vfill{
  (seen on \#guix)}
\end{frame}

\begin{frame}[fragile]
  \begin{semiverbatim}
    \vspace{-1cm}
    \small{
(\alert{operating-system}
  (host-name "guixbox")
  (timezone "Europe/Brussels")
  (locale "fr_BE.utf8")
  (bootloader (\alert{bootloader-configuration}
                (bootloader grub-efi-bootloader)
                (target "/boot/efi")))
  (file-systems (append (list (\alert{file-system}
                                (device (file-system-label "my-root"))
                                (mount-point "/")
                                (type "ext4")))
                        %base-file-systems))
  (users (append (list (\alert{user-account}
                         (name "charlie")
                         (group "users")
                         (home-directory "/home/charlie")))
                 %base-user-accounts))
  (services (append (list (\alert{service} dhcp-client-service-type)
                          (\alert{service} openssh-service-type))
                    %base-services)))
    }
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]
  \begin{semiverbatim}
    \Large{
\$ guix system \alert{vm} config.scm
\textrm{...}   

\$ guix system \alert{docker-image} config.scm
\textrm{...}

\$ guix system \alert{container} config.scm
\textrm{...}

\$ guix system \alert{reconfigure} config.scm
\textrm{...}
}
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{bg=guixdarkgrey}
\begin{frame}[fragile]
  \begin{semiverbatim}
(\alert{define} (os-for-machine n)
  ;; \textsf{\textit{Return an OS for machine number N.}}
  (operating-system
    (host-name (string-append "machine"
                              (number->string n)))
    \textsf{...}))

;; \textsf{\textit{Return a list of machines.}}
(map (\alert{lambda} (n)
       (\alert{machine}
        (operating-system (os-for-machine n))
        \only<1-3>{(environment managed-host-environment-type)
        (configuration (\alert{machine-ssh-configuration}
                         (host-name (ip-for-machine n))))))}\only<4->{(environment digital-ocean-environment-type)
        (configuration (\alert{digital-ocean-configuration}
                         (region "nyc3")
                         \textsf{...}))))}
     (list 1 2 3 4 5))
  \end{semiverbatim}

  \begin{tikzpicture}[overlay]
    \node<1-2> at (14,6) [anchor=north east,
           inner sep=1mm, rotate=-30, shape=star,
           fill=guixorange1, text=white] {
      \Large{\textbf{New!}}
    };

    \node<2>  [at=(current page.center), inner sep=5mm,
               rounded corners, fill=black, text=white,
               opacity=.75, text opacity=1] {
      \LARGE{\texttt{guix deploy machines.scm}}
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=guixgreen1}
\begin{frame}[fragile]
  \Huge{\textbf{It's all about source code.}}
\end{frame}

\setbeamercolor{normal text}{bg=guixgrey}
\begin{frame}[fragile]
  \begin{semiverbatim}
(define audacity
  (\alert{package}
    (name "audacity")
    (home-page "https://github.com/audacity/audacity")
    (\alert{source} (origin
              (method git-fetch)
              (uri (git-reference
                     (\alert{url} home-page)
                     (\alert{commit} "2f30ff07a")\tikz{\node(commit){};}
                     (recursive? #t)))
              (sha256
               (base32
                "106rf402cvfdhc2yf\textrm{...}"))))
    \textrm{...}))
  \end{semiverbatim}

  \begin{tikzpicture}[overlay]
    \node<2->(swh) [inner sep=3mm, rounded corners, fill=black,
                    opacity=.3, text opacity=1] at (12,5) {
       % https://annex.softwareheritage.org/public/logo/
       \includegraphics[width=0.33\textwidth]{images/software-heritage-logo-title-white}
    };
    \node<2->      [at=(current page.south), anchor=south,
                    inner sep=2mm, rounded corners, fill=black, text width=13cm,
                    opacity=.3, text opacity=1] {
       \url{https://www.softwareheritage.org/2019/04/18/software-heritage-and-gnu-guix-join-forces-to-enable-long-term-reproducibility/}
    };

    \path<2->[very thick, draw=guixorange1]
      (swh) edge [out=-90, in=0, ->] (commit);
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=white}
\screenshot[width=.8\paperwidth]{images/reproducible-builds}
\setbeamercolor{normal text}{bg=guixdarkgrey}


\begin{frame}[plain]
  \LARGE{
    $\texttt{emacs} = f(\texttt{gtk+}, \texttt{gcc}, \texttt{make}, \texttt{coreutils})$
    \\[1.1cm]
    \uncover<2->{$\texttt{gtk+} = g(\texttt{glib}, \texttt{gcc}, \texttt{make}, \texttt{coreutils})$}
    \\[1.1cm]
    \uncover<3->{$\texttt{gcc} = h(\texttt{make}, \texttt{coreutils}, \texttt{gcc}_0)$}
    \\[1.1cm]
    \uncover<3->{\textrm{...}}
  }

  \uncover<1>{\large{where $f =$ \texttt{./configure \&\& make \&\& make install}}}

  %% \begin{tikzpicture}[overlay]
  %%   \node<4->[fill=guixorange1, text=black, text opacity=1, opacity=.7,
  %%         rounded corners=2mm, inner sep=5mm] at (5, 1) {
  %%           \textbf{\Large{the complete DAG is captured}}
  %%         };
  %% \end{tikzpicture}
\end{frame}
%% \begin{frame}[fragile]
%%   \begin{tikzpicture}[overlay]
%%     \node [at=(current page.north west), anchor=north west,
%%       outer sep=4mm, text=white, text width=13mm]{
%%       \texttt{configure},
%%       \texttt{src/hello.c},
%%       GCC,\\
%%       Binutils,
%%       etc.
%%     };
%%     \node [at=(current page.center), outer sep=3mm, font=\rmfamily]{
%%       {\fontfamily{roman}\fontsize{45}{45}{$f(x,y,z)$}}
%%     };
%%   \end{tikzpicture}
%% \end{frame}
\setbeamercolor{normal text}{bg=black}

\begin{frame}[fragile]
  %% \frametitle{Bit-Reproducible Builds$^*$}
  %% \framesubtitle{$^*$ almost!}

  \begin{semiverbatim}
    \Large{
\$ guix build hello
\uncover<2->{/gnu/store/\tikz[baseline]{\node[anchor=base](nixhash){\alert<2>{h2g4sf72\textrm{...}}};}-hello-2.10}

\uncover<3->{\$ \alert<3>{guix gc --references /gnu/store/\textrm{...}-hello-2.10}
/gnu/store/\textrm{...}-glibc-2.29
/gnu/store/\textrm{...}-gcc-7.4.0-lib
/gnu/store/\textrm{...}-hello-2.10
}}
  \end{semiverbatim}

  \begin{tikzpicture}[overlay]
    \node<1>(labelnixhash) [fill=white, text=black, inner sep=0.5cm,
       rounded corners] at (current page.center) {%
      \Large{\textbf{isolated build}: chroot, separate name spaces, etc.}
    };

    \node<2>(labelnixhash) [fill=white, text=black] at (4cm, 2cm) {%
      hash of \textbf{all} the dependencies};
    \path[->]<2>(labelnixhash.north) edge [bend left, in=180, out=-45] (nixhash.south);

    \draw<4-> (-10pt, 105pt) [very thick, color=guixorange2, rounded corners=8pt]
      arc (10:-50:-50pt and 110pt);
    \node<4->[fill=white, text=black, text opacity=1, opacity=.7,
          rounded corners=2mm, inner sep=5mm]
      at (7, 2) {\textbf{\Large{(nearly) bit-identical for everyone}}};
  \end{tikzpicture}

\end{frame}

\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}[fragile]
  \begin{semiverbatim}
$ \alert{guix challenge} --substitute-urls="https://ci.guix.gnu.org https://example.org"
\alert{/gnu/store/\dots{}-openssl-1.0.2d contents differ}:
  local hash: 0725l22\dots{}
  http://ci.guix.gnu.org/\dots{}-openssl-1.0.2d: 0725l22\dots{}
  http://example.org/\dots{}-openssl-1.0.2d: 1zy4fma\dots{}
\alert{/gnu/store/\dots{}-git-2.5.0 contents differ}:
  local hash: 00p3bmr\dots{}
  http://ci.guix.gnu.org/\dots{}-git-2.5.0: 069nb85\dots{}
  http://example.org/\dots{}-git-2.5.0: 0mdqa9w\dots{}
\alert{/gnu/store/\dots{}-pius-2.1.1 contents differ}:
  local hash: 0k4v3m9\dots{}
  http://ci.guix.gnu.org/\dots{}-pius-2.1.1: 0k4v3m9\dots{}
  http://example.org/\dots{}-pius-2.1.1: 1cy25x1\dots{}
  \end{semiverbatim}
\end{frame}
\setbeamercolor{normal text}{bg=black}

% demo guix build foo --check | guix challenge

\setbeamercolor{normal text}{fg=black,bg=white}
\begin{frame}[fragile]
  \vspace{2.5cm}
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt,
    drop shadow={opacity=0.5}, draw, color=guixgrey, line width=1pt]
    {\includegraphics[height=0.9\paperheight]{images/reflections-on-trusting-trust}};
  \end{tikzpicture}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\setbeamercolor{normal text}{bg=white}
\screenshot[width=.8\paperwidth]{images/bootstrappable}
\setbeamercolor{normal text}{bg=black}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
      {\includegraphics[height=\paperheight]{images/bootstrap-graph}};
    \node<2-> [at=(current page.center), anchor=north, inner sep=20pt, text=guixgrey]
      {\Large{\textbf{250 MiB of binary blobs}}};
  \end{tikzpicture}
\end{frame}
\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
      {\includegraphics[height=\paperheight]{images/bootstrap-graph-reduced}};
    \node<2-> [at=(current page.center), fill=guixorange1, rounded corners=10pt,
               inner sep=10pt, opacity=.8, text opacity=1]
      {\Large{\textbf{250 MiB $\rightarrow$ 130 MiB of binary blobs}}};
    \node<2-> [at=(current page.south), anchor=south,
               inner sep=2mm, outer sep=3mm, rounded corners,
               fill=white, opacity=.7, text opacity=1, text=black]
      {\url{https://guix.gnu.org/blog/2019/guix-reduces-bootstrap-seed-by-50/}};
    \node<2-> [at=(current page.north east), anchor=north east,
               fill=white, text=guixdarkgrey, draw=guixblue1,
               rounded corners=10pt, %text width=5cm,
               inner sep=10pt, outer sep=3mm, text opacity=1]
      {\large{\textbf{Go to AW1.125, Sun.~11:50AM}}};
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt, rotate=30]
      {\includegraphics[height=1.1\paperheight]{images/rust-bootstrap}};
    %% \node<2-> [at=(current page.center), fill=guixorange1, rounded corners=10pt,
    %%            inner sep=10pt, opacity=.8, text opacity=1]
    %%   {\Large{\textbf{Thumbs up, Danny Milosavljevic!}}};
    \node<1> [at=(current page.center), fill=guixorange1, rounded corners=10pt,
               inner sep=10pt, opacity=.8, text opacity=1]
      {\Large{\textbf{Rust entirely built from source!}}};
    \node<1> [at=(current page.south), anchor=south,
               inner sep=2mm, outer sep=3mm, rounded corners, fill=white,
               opacity=.7, text opacity=1, text=black]
      {\url{https://guix.gnu.org/blog/2018/bootstrapping-rust/}};
    \node<1-> [at=(current page.north east), anchor=north east,
               fill=white, text=guixdarkgrey, draw=guixblue1,
               rounded corners=10pt, %text width=5cm,
               inner sep=10pt, outer sep=3mm, text opacity=1]
      {\large{\textbf{Go to K.3.401, Sun.~10:00AM}}};
  \end{tikzpicture}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

%% \setbeamercolor{normal text}{bg=white}
%% \begin{frame}[plain]
%%   \begin{tikzpicture}[remember picture, overlay]
%%     \node [at=(current page.center), fill=guixblue1,
%%       shape=circle, inner sep=2.2cm, opacity=.8, text opacity=1] {};
%%     \node [at=(current page.center), fill=guixorange1, rounded corners=10pt,
%%       shape=circle, inner sep=2cm, opacity=1, text opacity=1] {};
%%     \node [at=(current page.center), fill=guixorange1, rounded corners=10pt,
%%       shape=circle, inner sep=10pt, opacity=0, text opacity=1]
%%       {\Huge{\textbf{1.0!}}};

%%   \end{tikzpicture}
%% \end{frame}
%% \setbeamercolor{normal text}{fg=white,bg=black}

\setbeamercolor{normal text}{bg=guixtaupe}
\begin{frame}[fragile]
  \Huge{$f(\texttt{config.scm}) = \vcenter{\hbox{\includegraphics[width=20mm]{images/emblem-system-symbolic}}}$}

  \begin{tikzpicture}[remember picture, overlay]
    \node<2-> [at=(current page.center), shape=circle, inner sep=1cm,
               fill=white, text=black, opacity=.75, text opacity=.9] {
      \Huge{$f^{-1}$}~?
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}[fragile]
  \begin{semiverbatim}
    \Large{
\$ guix system \alert{describe}
  file name: /var/guix/profiles/system-126-link
  canonical file name: /gnu/store/\textsf{\dots{}}-system
  label: GNU with Linux-Libre 5.4.15
  bootloader: grub-efi
  root device: label: "root"
  \alert{channels}:
    guix:
      repository URL: https://git.savannah.gnu.org/\textsf{\dots{}}
      commit: 93f4511eb0c9b33f5083c2a04f4148e0a494059c
  \alert{configuration file}: /gnu/store/\textsf{\dots{}}-configuration.scm
    }
  \end{semiverbatim}

  \begin{tikzpicture}[overlay]
    \node at (14,6) [anchor=east,
           inner sep=1mm, rotate=-30, shape=star,
           fill=guixorange1, text=white] {
      \Large{\textbf{New!}}
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=guixblue1}
\begin{frame}
  \Huge{\textbf{Wrap-up.}}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\setbeamercolor{normal text}{fg=white,bg=guixdarkgrey}
\begin{frame}[fragile]
  \LARGE{
  Not included in this talk :-)
  \\[1cm]

  \begin{itemize}
  \item{ \textbf{embedded} usage
    \begin{itemize}
    \item Go to K.3.201, Sun. 11:00AM!
    \end{itemize} }
  \item { \textbf{Guile} \& programming language technology
    \begin{itemize}
    \item Go to AW1.125, Sun. 11:30AM!
    \end{itemize} }
  \item{ \textbf{Guix-HPC}: high-performance computing
    \begin{itemize}
    \item Go to UB.132, Sun. 12:30PM!
    \end{itemize}}
  \end{itemize}
  }
\end{frame}

\begin{frame}[fragile]
  \Huge{\textbf{Join us now, share the parens!}}
  \vspace{0.7cm}
  \Large{
    \begin{itemize}
    \item \textbf{install it!}
    \item \textbf{use it!}
    \item \textbf{hack it!}
    \item \textbf{join} for Outreachy or GSoC!
    \end{itemize}
  }
\end{frame}

\setbeamercolor{normal text}{bg=white}
\screenshot{images/guix-scope}


\setbeamercolor{normal text}{bg=white}
\begin{frame}[fragile]
  \vspace{-2cm}
  \begin{tikzpicture}
    \matrix[row sep=10mm, column sep=1cm]{
      % https://git-scm.com/downloads/logos
      \node {\includegraphics[width=0.2\textwidth]{images/Git-Logo-2Color}}; &
      \node {\includegraphics[width=0.15\textwidth]{images/arrow-right}}; &
      \node {\includegraphics[width=0.24\textwidth]{images/Guix-horizontal-print}};
      \\
    };
  \end{tikzpicture}

  \begin{tikzpicture}[overlay]
    \node [at=(current page.center), anchor=north,
           text=black, text width=.9\textwidth]{
      \Huge{\textbf{Reproducible deployment}\\ is
        the logical next step. \par
      }};
  \end{tikzpicture}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setbeamercolor{normal text}{bg=black}
\begin{frame}[plain]

\vfill{
  \vspace{1.5cm}
  \center{\includegraphics[width=0.3\textwidth]{images/Guix-white}}\\[1.0cm]
  \texttt{ludo@gnu.org}\hfill{\alert{\url{https://guix.gnu.org/}}}
}

\end{frame}

\begin{frame}{}

  \begin{textblock}{12}(2, 3)
    \tiny{
      Copyright \copyright{} 2010, 2012--2020 Ludovic Courtès \texttt{ludo@gnu.org}.\\[3.0mm]
      GNU Guix logo, CC-BY-SA 4.0, \url{https://gnu.org/s/guix/graphics} \\
      Reproducible Builds logo under CC-BY 3.0,
      \url{https://uracreative.github.io/reproducible-builds-styleguide/visuals/}. \\
      Bootstrappable Builds logo by Ricardo Wurmus,
      \url{https://bootstrappable.org}. \\
      Docker whale image by Ricardo Wurmus. \\
      Smoothie image \copyright{} 2019 Ricardo Wurmus, CC-BY-SA 4.0. \\
      Hand-drawn arrows by Freepik from flaticon.com.
      \\[1.5mm]
      Picture of the sun under CC-BY-SA 3.0,
      \url{https://commons.wikimedia.org/wiki/File:\%22Sun\%22.JPG}. \\
      Cloud picture 1 under CC-BY-SA 2.0,
      \url{https://commons.wikimedia.org/wiki/File:Cloud_(5018750171).jpg}. \\
      Cloud picture 2 under CC-BY-SA 3.0,
      \url{https://commons.wikimedia.org/wiki/File:Cumulunimbus_IMG_5537.JPG}. \\
      Cloud picture 3 under CC-BY-SA 4.0,
      \url{https://commons.wikimedia.org/wiki/File:2018-05-18_18_27_24_Low_stratiform_clouds_(base_near_3,000_feet_AGL)_with_wavy,_bumpy_base_viewed_from_Mercer_County_Route_622_(North_Olden_Avenue)_in_Ewing_Township,_Mercer_County,_New_Jersey.jpg}. \\
      Thunder picture under CC-BY-SA 4.0,
      \url{https://commons.wikimedia.org/wiki/File:004_2018_05_14_Extremes_Wetter.jpg}.
      \\[1.5mm]
      Copyright of other images included in this document is held by
      their respective owners.
      \\[3.0mm]
      This work is licensed under the \alert{Creative Commons
        Attribution-Share Alike 3.0} License.  To view a copy of this
      license, visit
      \url{http://creativecommons.org/licenses/by-sa/3.0/} or send a
      letter to Creative Commons, 171 Second Street, Suite 300, San
      Francisco, California, 94105, USA.
      \\[2.0mm]
      At your option, you may instead copy, distribute and/or modify
      this document under the terms of the \alert{GNU Free Documentation
        License, Version 1.3 or any later version} published by the Free
      Software Foundation; with no Invariant Sections, no Front-Cover
      Texts, and no Back-Cover Texts.  A copy of the license is
      available at \url{http://www.gnu.org/licenses/gfdl.html}.
      \\[2.0mm]
      % Give a link to the 'Transparent Copy', as per Section 3 of the GFDL.
      The source of this document is available from
      \url{http://git.sv.gnu.org/cgit/guix/maintenance.git}.
    }
  \end{textblock}
\end{frame}

\end{document}

% Local Variables:
% coding: utf-8
% comment-start: "%"
% comment-end: ""
% ispell-local-dictionary: "american"
% compile-command: "rubber --pdf talk.tex"
% End:
