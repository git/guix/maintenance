% The comment below tells Rubber to compile the .dot files.
%
% rubber: module graphics
% rubber: rules rules.ini

% Make sure URLs are broken on hyphens.
% See <https://tex.stackexchange.com/questions/3033/forcing-linebreaks-in-url>.
\RequirePackage[hyphens]{url}

\documentclass[aspectratio=169]{beamer}

\usetheme{default}

\usefonttheme{structurebold}

% Nice sans-serif font.
\usepackage[sfdefault,lining]{FiraSans} %% option 'sfdefault' activates Fira Sans as the default text font
\renewcommand*\oldstylenums[1]{{\firaoldstyle #1}}

% Nice monospace font.
\usepackage{inconsolata}
%% \renewcommand*\familydefault{\ttdefault} %% Only if the base font of the document is to be typewriter style
\usepackage[T1]{fontenc}

\usepackage[utf8]{inputenc}
\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref,xspace,multicol}

\usepackage[absolute,overlay]{textpos}
\usepackage{tikz}
\usetikzlibrary{arrows,shapes,trees,shadows,positioning,backgrounds}
\usepackage{fancyvrb}           % for '\Verb'
\usepackage{xifthen}            % for '\isempty'

% Remember the position of every picture.
\tikzstyle{every picture}+=[remember picture]

\tikzset{onslide/.code args={<#1>#2}{%
  \only<#1>{\pgfkeysalso{#2}} % \pgfkeysalso doesn't change the path
}}

% Colors.
\definecolor{guixred1}{RGB}{226,0,38}  % red P
\definecolor{guixorange1}{RGB}{243,154,38}  % guixorange P
\definecolor{guixyellow}{RGB}{254,205,27}  % guixyellow P
\definecolor{guixred2}{RGB}{230,68,57}  % red S
\definecolor{guixred3}{RGB}{115,34,27}  % dark red
\definecolor{guixorange2}{RGB}{236,117,40}  % guixorange S
\definecolor{guixtaupe}{RGB}{134,113,127} % guixtaupe S
\definecolor{guixgrey}{RGB}{91,94,111} % guixgrey S
\definecolor{guixdarkgrey}{RGB}{46,47,55} % guixdarkgrey S
\definecolor{guixblue1}{RGB}{38,109,131} % guixblue S
\definecolor{guixblue2}{RGB}{10,50,80} % guixblue S
\definecolor{guixgreen1}{RGB}{133,146,66} % guixgreen S
\definecolor{guixgreen2}{RGB}{157,193,7} % guixgreen S

\setbeamerfont{title}{size=\huge}
\setbeamerfont{frametitle}{size=\huge}
\setbeamerfont{normal text}{size=\Large}

% White-on-black color theme.
\setbeamercolor{structure}{fg=guixorange1,bg=black}
\setbeamercolor{title}{fg=white,bg=black}
\setbeamercolor{date}{fg=guixorange1,bg=black}
\setbeamercolor{frametitle}{fg=white,bg=black}
\setbeamercolor{titlelike}{fg=white,bg=black}
\setbeamercolor{normal text}{fg=white,bg=black}
\setbeamercolor{alerted text}{fg=guixyellow,bg=black}
\setbeamercolor{section in toc}{fg=white,bg=black}
\setbeamercolor{section in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsubsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsubsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{frametitle in toc}{fg=white,bg=black}
\setbeamercolor{local structure}{fg=guixorange1,bg=black}

\newcommand{\highlight}[1]{\alert{\textbf{#1}}}

\title{Reproductibilité et portabilité des performances}

\author{Ludovic Courtès}
\date{\small{JCAD, 14 décembre 2019}}

\setbeamertemplate{navigation symbols}{} % remove the navigation bar


\newcommand{\screenshot}[2][width=\paperwidth]{
  \begin{frame}[plain]
    \begin{tikzpicture}[remember picture, overlay]
      \node [at=(current page.center), inner sep=0pt]
        {\includegraphics[{#1}]{#2}};
    \end{tikzpicture}
  \end{frame}
}


\begin{document}

\begin{frame}[plain, fragile]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.center)] {
      % https://images.bnf.fr/#/detail/1535157/9
      \includegraphics[width=1.5\textwidth]{images/tandem-jules-beau-1896}
    };
    \node [at=(current page.center), fill=guixorange2, opacity=.4,
      text width=1.3\textwidth, text height=\textheight] {
    };
    \node [at=(current page.south east), anchor=south east, inner sep=5mm] {
      {\includegraphics[width=0.2\paperwidth]{images/inria-white-2019}}
    };
  \end{tikzpicture}

  \vspace{20mm}
  \Huge{\textbf{Reproductibilité \& \\
      Portabilité des performances}}
  \\[15mm]
  \large{Ludovic Courtès}
  \\[2mm]
  \alert{\textbf{Journées calcul et données (JCAD)}}
  \\[1.5mm]
  \textbf{\oldstylenums{14 décembre 2021}}
  \vfill{}

\end{frame}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \includegraphics[width=\textwidth]{images/big-picture-3}
\end{frame}


\setbeamercolor{normal text}{fg=black,bg=guixtaupe}
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.center)] {
      \includegraphics[width=\paperwidth]{images/supercomputer-us-doe}
    };
  \end{tikzpicture}

  \vfill{}
  \Huge{\textbf{Deux obsessions : MPI et AVX}}
\end{frame}

\begin{frame}[plain, fragile]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.center), inner sep=0pt, rotate=5,
    drop shadow={opacity=0.7, fill=black}, draw, color=black, line width=1pt]
    {\includegraphics[width=0.9\paperwidth]{images/gamblin-binary-portability}};

    \node(ref) [at=(current page.south), anchor=south, color=guixdarkgrey, rounded corners,
      fill=white, opacity=.7, text opacity=1, inner sep=3mm]
          {\url{https://trex-coe.eu/events/trex-build-system-hackathon-8-12-nov-2021}};
    \node [at=(ref.north east), anchor=south east, color=guixdarkgrey, inner sep=3mm, outer sep=3mm] {
      Todd Gamblin (Spack)
    };

  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{fg=black,bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.center)] {
      \includegraphics[height=\textheight]{images/openmpi-graph}
    };
    \node<2-> [at=(current page.center), fill=guixblue2, opacity=.6,
      text width=1.3\textwidth, text height=\textheight] {
    };
    \node<2-> [at=(current page.south), anchor=south, inner sep=3mm,
      text=white] {
      \url{https://hpc.guix.info/blog/2019/12/optimized-and-portable-open-mpi-packaging/}
    };
  \end{tikzpicture}

  \Large{
  \begin{itemize}
    \item<2-> InfiniBand (UCX, rdma-core)
    \item<2-> InfiniPath/TrueScale (PSM)
    \item<2-> Omni-Path (PSM2)
    \item<2-> Ethernet
    \item<2-> ...
  \end{itemize}
  }
\end{frame}


\setbeamercolor{normal text}{fg=black,bg=guixgreen2}
\begin{frame}[plain]
  \vspace{12mm}
  \Huge{\textbf{La jungle\\ des extensions vectorielles}}
\end{frame}

\setbeamercolor{normal text}{fg=black,bg=guixtaupe}

\begin{frame}[plain, fragile]
  \begin{tikzpicture}[
        box/.style = { fill=guixblue2, inner sep=3mm, rounded corners, font=\LARGE\bf }
      ]
    \matrix[row sep=15mm, column sep=12mm] {
      \node(sse2) [box, draw=guixorange1, thick] {SSE2 (ca. 2003)}; & & \node(avx512) [box] {AVX-512 (2013)}; \\
      \node(sse3) [box] {SSE3}; & \node {\LARGE\textbf{x86\_64}}; & \node(avx2) [box] {AVX2}; \\
      \node(ssse3) [box] {SSSE3}; & & \node(avx) [box] {AVX}; \\
    };

    \path[very thick, draw=guixorange1] (sse2) edge [->] (sse3);
    \path[very thick, draw=guixorange1] (sse3) edge [->] (ssse3);
    \path[very thick, draw=guixorange1] (ssse3) edge [->] (avx);
    \path[very thick, draw=guixorange1] (avx) edge [->] (avx2);
    \path[very thick, draw=guixorange1] (avx2) edge [->] (avx512);
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain, fragile]
  \begin{tikzpicture}[
        box/.style = { fill=guixblue2, inner sep=3mm, rounded corners, font=\Large }
      ]
    \matrix[row sep=12mm, column sep=17mm] {
      \node(bonnel) [box] {bonnel};         & \node(haswell) [box] {haswell};         & \node(broadwell) [box] {broadwell}; \\
      \node(silvermont) [box] {silvermont}; & \node(ivybridge) [box] {ivybridge};     & \node(skylake) [box] {skylake}; \\
      \node(core2) [box] {core2};           & \node(sandybridge) [box] {sandybridge}; & \node(skylake-avx512) [box] {skylake-avx512}; \\
      \node(nehalem) [box] {nehalem};       & \node(westmere) [box] {westmere};       & \node(etc) [box] {\textit{etc}...}; \\
    };

    \path[very thick, draw=guixorange1, ->] (bonnel) edge (silvermont);
    \path[very thick, draw=guixorange1, ->] (silvermont) edge (core2);
    \path[very thick, draw=guixorange1, ->] (core2) edge (nehalem);
    \path[very thick, draw=guixorange1, ->] (nehalem) edge (westmere);
    \path[very thick, draw=guixorange1, ->] (westmere) edge (sandybridge);
    \path[very thick, draw=guixorange1, ->] (sandybridge) edge (ivybridge);
    \path[very thick, draw=guixorange1, ->] (ivybridge) edge (haswell);
    \path[very thick, draw=guixorange1, ->] (haswell) edge (broadwell);
    \path[very thick, draw=guixorange1, ->] (broadwell) edge (skylake);
    \path[very thick, draw=guixorange1, ->] (skylake) edge (skylake-avx512);
    \path[very thick, draw=guixorange1, ->] (skylake-avx512) edge (etc);
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain, fragile]
  \vspace{2cm}
  \Large{
    \begin{tabular}{lrr}
      \textbf{code} & \textbf{GEMM 240x240x240} \\
      Eigen (x86\_64 baseline)          & 11 Gflops/s \\
      Eigen \texttt{-march=westmere}    & 12 Gflops/s \\
      Eigen \texttt{-march=sandybridge} & 20 Gflops/s \\
      Eigen \texttt{-march=skylake}     & \textbf{36} Gflops/s \\
      OpenBLAS                          & 16 Gflops/s \\
    \end{tabular}
  }

  \vfill{}
  \small{\texttt{guix shell eigen-benchmarks -- benchBlasGemm 240 240 240}, Intel CORE i7}

  \begin{tikzpicture}[overlay]
    \node at (13,4) [shape=circle, fill=guixorange2, inner sep=3mm] {
      \LARGE{\textbf{x3}}
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{fg=black,bg=guixred3}
\begin{frame}[plain, fragile]
  \begin{semiverbatim}
    \Large{
$ ls /cm/shared/modules/\alert{intel}/
broadwell       haswell  ivybridge
knightslanding  knl      skylake

$ ls /cm/shared/modules/intel/\alert{skylake}/modulefiles/
compiler  linalg  tools
}
  \end{semiverbatim}
\end{frame}

\begin{frame}[plain, fragile]
  \begin{semiverbatim}
$ \alert{spack} spec netcdf-c
Concretized
--------------------------------
netcdf-c@4.8.1\textsf{...} arch=linux-ubuntu18.04-\alert{skylake_avx512}
    ^hdf5@1.10.7\textsf{...} arch=linux-ubuntu18.04-\alert<2->{skylake_avx512}
        ^cmake@3.21.4\textsf{...} arch=linux-ubuntu18.04-\alert<3->{skylake_avx512}
            ^ncurses@6.2\textsf{...} arch=linux-ubuntu18.04-\alert<4->{skylake_avx512}
                ^pkgconf@1.8.0\textsf{...} arch=linux-ubuntu18.04-\alert<4->{skylake_avx512}
            ^openssl@1.1.1l\textsf{...} arch=linux-ubuntu18.04-\alert<4->{skylake_avx512}
                ^perl@5.34.0\textsf{...} arch=linux-ubuntu18.04-\alert<4->{skylake_avx512}
                    ^berkeley-db@18.1.40\textsf{...} arch=linux-ubuntu18.04-\alert<4->{skylake_avx512}
                    ^bzip2@1.0.8\textsf{...} arch=linux-ubuntu18.04-\alert<4->{skylake_avx512}
                        ^diffutils@3.8\textsf{...} arch=linux-ubuntu18.04-\alert<4->{skylake_avx512}
                            ^libiconv@1.16\textsf{...} arch=linux-ubuntu18.04-\alert<4->{skylake_avx512}
                    ^gdbm@1.19\textsf{...} arch=linux-ubuntu18.04-\alert<4->{skylake_avx512}
                        ^readline@8.1\textsf{...} arch=linux-ubuntu18.04-\alert<4->{skylake_avx512}
                    ^zlib@1.2.11\textsf{...} arch=linux-ubuntu18.04-\alert<4->{skylake_avx512}
  \end{semiverbatim}

  \begin{tikzpicture}[overlay]
    \node<4> [at=(current page.south), anchor=south, inner sep=3mm,
      outer sep=3mm,
      text=white, fill=black, rounded corners, opacity=.4, text opacity=1] {
      \url{https://spack.readthedocs.io/en/latest/getting_started.html}
    };

    \node<5> [at=(current page.center)] {
      \includegraphics[height=.9\textheight]{images/xkcd-compiling}
    };
    \node<5> [at=(current page.west), anchor=north, inner sep=3mm,
      outer sep=3mm, rotate=90,
      text=white, fill=black, rounded corners, opacity=.4, text opacity=1] {
      \url{https://xkcd.com/303/}
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{fg=black,bg=guixorange1}
\begin{frame}[plain]
  \begin{tikzpicture}[overlay]
    \node at (current page.center) [anchor=center,
           inner sep=-10mm, shape=star, star points=9,
           fill=white, text=guixdarkgrey] {
      \LARGE{\textbf{Function multi-versioning!}}
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{fg=black,bg=guixtaupe}

\begin{frame}[plain, fragile]
  \begin{tikzpicture}[
        box/.style = { inner sep=3mm, rounded corners, font=\Large },
        crypto/.style = { box, fill=guixblue2 },
        linalg/.style = { box, fill=guixblue1 },
        multip/.style = { box, fill=guixdarkgrey },
        lang/.style = { box, fill=guixred2 },
      ]
    \matrix[row sep=12mm, column sep=20mm] {
      \node[box]{GNU libc}; & \node[crypto]{Libgcrypt}; & \node[crypto]{Nettle}; \\
      \node[linalg]{OpenBLAS}; & \node[linalg]{BLIS}; & \node[linalg]{FFTW}; \\
      \node[multip]{GMP}; & \node[lang]{Julia}; & \node[lang]{Rust}; \\
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain, fragile]
  \Large{
  \begin{semiverbatim}
void my_func (double *array)
  __attribute__ ((\alert{target_clones} ("arch=skylake", "default")));

void my_func (double *array)
\{
  \textrm{...}
\}
  \end{semiverbatim}
  }

  \begin{tikzpicture}[overlay]
    \node<2> [at=(current page.south), anchor=south, rounded corners, inner sep=3mm, fill=guixdarkgrey, text opacity=1, opacity=.6, text=white, outer sep=3mm] {
      \small{\url{https://hpc.guix.info/blog/2018/01/pre-built-binaries-vs-performance/}}
    };
  \end{tikzpicture}
\end{frame}

% generate patch -> applicability?

% (note: simdjson does FMV
% <https://github.com/simdjson/simdjson/blob/master/doc/implementation-selection.md>)

% Problem: Eigen, etc.

\setbeamercolor{normal text}{fg=black,bg=white}
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.center), shape=star, star points=9, fill=white, inner sep=7mm] {};

    \node [at=(current page.center)] {
      \includegraphics[width=\paperwidth]{images/eigen-users}
    };

    \begin{scope}[on background layer]
    \node<2-> [at=(current page.center), text width=\paperwidth, text height=\paperheight,
      fill=guixred3, opacity=.9] {};
    \end{scope}

    \node<3> [at=(current page.south), anchor=south, rounded corners, inner sep=3mm, fill=guixdarkgrey, text opacity=1, opacity=.6, text=white, outer sep=3mm] {
      \url{https://gitlab.com/libeigen/eigen/-/issues/2344}
    };
  \end{tikzpicture}

  \begin{semiverbatim}
  \only<3->{
    \LARGE{
#ifdef \alert{__AVX512F__}
    \textrm{...}
#elif defined \alert{__AVX__}
    \textrm{...}

#else
// Unoptimized baseline code.
#endif
}}
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{fg=black,bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[overlay]
    \node at (current page.center) [anchor=center,
           inner sep=-30mm, shape=star, star points=9,
           fill=guixorange1, text=black] {
      \Huge{\textbf{Package multi-versioning!}}
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{fg=black,bg=guixtaupe}
\begin{frame}[plain, fragile]
  \begin{semiverbatim}
    \Large{
\$ guix \alert{shell} eigen-benchmarks -- \\
    benchBlasGemm 240 240 240
240 x 240 x 240
cblas: 0.20367 (16.289 GFlops/s)
eigen : 0.285149 (11.635 GFlops/s)

\uncover<2->{\$ guix \alert{shell} \alert{--tune} eigen-benchmarks -- \\
guix shell: tuning for CPU micro-architecture \alert{skylake}
240 x 240 x 240
cblas: 0.203131 (16.333 GFlops/s)
eigen : 0.0929638 (\alert{35.688} GFlops/s)}
    }
  \end{semiverbatim}

  \begin{tikzpicture}[overlay]
    \node<3-> [at=(current page.north east), anchor=north, rotate=-30, inner sep=0mm,
      shape=star, star points=5, fill=guixorange1, text=black, outer sep=40mm]
              { \LARGE{\textbf{WIP}} };
    \node<3-> [at=(current page.south), anchor=south, rounded corners, inner sep=3mm, fill=guixdarkgrey, text opacity=1, opacity=.6, text=white, outer sep=3mm] {
      \url{https://issues.guix.gnu.org/52283}
    };

  \end{tikzpicture}
\end{frame}

\begin{frame}[plain, fragile]
  \begin{tikzpicture}[
        box/.style = { fill=guixblue2, inner sep=3mm, rounded corners, font=\Large },
        tuned/.style = { box, fill=guixblue2 }
      ]
    \matrix[row sep=12mm, column sep=20mm] {
      \node(eigen) [box]{Eigen}; & & \\
      \uncover<2->{\node(vtk2)[tuned]{VTK \texttt{-march=skylake}};} & \node(vtk)[box]{VTK}; & \\
      & & \node(opencv)[box] {OpenCV}; \\
    };

    \path<1>[very thick, draw=guixorange1, ->] (eigen) edge (vtk);
    \path<1>[very thick, draw=guixorange1, ->] (vtk) edge (opencv);
    \path<2->[very thick, draw=guixorange1, ->, dotted] (eigen) edge (vtk);
    \path<2->[very thick, draw=guixorange1, ->, dotted] (vtk) edge (opencv);
    \path<2->[very thick, draw=guixorange1, ->] (eigen) edge (vtk2);
    \path<2->[very thick, draw=guixorange1, ->] (vtk2) edge (opencv);
  \end{tikzpicture}

  \begin{tikzpicture}[overlay]
    \node<3> at (10,5) [fill=guixorange1, inner sep=3mm, rounded corners,
      rotate=-20] { \LARGE{\textbf{Reproducible!}} };
  \end{tikzpicture}
\end{frame}

% conclusion: 
\setbeamercolor{normal text}{fg=black,bg=guixblue1}
\begin{frame}[plain, fragile]
  \Huge{
    \vspace{2cm}
    MPI, OPA, UCX, SSE, AVX, NEON...
    \\[2cm]
    \uncover<2->{\textbf{Portabilité, performance \\[2mm]
      \emph{et} reproductibilité.}}
    %% \uncover<2->{\textbf{Nul besoin de sacrifier \\[2mm]
    %%     la \emph{reproductibilité} \\[2mm]
    %%     pour les \emph{performances}.}}
  }
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setbeamercolor{normal text}{bg=black}
\begin{frame}[plain]

\vfill{
  \vspace{3cm}
  \center{\includegraphics[width=0.5\textwidth]{images/guixhpc-logo-transparent}}\\[1.0cm]
  \center{\alert{\url{https://hpc.guix.info}}} \\[0.2cm]
  \texttt{ludovic.courtes@inria.fr |} @GuixHPC
  \\[1.2cm]
}
\end{frame}


\begin{frame}{}
  \begin{textblock}{12}(2, 6)
    \tiny{
      Copyright \copyright{} 2010, 2012--2021 Ludovic Courtès \texttt{ludo@gnu.org}.\\[3.0mm]
      GNU Guix logo, CC-BY-SA 4.0, \url{https://gnu.org/s/guix/graphics}.
      \\[1.5mm]
      Tandem picture by Jules Beau, public domain, \url{https://images.bnf.fr/\#/detail/1535157/9}
      \\[1.5mm]
      LLNL supercomputer picture by US DoE, public domain,
      \url{https://commons.wikimedia.org/wiki/File:U.S._Department_of_Energy_-_Science_-_477_018_010_(9563440651).jpg}
      \\[1.5mm]
      Copyright of other images included in this document is held by
      their respective owners.
      \\[3.0mm]
      This work is licensed under the \alert{Creative Commons
        Attribution-Share Alike 3.0} License.  To view a copy of this
      license, visit
      \url{https://creativecommons.org/licenses/by-sa/3.0/} or send a
      letter to Creative Commons, 171 Second Street, Suite 300, San
      Francisco, California, 94105, USA.
      \\[2.0mm]
      At your option, you may instead copy, distribute and/or modify
      this document under the terms of the \alert{GNU Free Documentation
        License, Version 1.3 or any later version} published by the Free
      Software Foundation; with no Invariant Sections, no Front-Cover
      Texts, and no Back-Cover Texts.  A copy of the license is
      available at \url{https://www.gnu.org/licenses/gfdl.html}.
      \\[2.0mm]
      % Give a link to the 'Transparent Copy', as per Section 3 of the GFDL.
      The source of this document is available from
      \url{https://git.sv.gnu.org/cgit/guix/maintenance.git}.
    }
  \end{textblock}
\end{frame}

\end{document}

% Local Variables:
% coding: utf-8
% comment-start: "%"
% comment-end: ""
% ispell-local-dictionary: "francais"
% compile-command: "guix shell -m ../beamer-manifest.scm -- rubber --pdf talk.tex"
% End:

%%  LocalWords:  Reproducibility
